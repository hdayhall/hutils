
<!-- create_documentation.py START Module root -->

# Module root

*Documentation for the code at `root.py`*

Handle root files.
Tools specifically for dealing with root files, which both cover
common operations and can sometimes serve all needs without importing
root anywhere else.
This is valuable because when imported, root swallows any `--help`
flags, which interferes with the help text provided by argparse.
The import here is through a proxy pattern, so that root only appears
to be imported from the start, and is actually imported first time
there is an attempt to retrieve an attribute from it.
This see normally more than enough of a delay to allow argparse
to be called first, parsing the `--help` flag correctly when needed.


## Example


Use case, you need to open a TFile, but don't expect
to do much else requiring pyroot and want to avoid the import
for the reasons mentioned above.

You could just use the proxy pattern import created here;


```python
from hutils.root import ROOT
```


which fixes issues with the help flag.
But if you really just want a file open;


```python
from hutils import root
tfile = root.open_tfile("/path/to/file.root")
```


By default this opens the file in `READ` mode,
and throws an error if it doesn't exist.
There is a mode argument to change that behavior.
If the argument passed is actually a TFile, not a string
the `ReOpen` method is called to ensure the mode matches the
users requirement.


## Example


Use case, you have a TFile, made of objects that
implement `GetListOfKeys` and `Get` to some unknown depth.
You want to get a python dictionary that replicates the
TFile structure.


```python
from hutils import root
tfile = root.open_tfile("/path/to/file.root")
key_dict = root.recursive_get_keys(tfile)
```


This `key_dict` object is a recursive dictionary,
that ends with the string representation of the first object
that cannot be unpacked using `GetListOfKeys` and `Get`.


## Example


Use case; you have some text that you with to read, write
or append to an otherwise blank canvas in a root file.

Let us imagine that you want to add the hash of the
git commit currently active to a canvas on a root file.
First, get the has itself;


```python
import subprocess
githash = subprocess.getoutput("git rev-parse HEAD")
```


Of course, if you are not in a git repo, this will return nonsense,
but let's assume you are.


```python
from hutils import root
text = "Githash at run time=\n" + githash
root.write_canvas_text("/path/to/file.root", "GitTextCanvas", text)
```


Now if you open the root file at `/path/to/file.root` with a
TBrowser you should see a canvas with two text objects, one
for each line of text in our `text` string.
They have to be separate text objects, because TText has
very limited facilities for handling line breaks.

To check it worked algorithmically;


```python
from hutils import root
assert root.has_canvas("/path/to/file.root", "GitTextCanvas")
found_text = root.read_canvas_text("/path/to/file.root", "GitTextCanvas")
assert isinstance(found_text, str)
print(found_text)
```




<!-- create_documentation.py END Module root -->


<!-- create_documentation.py START Breakpoint disclaimer -->



 -----
 -----

 This should be everything needed for basic use of the package.
 Unless you are looking to develop the package, or debug bad behaviour,
 you probably don't need anything below this point.


<!-- create_documentation.py END Breakpoint disclaimer -->


<!-- create_documentation.py START Docstrings header -->



 ## Doctrings of functions and classes


<!-- create_documentation.py END Docstrings header -->


<!-- create_documentation.py START Function append\_canvas\_text -->

## Function append\_canvas\_text

**append\_canvas\_text\(root\_file, title, text\)**

Given a TFile or the path to a root file, append
given text to a canvas that may already be present
at top level.


### Parameters


**root\_file : str or ROOT.TFile**

root file, or path to the root file

**title : str**

The name the canvas is saved under.
Technically its the canvas name, not it's title.

**text : str**

All the lines of text to put on the canvas joined
into a multiline string.
<!-- create_documentation.py END Function append\_canvas\_text -->


<!-- create_documentation.py START Function has\_canvas -->

## Function has\_canvas

**has\_canvas\(root\_file, title\)**

Check if the given TFile contains a canvas of the specified
name at top level.


### Parameters


**root\_file : str or ROOT.TFile**

root file, or path to the root file

**title : str**

The name the canvas is saved under.
Technically its the canvas name, not it's title.


### Returns


**: bool**

`True` if the canvas is present
    
<!-- create_documentation.py END Function has\_canvas -->


<!-- create_documentation.py START Function open\_tfile -->

## Function open\_tfile

**open\_tfile\(file\_name, mode='READ'\)**

Given a filepath of a tfile, try to open it in the specified mode,
calling `ReOpen` if needed.


### Parameters


**file\_name : str or ROOT.TFile**

File to be opened

**mode : str \(optional\)**

Mode to open the file in.
Can be `"READ"`, `"NEW"`, `"CREATE"`, `"RECREATE"`, `"NET"` or `"WEB"`.
\(Default value = `"READ"`\)


### Returns


**tfile : ROOT.TFile**

The opened, or reopened TFile.
<!-- create_documentation.py END Function open\_tfile -->


<!-- create_documentation.py START Function read\_canvas\_text -->

## Function read\_canvas\_text

**read\_canvas\_text\(root\_file, title\)**

Extract text that has been written to a canvas on a root file.
Assumes each separate TText object on the canvas is another line
of the text, and joins them with line breaks.


### Parameters


**root\_file : str or ROOT.TFile**

root file, or path to the root file

**title : str**

The name the canvas is saved under.
Technically its the canvas name, not it's title.


### Returns


**text : str**

All the lines of text on the canvas joined into a
multiline string.
<!-- create_documentation.py END Function read\_canvas\_text -->


<!-- create_documentation.py START Function recursive\_get\_keys -->

## Function recursive\_get\_keys

**recursive\_get\_keys\(root\_tree, max\_depth=\-1, \_recusion=0\)**

Given a TTree, make a recursive dictionary of all keys found by
using `GetListOfKeys` and `Get`. The terminal layer of the dictionary
contains string representations of the objects that did not respond
to `Get` or `GetListOfKeys`.

Not often useful, most root objects don't respond to generic `Get`
which is a shame, because that would be a really nice use of
the collection pattern.


### Parameters


**root\_tree : ROOT.TObject**

Open TFile or TTree to be inspected.

**max\_depth : int**

Maximum depth to recurse to, if this is hit,
the final layer will be an empty dict, to
indicate the possibility of lower layers.


### Returns


**key\_dict : recursive dict of str**

This `key_dict` object is a recursive dictionary,
that ends with the string representation of the first object
that cannot be unpacked using `GetListOfKeys` and `Get`.
<!-- create_documentation.py END Function recursive\_get\_keys -->


<!-- create_documentation.py START Function text\_on\_canvas -->

## Function text\_on\_canvas

**text\_on\_canvas\(canvas\_name, text, title=''\)**

Create a canvas, and write specified text to it.


### Parameters


**canvas\_name : str**

The name for the canvas to take in the root file,
referred to elsewhere in this module as title.

**text : str**

String with one or more lines of text.

**title : str \(optional\)**

A title for the canvas \(in the sense of a root
canvas object's title attribute\).


### Returns


**canvas : ROOT.TCanvas**

A canvas object containing the lines of text as one
or more ROOT.TText object.
<!-- create_documentation.py END Function text\_on\_canvas -->


<!-- create_documentation.py START Function write\_canvas\_text -->

## Function write\_canvas\_text

**write\_canvas\_text\(root\_file, title, text\)**

Given a TFile or the path to a root file, write
given text on a canvas at top level.


### Parameters


**root\_file : str or ROOT.TFile**

root file, or path to the root file

**title : str**

The name the canvas is saved under.
Technically its the canvas name, not it's title.

**text : str**

All the lines of text to put on the canvas joined
into a multiline string.
<!-- create_documentation.py END Function write\_canvas\_text -->


<!-- create_documentation.py START Object ROOT -->

## Object ROOT

**__root_importer**

<hutils.root.\_\_root\_importer object at 0x106f22310>
<!-- create_documentation.py END Object ROOT -->


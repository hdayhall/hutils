
<!-- create_documentation.py START Module create documentation -->

# Module create documentation

*Documentation for the code at `create_documentation.py`*

Create documentation.
Module for automatically generating documentation for python projects that
have numpy docstrings.

Forms markdown files that can be updated, and can have other
comments included that will not be overwritten by updates.
Markdown files are designed for display in gitlab.
It is assumed that for the most part documentation found in the python
file is not already markdown formatted.


## Example


Say you want to create documentation for the python code below,
which you have saved in animal\_example.py;


```python
""" 
My Module docstring
"""
__documentation_file = "/path/to/documentation/for/animal_example.md"
cat = "Meow"

def dog(noise="woof"):
    """ Docstring for dog

    Parameters
    ----------
    noise : str
        Noise that a dog makes.
        (Default value = "woof")

    Returns
    -------
    balls : int
        Number of tennis balls
    sticks : float
        Length of stick

    """
    balls = 4
    sticks = 3.5
    return balls, sticks

```


In particular note that;

1. All doc strings are surrounded by three double quotes
2. All docstrings have the numpydoc format
3. No markdown is used in the docstrings, as this would be interpreted literally
4. The module has a module docstring at the top before the first class or function.
5. Any single backticks in documentation are contained in a single line, for multiline code formatting, use three backticks.

Documentation for this could be created by running;


```python
import animal_example
from hutils import create_documentation
create_documentation.update_docs(animal_example)
```


As the `animal_example.py` contains a variable `__documentation_file`
a new file will be created at the path specified by `__documentation_file`.
If that variable hadn't been present then the documentation would have been
created in the same place as the code, with the name `animal_example.md`.

The documentation for this file will contain;


```markdown

<!-- create_documentation.py START Module animal example -->

# Module animal example

*Documentation for the code at `animal_example.py`*

 
My Module docstring

<!-- create_documentation.py END Module animal example -->


<!-- create_documentation.py START Breakpoint disclaimer -->



 -----
 -----

 This should be everything needed for basic use of the package.
 Unless you are looking to develop the package, or debug bad behaviour,
 you probably don't need anything below this point.


<!-- create_documentation.py END Breakpoint disclaimer -->


<!-- create_documentation.py START Docstrings header -->



 ## Doctrings of functions and classes


<!-- create_documentation.py END Docstrings header -->


<!-- create_documentation.py START Function dog -->

## Function dog

**dog\(noise='woof'\)**

Docstring for dog


### Parameters


**noise : str**

Noise that a dog makes.
\(Default value = "woof"\)


### Returns


**balls : int**

Number of tennis balls

**sticks : float**

Length of stick
<!-- create_documentation.py END Function dog -->


<!-- create_documentation.py START Object cat -->

## Object cat

**str**

Meow
<!-- create_documentation.py END Object cat -->

```


Each function, class or object has been given it's own set of
`create_documentation.py START` and `create_documentation.py END` html comments
and the documentation for the function has been written between them.
These form tags for the corresponding section of the documentation file.
These will not be visible when the file is viewed on gitlab.
The blank line below each html comment must be respected.

If the `create_documentation.update_docs` is run again,
it will apply changes found in `animal_example.py` to the
documentation file automatically, searching for existing `create_documentation.py START`
and `create_documentation.py END` tags and replacing the content between them.
If a pair of tags is not found, the new function, object or class will be
appended to the documentation with new tags.

It is possible to add to this documentation file manually,
and provided the additions are not between a
`create_documentation.py START` and `create_documentation.py END`
html comment, they will not be overwritten.

It is also possible to rearrange the order of the tags in the file
and the new order will be respected by subsequent calls to `create_documentation.update_docs`.


<!-- create_documentation.py END Module create documentation -->


-----
-----

This should be everything needed for basic use of the package.
Unless you are looking to develop the package, or debug bad behaviour,
you probably don't need anything below this point.



## Doctrings of functions and classes

<!-- create_documentation.py START Class BreakDiscalimer -->

## Class BreakDiscalimer

Represents a special part of the docfile that seperates
general information about the module from docstrings for the minuta.
Gets converted to a string then written to file.


### Attributes


**placement : int**

Gives the intended index of this object relative to other docstrings.
The default value, 1, indicates that this can be placed right after
the module docstring, unless it's tags already exist.
Value is ignored if document already contains tags for this docstring.


**title : str**

Title of this object.
    

## Class function BreakDiscalimer.\_\_init\_\_

**\_\_init\_\_\(self, \*args, \*\*kwargs\)**

Initialize self.  See help\(type\(self\)\) for accurate signature.
<!-- create_documentation.py END Class BreakDiscalimer -->


<!-- create_documentation.py START Class ClassDocstring -->

## Class ClassDocstring

Contains information relating to the docstring of a single class,
not including contained functions asside from `__init__`.
Would typically contain attribute information, becuase attribute information
should be written in the class docstring according to numpydoc standards.
Inherits from Docstring.


### Attributes


**placement : int**

Gives the intended index of this docstring relative to other docstrings.
The default value, \-1, indicates that this can be placed at the end
of the docfile, unless it's tags already exist.
Value is ignored if document already contains tags for this docstring.
    

## Class function ClassDocstring.\_\_init\_\_

**\_\_init\_\_\(self, title, numpydoc\_text, \*\*additional\_attributes\)**

Docstring initialisation.


### Parameters


**title : str**

Title attribute, which will be displayed literally
with all special chars, by processing with
Docstring.sanatise\_for\_markdown.

**numpydoc\_text : str**

The body of the docstring. Any common indent will be removed,
then it will be stored as a list of lines

**\*\*additional\_attributes : dict**

anything else that should be stored, will be stored as attributes.

## Class function ClassDocstring.class\_functions

**class\_functions\(cls, to\_read\)**

Find all the functions and methods in this class \(not attributes\).


### Parameters


**to\_read : class**

The class whose functions we need.


### Returns


**functions : list of callables**

The functions found.

## Class function ClassDocstring.class\_name

**class\_name\(cls, to\_read\)**

Get the name for this class.


### Parameters


**to\_read : class**

The class whose name we need.


### Returns


**name : str**

The name of the class

## Class function ClassDocstring.get\_indent

**get\_indent\(cls, lines\)**

Find the greatest common number of leading spaces for a list of lines.


### Parameters


**lines : list of str**

text to be analysed


### Returns


**: int**

number of leading spaces common to all lines

## Class function ClassDocstring.is\_attribute

**is\_attribute\(cls, line\)**

Check if the given line could be an
attribute line in a numpy doc. Attribute lines
optionally name, and always give the class of an
object, with a colon seperating the two properties.


### Parameters


**line : str**

Line to check.


### Returns


**: bool**

True if the line looks like an attribute line.

## Class function ClassDocstring.is\_exception

**is\_exception\(cls, line\)**

Check if the given line could be an
exception line in a numpy doc. Exception lines
contain only the name of an exception.


### Parameters


**line : str**

Line to check.


### Returns


**: bool**

True if the line looks like an exception line.

## Class function ClassDocstring.modify\_func\_docstring

**modify\_func\_docstring\(self, function\_docstring\)**

Classes often contain functions, given the docstring of a
function that came from the class described by this docstring,
modify the functions docstring to indicate it's membership.


### Parameters


**function\_docstring : FunctionDocstring**

Docstring that describes a function which is a member
of the class corrisponding to this docstring.
Will be modified in place.

## Class function ClassDocstring.numpydoc\_to\_markdown

**numpydoc\_to\_markdown\(self, title\_level=3\)**

Produce a markdown version of the numpydoc\_lines stored in this class.

Takes a number of formatting decisions designed to render numpydocs nicely
into markdown. The includes inserting titles, ensuring code is surrounded
by formatting tags, and bolding various key lines. With very few exceptions,
it is assumed that all charicters in the numpydoc shoudl be prepresented literally.
Things that are not interpreted literally are backticks for code, and indetation.
Any code that is surrounded by single backticks must be all on one line,
the alternative is too error prone to interpret.


### Parameters


**title\_level : int**

Number of hashes to prepend to the largest titles in this section,
larger values result in smaller titles.


### Returns


**markdown : str**

The resulting markdown representation of numpydoc\_lines.
    

## Class function ClassDocstring.read\_from\_class

**read\_from\_class\(cls, to\_read\)**

Alternative constructor for this docstring, using a class object.


### Parameters


**to\_read : class**

The python class to be documented


### Returns


**: ClassDocstring**

The docstring representaiton

## Class function ClassDocstring.sanatise\_for\_markdown

**sanatise\_for\_markdown\(cls, line\)**

Convert the line into something that will be
displayed literally in markdown


### Parameters


**line : str**

text to be processed


### Returns


**line : str**

input text represented so that all chars
are displayed litteraly in markdown
<!-- create_documentation.py END Class ClassDocstring -->


<!-- create_documentation.py START Class Docstring -->

## Class Docstring

General object to represent the information relating
to a component of documentation.
This is intented as a base class, carrying functions general to all Docstrings,
from which more specific Docstring types can be created.
Gets converted to a string then written to file.


### Attributes


**placement : int**

Gives the intended index of this docstring relative to other docstrings.
The default value, \-1, just places the docstring at the end of the document.
Value is ignored if document already contains tags for this docstring.
    

## Class function Docstring.\_\_init\_\_

**\_\_init\_\_\(self, title, numpydoc\_text, \*\*additional\_attributes\)**

Docstring initialisation.


### Parameters


**title : str**

Title attribute, which will be displayed literally
with all special chars, by processing with
Docstring.sanatise\_for\_markdown.

**numpydoc\_text : str**

The body of the docstring. Any common indent will be removed,
then it will be stored as a list of lines

**\*\*additional\_attributes : dict**

anything else that should be stored, will be stored as attributes.

## Class function Docstring.get\_indent

**get\_indent\(cls, lines\)**

Find the greatest common number of leading spaces for a list of lines.


### Parameters


**lines : list of str**

text to be analysed


### Returns


**: int**

number of leading spaces common to all lines

## Class function Docstring.is\_attribute

**is\_attribute\(cls, line\)**

Check if the given line could be an
attribute line in a numpy doc. Attribute lines
optionally name, and always give the class of an
object, with a colon seperating the two properties.


### Parameters


**line : str**

Line to check.


### Returns


**: bool**

True if the line looks like an attribute line.

## Class function Docstring.is\_exception

**is\_exception\(cls, line\)**

Check if the given line could be an
exception line in a numpy doc. Exception lines
contain only the name of an exception.


### Parameters


**line : str**

Line to check.


### Returns


**: bool**

True if the line looks like an exception line.

## Class function Docstring.numpydoc\_to\_markdown

**numpydoc\_to\_markdown\(self, title\_level=3\)**

Produce a markdown version of the numpydoc\_lines stored in this class.

Takes a number of formatting decisions designed to render numpydocs nicely
into markdown. The includes inserting titles, ensuring code is surrounded
by formatting tags, and bolding various key lines. With very few exceptions,
it is assumed that all charicters in the numpydoc shoudl be prepresented literally.
Things that are not interpreted literally are backticks for code, and indetation.
Any code that is surrounded by single backticks must be all on one line,
the alternative is too error prone to interpret.


### Parameters


**title\_level : int**

Number of hashes to prepend to the largest titles in this section,
larger values result in smaller titles.


### Returns


**markdown : str**

The resulting markdown representation of numpydoc\_lines.
    

## Class function Docstring.sanatise\_for\_markdown

**sanatise\_for\_markdown\(cls, line\)**

Convert the line into something that will be
displayed literally in markdown


### Parameters


**line : str**

text to be processed


### Returns


**line : str**

input text represented so that all chars
are displayed litteraly in markdown
<!-- create_documentation.py END Class Docstring -->


<!-- create_documentation.py START Class DocstringsHeader -->

## Class DocstringsHeader

Represents a special part of the docfile that opens the section
on the minuta of the module.
Gets converted to a string then written to file.


### Attributes


**placement : int**

Gives the intended index of this object relative to other docstrings.
The default value, 2, indicates that this can be placed after the BreakDiscalimer
and before the classes, functions and objects, unless it's tags already exist.
Value is ignored if document already contains tags for this docstring.


**title : str**

Title of this object.
    

## Class function DocstringsHeader.\_\_init\_\_

**\_\_init\_\_\(self, \*args, \*\*kwargs\)**

Initialize self.  See help\(type\(self\)\) for accurate signature.
<!-- create_documentation.py END Class DocstringsHeader -->


<!-- create_documentation.py START Class FunctionDocstring -->

## Class FunctionDocstring

Contains information relating to the docstring of a single function.
Inherits from Docstring.


### Attributes


**placement : int**

Gives the intended index of this docstring relative to other docstrings.
The default value, \-1, indicates that this can be placed at the end
of the docfile, unless it's tags already exist.
Value is ignored if document already contains tags for this docstring.
    

## Class function FunctionDocstring.\_\_init\_\_

**\_\_init\_\_\(self, title, numpydoc\_text, \*\*additional\_attributes\)**

Docstring initialisation.


### Parameters


**title : str**

Title attribute, which will be displayed literally
with all special chars, by processing with
Docstring.sanatise\_for\_markdown.

**numpydoc\_text : str**

The body of the docstring. Any common indent will be removed,
then it will be stored as a list of lines

**\*\*additional\_attributes : dict**

anything else that should be stored, will be stored as attributes.

## Class function FunctionDocstring.function\_name

**function\_name\(cls, to\_read\)**

Get the name of the function.


### Parameters


**to\_read : callable**

The function we need a name for


### Returns


**name : str**

Name for the function

## Class function FunctionDocstring.function\_signature

**function\_signature\(cls, to\_read\)**

Given a function, write it's signature.
Note this will include `cls` or `self` for functions in classes.


### Parameters


**to\_read : callable**

The function we need a signature for


### Returns


**: str**

Function signature formatted for markdown.

## Class function FunctionDocstring.get\_indent

**get\_indent\(cls, lines\)**

Find the greatest common number of leading spaces for a list of lines.


### Parameters


**lines : list of str**

text to be analysed


### Returns


**: int**

number of leading spaces common to all lines

## Class function FunctionDocstring.is\_attribute

**is\_attribute\(cls, line\)**

Check if the given line could be an
attribute line in a numpy doc. Attribute lines
optionally name, and always give the class of an
object, with a colon seperating the two properties.


### Parameters


**line : str**

Line to check.


### Returns


**: bool**

True if the line looks like an attribute line.

## Class function FunctionDocstring.is\_exception

**is\_exception\(cls, line\)**

Check if the given line could be an
exception line in a numpy doc. Exception lines
contain only the name of an exception.


### Parameters


**line : str**

Line to check.


### Returns


**: bool**

True if the line looks like an exception line.

## Class function FunctionDocstring.numpydoc\_to\_markdown

**numpydoc\_to\_markdown\(self, title\_level=3\)**

Produce a markdown version of the numpydoc\_lines stored in this class.

Takes a number of formatting decisions designed to render numpydocs nicely
into markdown. The includes inserting titles, ensuring code is surrounded
by formatting tags, and bolding various key lines. With very few exceptions,
it is assumed that all charicters in the numpydoc shoudl be prepresented literally.
Things that are not interpreted literally are backticks for code, and indetation.
Any code that is surrounded by single backticks must be all on one line,
the alternative is too error prone to interpret.


### Parameters


**title\_level : int**

Number of hashes to prepend to the largest titles in this section,
larger values result in smaller titles.


### Returns


**markdown : str**

The resulting markdown representation of numpydoc\_lines.
    

## Class function FunctionDocstring.read\_from\_function

**read\_from\_function\(cls, to\_read\)**

Alternative constructor for this docstring, using a function object.


### Parameters


**to\_read : callable**

The python function to be documented


### Returns


**: FunctionDocstring**

The docstring representaiton

## Class function FunctionDocstring.sanatise\_for\_markdown

**sanatise\_for\_markdown\(cls, line\)**

Convert the line into something that will be
displayed literally in markdown


### Parameters


**line : str**

text to be processed


### Returns


**line : str**

input text represented so that all chars
are displayed litteraly in markdown
<!-- create_documentation.py END Class FunctionDocstring -->


<!-- create_documentation.py START Class ModuleDocstring -->

## Class ModuleDocstring

Contains information relating to the docstring of the module itself.
Inherits from Docstring.


### Attributes


**placement : int**

Gives the intended index of this docstring relative to other docstrings.
The default value, 0, becuase the Moduledocstring usually want to be the first docstring
in the documentation.
Value is ignored if document already contains tags for this docstring.
    

## Class function ModuleDocstring.\_\_init\_\_

**\_\_init\_\_\(self, title, numpydoc\_text, \*\*additional\_attributes\)**

Docstring initialisation.


### Parameters


**title : str**

Title attribute, which will be displayed literally
with all special chars, by processing with
Docstring.sanatise\_for\_markdown.

**numpydoc\_text : str**

The body of the docstring. Any common indent will be removed,
then it will be stored as a list of lines

**\*\*additional\_attributes : dict**

anything else that should be stored, will be stored as attributes.

## Class function ModuleDocstring.get\_indent

**get\_indent\(cls, lines\)**

Find the greatest common number of leading spaces for a list of lines.


### Parameters


**lines : list of str**

text to be analysed


### Returns


**: int**

number of leading spaces common to all lines

## Class function ModuleDocstring.is\_attribute

**is\_attribute\(cls, line\)**

Check if the given line could be an
attribute line in a numpy doc. Attribute lines
optionally name, and always give the class of an
object, with a colon seperating the two properties.


### Parameters


**line : str**

Line to check.


### Returns


**: bool**

True if the line looks like an attribute line.

## Class function ModuleDocstring.is\_exception

**is\_exception\(cls, line\)**

Check if the given line could be an
exception line in a numpy doc. Exception lines
contain only the name of an exception.


### Parameters


**line : str**

Line to check.


### Returns


**: bool**

True if the line looks like an exception line.

## Class function ModuleDocstring.numpydoc\_to\_markdown

**numpydoc\_to\_markdown\(self, title\_level=3\)**

Produce a markdown version of the numpydoc\_lines stored in this class.

Takes a number of formatting decisions designed to render numpydocs nicely
into markdown. The includes inserting titles, ensuring code is surrounded
by formatting tags, and bolding various key lines. With very few exceptions,
it is assumed that all charicters in the numpydoc shoudl be prepresented literally.
Things that are not interpreted literally are backticks for code, and indetation.
Any code that is surrounded by single backticks must be all on one line,
the alternative is too error prone to interpret.


### Parameters


**title\_level : int**

Number of hashes to prepend to the largest titles in this section,
larger values result in smaller titles.


### Returns


**markdown : str**

The resulting markdown representation of numpydoc\_lines.
    

## Class function ModuleDocstring.read\_from\_file

**read\_from\_file\(cls, file\_path\)**

Alternative constructor for this docstring, using a filepath name.

Getting the module's docstring from the module object itself using inspect
is unreliable. Inspect assumes that the module docstring is the very first
thing in the file, but unfortunatly future imports must go first.
Instead, this method actually reads the file content and identifies the
module docstring as the first multiline string encountered before
any class or function is defined.


### Parameters


**file\_path : str**

Path to the module file


### Returns


**: ModuleDocstring**

The docstring representaiton

## Class function ModuleDocstring.sanatise\_for\_markdown

**sanatise\_for\_markdown\(cls, line\)**

Convert the line into something that will be
displayed literally in markdown


### Parameters


**line : str**

text to be processed


### Returns


**line : str**

input text represented so that all chars
are displayed litteraly in markdown
<!-- create_documentation.py END Class ModuleDocstring -->


<!-- create_documentation.py START Class ObjectDocstring -->

## Class ObjectDocstring

Contains information relating to the docstring of a single object.
To be used for things in modules that are neither classes nor functions.
Inherits from Docstring.


### Attributes


**placement : int**

Gives the intended index of this docstring relative to other docstrings.
The default value, \-1, indicates that this can be placed at the end
of the docfile, unless it's tags already exist.
Value is ignored if document already contains tags for this docstring.
    

## Class function ObjectDocstring.\_\_init\_\_

**\_\_init\_\_\(self, title, numpydoc\_text, \*\*additional\_attributes\)**

Docstring initialisation.


### Parameters


**title : str**

Title attribute, which will be displayed literally
with all special chars, by processing with
Docstring.sanatise\_for\_markdown.

**numpydoc\_text : str**

The body of the docstring. Any common indent will be removed,
then it will be stored as a list of lines

**\*\*additional\_attributes : dict**

anything else that should be stored, will be stored as attributes.

## Class function ObjectDocstring.get\_indent

**get\_indent\(cls, lines\)**

Find the greatest common number of leading spaces for a list of lines.


### Parameters


**lines : list of str**

text to be analysed


### Returns


**: int**

number of leading spaces common to all lines

## Class function ObjectDocstring.is\_attribute

**is\_attribute\(cls, line\)**

Check if the given line could be an
attribute line in a numpy doc. Attribute lines
optionally name, and always give the class of an
object, with a colon seperating the two properties.


### Parameters


**line : str**

Line to check.


### Returns


**: bool**

True if the line looks like an attribute line.

## Class function ObjectDocstring.is\_exception

**is\_exception\(cls, line\)**

Check if the given line could be an
exception line in a numpy doc. Exception lines
contain only the name of an exception.


### Parameters


**line : str**

Line to check.


### Returns


**: bool**

True if the line looks like an exception line.

## Class function ObjectDocstring.numpydoc\_to\_markdown

**numpydoc\_to\_markdown\(self, title\_level=3\)**

Produce a markdown version of the numpydoc\_lines stored in this class.

Takes a number of formatting decisions designed to render numpydocs nicely
into markdown. The includes inserting titles, ensuring code is surrounded
by formatting tags, and bolding various key lines. With very few exceptions,
it is assumed that all charicters in the numpydoc shoudl be prepresented literally.
Things that are not interpreted literally are backticks for code, and indetation.
Any code that is surrounded by single backticks must be all on one line,
the alternative is too error prone to interpret.


### Parameters


**title\_level : int**

Number of hashes to prepend to the largest titles in this section,
larger values result in smaller titles.


### Returns


**markdown : str**

The resulting markdown representation of numpydoc\_lines.
    

## Class function ObjectDocstring.object\_signature

**object\_signature\(cls, to\_read\)**

Given an object, write it's signature.


### Parameters


**to\_read : object**

The function we need a signature for


### Returns


**: str**

Object signature formatted for markdown.

## Class function ObjectDocstring.read\_from\_module

**read\_from\_module\(cls, object\_name, module\)**

Alternative constructor for this docstring,
using the module and object name.

Cannot construct form the object itself as objects
don't know their own name.


### Parameters


**object\_name : str**

The variable name of the object to be documented


**module : module**

The module in which the object can be found.


### Returns


**: ObjectDocstring**

The docstring representaiton

## Class function ObjectDocstring.sanatise\_for\_markdown

**sanatise\_for\_markdown\(cls, line\)**

Convert the line into something that will be
displayed literally in markdown


### Parameters


**line : str**

text to be processed


### Returns


**line : str**

input text represented so that all chars
are displayed litteraly in markdown
<!-- create_documentation.py END Class ObjectDocstring -->


<!-- create_documentation.py START Function find\_docs -->

## Function find\_docs

**find\_docs\(module\)**

Given a python module, get the path to it's documentation,
if it has a `__documentation_file` variable, use that, otherwise
put it at the same path as it's code, with a markdown extention,
`.md`.


### Parameters


**module : module**

The python module object whoes documentation should be located.


### Returns


**docs : str**

Absolute path of the module's documentaion
<!-- create_documentation.py END Function find\_docs -->


<!-- create_documentation.py START Function get\_module\_filepath -->

## Function get\_module\_filepath

**get\_module\_filepath\(module\)**

Given a python module, get it's path on disk.


### Parameters


**module : module**

The python module object whoes file should be located.


### Returns


**path : str**

Absolute path of the module's code
<!-- create_documentation.py END Function get\_module\_filepath -->


<!-- create_documentation.py START Function harvest\_module -->

## Function harvest\_module

**harvest\_module\(module\)**

Find and classify all top level content in a module.

Sorts items into functions, classes and, where neither of
thos catagories apply, objects.
Skips anything with a leading underscore, so classes functions
imports or variables can be kept out the documentation the
same way they are kept out the default namespace,
by naming them with a leading underscore.

So this import;


```python
import os
```


would result in `os` being included in the docs but


```python
import os as _os
```


would not result in `_os` being in the docs, and has the
advantage that `os` will not polute the namespace normally
accessed by tab completion.


### Parameters


**module : module**

The python module object to be documented


### Returns


**classes : list of classes**

Classes found at top level.

**functions : list of callables**

Callabels that are not classes found at top level.

**objects : list of objects**

Anything else at top level that is neither a class nor
a callable.
<!-- create_documentation.py END Function harvest\_module -->


<!-- create_documentation.py START Function make\_tags -->

## Function make\_tags

**make\_tags\(docstring\)**

Given an object that represetns a docstring, design the tags
that should identify it's start and end in a documentation file.

Allows the documentation file to be updated when the docstrings change
without losing information about docstring order or information written
outside of docstrings.


### Parameters


**docstring : Docstring**

Object derived from Docstring, that should be written or updated in
a docuemntation file


### Returns


**start\_tag : str**

An html comment that is placed before the start of the text of this
documentation section.

**end\_tag : str**

An html comment that is placed after the end of the text of this
documentation section.
<!-- create_documentation.py END Function make\_tags -->


<!-- create_documentation.py START Function update\_docs -->

## Function update\_docs

**update\_docs\(module\)**

Given a module, find it's docuemntation file and update it
with the current docstrings for the module itself, all functions,
classes and objects inside the module.

Respects any tags that already exist in the module, and
inserts objects for which tags cannot be found.


### Parameters


**module : module**

Python module object of the module to be updated.
<!-- create_documentation.py END Function update\_docs -->


<!-- create_documentation.py START Object absolute\_import -->

## Object absolute\_import

**_Feature**

\_Feature\(\(2, 5, 0, 'alpha', 1\), \(3, 0, 0, 'alpha', 0\), 262144\)
<!-- create_documentation.py END Object absolute\_import -->


<!-- create_documentation.py START Object doc\_delimiator -->

## Object doc\_delimiator

**str**


<!-- create_documentation.py END Object doc\_delimiator -->


<!-- create_documentation.py START Object markdown\_comment\_close -->

## Object markdown\_comment\_close

**str**

\-\->
<!-- create_documentation.py END Object markdown\_comment\_close -->


<!-- create_documentation.py START Object markdown\_comment\_open -->

## Object markdown\_comment\_open

**str**

<\!\-\-
<!-- create_documentation.py END Object markdown\_comment\_open -->


<!-- create_documentation.py START Object tag\_beggining -->

## Object tag\_beggining

**str**


<\!\-\- create\_documentation.py
<!-- create_documentation.py END Object tag\_beggining -->


<!-- create_documentation.py START Object tag\_ending -->

## Object tag\_ending

**str**

\-\->


<!-- create_documentation.py END Object tag\_ending -->


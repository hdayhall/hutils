import sys, os
package_dir = os.path.join(os.path.dirname(__file__), "../..")
sys.path.insert(0, package_dir)
from hutils import dsid

def test_eta_range_to_tuple():
    found = dsid.eta_range_to_tuple("m25_m20_20_25")
    assert found == (-25, -20, 20, 25)
    found = dsid.eta_range_to_tuple("20_25")
    assert found == (20, 25)
    found = dsid.eta_range_to_tuple("m20_m15_2_5")
    assert found == (-20, -15, 2, 5)


def test_eta_range_to_string():
    found = dsid.eta_range_to_string((-25, -20, 20, 25))
    assert found == "m25_m20_20_25"
    found = dsid.eta_range_to_string((20, 25), force_long=False)
    assert found == "20_25"
    found = dsid.eta_range_to_string((20, 25), force_long=True)
    assert found == "m25_m20_20_25"
    found = dsid.eta_range_to_string((-20, -15, 2, 5))
    assert found == "m20_m15_2_5"

unstrict_mappings =   {"particle_E_energy_eta_eta_range_zz0": ("particle", "energy", "eta_range", "z0"),
                       "pids11_E22_2220_eta_20-25_z0p0": ("pids11", "22_2220", "20-25", .0),}


def test_make_DSID():
    mappings = {"pid11_E22_2220_eta_m25_m20_20_25_z0": ("pid11", "22_2220", (20, 25), 0),
                "electrons_E6p1_eta_20_z0": (11, 6.1, "20", "0"),
                "photons_E1_eta_m25_m20_20_25_z0": ("photons", 1, (20, 25), 0),
                "photons_E100_eta_m35_m30_20_30_z1": ("photons", 100, (-35, -30, 20, 30), 1)}
    mappings.update(unstrict_mappings)
    for string, components in mappings.items():
        found = dsid.make_DSID(*components)
        assert found == string, "Expected = {}, found = {}".format(string, found)


def test_decode_DSID():
    mixed_mappings  = {"pid11_E22_2220_eta_20_25_z0": ("electrons", "22_2220", (20, 25), 0),
                       "electrons_E6p1_eta_20_z0": ("electrons", 6.1, "20", 0)}
    mixed_mappings.update(unstrict_mappings)
    for string, components in mixed_mappings.items():
        found = dsid.decode_DSID(string, strict=False)
        assert found == components, "expected {}, found {}".format(components, found)
        try:
            dsid.decode_DSID(string, strict=True)
        except ValueError:
            pass  # should be thrown
        else:
            assert False, "Strict parsing of {} should fail".format(string)
    strict_mappings = {"photons_E1_eta_20_25_z0": ("photons", 1, (20, 25), 0),
                       "photons_E100_eta_m35_m30_20_30_z1": ("photons", 100, (-35, -30, 20, 30), 1)}
    for string, components in strict_mappings.items():
        found = dsid.decode_DSID(string, strict=True)
        assert found == components, "expected {}, found {}".format(components, found)


if __name__ == "__main__":
    from hutils.test.utils_for_testing import run_tests
    run_tests(globals())


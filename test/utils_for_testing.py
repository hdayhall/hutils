import os, io, sys
try:
    FileExistsError  # defined in python3
except NameError:
    # not defined in python2
    FileExistsError = OSError


def run_tests(dict_containing_tests, report=True):
    passing = []
    failing = []
    for name, func in dict_containing_tests.items():
        if name.startswith("test_") and hasattr(func, "__call__"):
            try:
                func()
            except Exception as e:
                failing.append(name)
                print("\n{}\n{}".format(name, e))
            else:
                passing.append(name)
    if report:
        print(make_report(passing, failing))
    return passing, failing


def make_report(passing, failing):
    report = ""
    if passing:
        report += "{} tests pass;\n{}\n\n".format(len(passing), passing)
    else:
        report += "No tests fail\n\n"
    if failing:
        report += "{} tests fail;\n{}\n".format(len(failing), failing)
    else:
        report += "No tests fail\n"
    return report


# context manager for test directory
class TempTestDir:
    def __init__(self, base_name="temp_test_dir_"):
        self.base_name = base_name
        self.num = 1

    @property
    def dir_name(self):
        return "{}{}".format(self.base_name, self.num)

    def __enter__(self):
        made_dir = False
        while not made_dir:
            try:
                os.makedirs(self.dir_name)
                made_dir = True
            except FileExistsError:
                self.num += 1
        return self.dir_name

    def __exit__(self, *args):
        # this does not fail if there is a locked/read only
        # file left behind
        import shutil
        shutil.rmtree(self.dir_name, ignore_errors=True)


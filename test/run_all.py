if __name__ == "__main__":
    import os, sys
    # ensure it works no matter when you call it from
    this_dir = os.path.dirname(__file__)
    sys.path.insert(0, this_dir)

    import importlib
    from utils_for_testing import run_tests, make_report
    results_by_module = {}
    for name in os.listdir(this_dir):
        if name.startswith("test_") and name.endswith(".py"):
            name = name[:-3]  # trim off .py
            print(name)
            # it's a tsting module
            module = importlib.import_module(name)
            passing, failing = run_tests(vars(module), report=False)
            results_by_module[name] = make_report(passing, failing)
    print("Findings; ")
    for name, results in results_by_module.items():
        print(name)
        print(results)

    

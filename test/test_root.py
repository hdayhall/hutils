import sys, os
package_dir = os.path.join(os.path.dirname(__file__), "../..")
sys.path.insert(0, package_dir)
from hutils import root
# prevent root from trying to open windows
import ROOT
ROOT.gROOT.SetBatch(True)

def test_open_tfile():
    from hutils.test.utils_for_testing import TempTestDir
    with TempTestDir() as temp_dir_name:
        file_path = os.path.join(temp_dir_name, "temp.root")
        try:
            # default mode is READ
            open_tfile = root.open_tfile(file_path)
        except OSError:
            pass
        else:
            assert False, file_path + \
                " dosn't yet exist, so opening to read should be an error"
        open_tfile = root.open_tfile(file_path, 'NEW')
        assert open_tfile.IsWritable()
        # if we save this we should be able to open it again for reading
        open_tfile.SaveSelf()
        # now again, jsut for reading
        open_tfile2 = root.open_tfile(file_path)
        assert not open_tfile2.IsWritable()
        open_tfile.Close()
        open_tfile2.Close()


def test_recursive_get_keys():
    intended_structure = {"treeA": {}}  # more complex structures
    # don't get made by pyroot
    from hutils.test.utils_for_testing import TempTestDir
    with TempTestDir() as temp_dir_name:
        file_path = os.path.join(temp_dir_name, "temp.root")
        open_tfile = root.open_tfile(file_path, 'NEW')
        tree = root.ROOT.TTree("treeA", "My new tree")
        open_tfile.cd()
        tree.Fill()
        open_tfile.Write()
        found = root.recursive_get_keys(open_tfile)
        assert len(found) == 1
        assert "treeA" in found
        open_tfile.Close()
        


# easiest to test these two together
def test_read_write_canvas():
    from hutils.test.utils_for_testing import TempTestDir
    with TempTestDir() as temp_dir_name:
        # make the file
        file_path = os.path.join(temp_dir_name, "temp.root")
        #root_file = root.open_tfile(file_path, "NEW")
        root_file = root.ROOT.TFile.Open(file_path, "NEW")
        root_file.Write()
        root_file.Close()
        # it shoud split and rejoin lines;
        title = "haphazardmelody"
        sample_text = "May life whisper\njoy through your veins\nbefore lidding your eyes."
        root.write_canvas_text(file_path, title, sample_text)
        found = root.read_canvas_text(file_path, title)
        assert found == sample_text
        # also check it wont choke on an empty sample;
        title = "empty"
        sample_text = ""
        root.write_canvas_text(file_path, title, sample_text)
        found = root.read_canvas_text(file_path, title)
        assert found == sample_text


# easiest to test these two together
def test_has_canvas():
    from hutils.test.utils_for_testing import TempTestDir
    with TempTestDir() as temp_dir_name:
        # make the file
        file_path = os.path.join(temp_dir_name, "temp.root")
        #root_file = root.open_tfile(file_path, "NEW")
        root_file = root.ROOT.TFile.Open(file_path, "NEW")
        root_file.Write()
        root_file.Close()
        # now check the canvas is correclty detected
        title = "haphazardmelody"
        sample_text = "May life whisper\njoy through your veins\nbefore lidding your eyes."
        # before adding canvas
        assert not root.has_canvas(file_path, title)
        root.write_canvas_text(file_path, title, sample_text)
        # after adding canvas
        assert root.has_canvas(file_path, title)
        # with wrong name
        assert not root.has_canvas(file_path, "wrong")


def test_append_canvas_text():
    from hutils.test.utils_for_testing import TempTestDir
    with TempTestDir() as temp_dir_name:
        # make the file
        file_path = os.path.join(temp_dir_name, "temp.root")
        #root_file = root.open_tfile(file_path, "NEW")
        root_file = root.ROOT.TFile.Open(file_path, "NEW")
        root_file.Write()
        root_file.Close()
        # inital text
        title = "haphazardmelody"
        sample_text = "May life whisper\njoy through your veins"
        root.write_canvas_text(file_path, title, sample_text)
        # append anouther line
        next_line = "before lidding your eyes."
        root.append_canvas_text(file_path, title, next_line)
        # read it back
        found = root.read_canvas_text(file_path, title)
        assert found == '\n'.join([sample_text, next_line])


if __name__ == "__main__":
    from hutils.test.utils_for_testing import run_tests
    run_tests(globals())


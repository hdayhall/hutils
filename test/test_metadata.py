import sys, os
package_dir = os.path.join(os.path.dirname(__file__), "../..")
sys.path.insert(0, package_dir)
from hutils import metadata
# prevent root from trying to open windows
import ROOT
ROOT.gROOT.SetBatch(True)

def test_copy_script_location():
    location = metadata.copy_script_location()
    assert os.path.exists(location)


def test_SampleMetadata_init_getitem_len():
    # check init, len and getitem for items that meet the criteria of strict
    strict_params = [(dict(), 'same'),
                     (dict(energy=1), 'same'),
                     (dict(energy="1"), dict(energy=1)),
                     (dict(sample_name="foo", simulation_type="blas",
                           simulation_release="cobble", EVNT_location="dog",
                           xAOD_location="cat", plots_folder="tasty",
                           base_dir="fog"), "don't compare"),
                     (dict(eta_range=(20, 25)), dict(eta_range=(20, 25))),
                     (dict(eta_range=(-25, -20, 20, 25)), 'same'),
                     (dict(eta_range="m25_m20_20_25"), dict(eta_range=(-25, -20, 20, 25))),
                     (dict(zv=0), 'same'),
                     (dict(zv="0"), dict(zv=0)),
                     (dict(particle_content="photons"), 'same'),
                     (dict(particle_content="higgsyy"), 'same'),
                     (dict(particle_content="pid22"), dict(particle_content='photons'))]
    with_defaults = {"sample_name": "unnamed", "zv": 0}
    for params, expected_attrs in strict_params:
        sample1 = metadata.SampleMetadata(**params)
        sample2 = metadata.SampleMetadata(strict=True, **params)
        sample3 = metadata.SampleMetadata("Some notes", True, **params)
        samples = [sample1, sample2, sample3]
        if expected_attrs != "don't compare":
            if expected_attrs == 'same':
                expected_attrs = params
            for name, value in with_defaults.items():
                if name not in expected_attrs: expected_attrs[name] = value
            for key, value in expected_attrs.items():
                for sample in samples:
                    assert sample[key] == value, \
                        "expected {}, got {}".format(value, sample[key])
        # plus one is for the date
        num_expected = len(params) + len([name for name in with_defaults
                                          if name not in params]) + 1
        for sample in samples:
            assert num_expected == len(sample), \
                "Expected {}, but found keys {}".format(expected_attrs, sample.keys())
        assert isinstance(sample["date"], str)
    # check init, getitem and len when strict criteria are not met
    unstrict_params = [(dict(energy=-1), 'same'),
                       (dict(energy="dog"), 'same'),
                       (dict(eta_range=(20, 25, 5)), 'same'),
                       (dict(energy="dog", eta_range="m25_m20_20_25"), dict(energy="dog", eta_range=(-25, -20, 20, 25))),
                       (dict(zv={}), dict(zv={})),
                       (dict(particle_content="photonsplus"), 'same'),
                       (dict(particle_content="pid2256"), dict(particle_content=2256))]
    with_defaults = {"sample_name": "unnamed", "zv": 0}
    for params, expected_attrs in unstrict_params:
        sample = metadata.SampleMetadata(strict=False, **params)
        if expected_attrs == 'same':
            expected_attrs = params
        for name, value in with_defaults.items():
            if name not in expected_attrs: expected_attrs[name] = value
        for key, value in expected_attrs.items():
            assert sample[key] == value, \
                "expected {}, got {}".format(value, sample[key])
        # plus one is for the date
        num_expected = len(params) + len([name for name in with_defaults
                                          if name not in params]) + 1
        assert num_expected == len(sample), \
            "Expected {}, but found keys {}".format(expected_attrs, sample.keys())
        try:
            metadata.SampleMetadata(strict=True, **params)
        except ValueError:
            pass  # should fail
        else:
            assert False, "Expect params {} to fail under strict".format(params)


def test_SampleMetadata_get_eq_ne_strict():
    # check the get method
    empty = metadata.SampleMetadata()
    something = object()
    assert empty.get("dontHaveThis", something) == something
    # check the deepcopy method
    simple = metadata.SampleMetadata(sample_name="MySample",
                                     simulation_type="simType",
                                     simulation_release="simRel",
                                     particle_content="pions",
                                     energy=1,
                                     eta_range=(20, 25),
                                     zv=0,
                                     EVNT_location="somewhere.root",
                                     xAOD_location="somewhere/else.root",
                                     flatTTree_location="/my/dir.root",
                                     plots_folder="/a/folder",
                                     base_dir="/sepreate/dir")
    import copy
    simple2 = copy.deepcopy(simple)
    assert simple == simple2
    assert simple != empty
    simple2["energy"] = 10
    assert simple != simple2
    # test set_without_check and the strict setter
    try:
        simple["particle_content"] = "fancytron"
    except ValueError:
        pass  # shouldn't be possible
    else:
        assert False, "setting particle_content to fancytron should raise an error"
    # but by specifically evaiding the checks
    simple.set_without_check(particle_content="fancytron")
    assert simple["particle_content"] == "fancytron"
    # can use it to avoid conversions
    simple["particle_content"]  = 22
    assert simple["particle_content"] == "photons"
    simple.set_without_check(particle_content=22)
    assert simple["particle_content"] == 22
    # now if we set strict, the setting proccess shoudl fix it
    simple.strict = True
    assert simple["particle_content"] == "photons"
    # but setting should fail if at least one attribute has a bad value
    simple2.set_without_check(particle_content="foggle")
    try:
        simple2.strict = True
    except ValueError:
        pass
    else:
        assert False, "Shouldn't be able to set this to strict"


def test_SampleMetadata_read_write():
    # compare three params
    empty = dict()
    strict = dict(sample_name="MySample",
                  simulation_type="simType",
                  simulation_release="simRel",
                  particle_content="pions",
                  energy=10,
                  eta_range=(-35, -30, 20, 25),
                  zv=0,
                  EVNT_location="somewhere.root",
                  xAOD_location="somewhere/else.root",
                  flatTTree_location="/my/dir.root",
                  plots_folder="/a/folder",
                  base_dir="/sepreate/dir")
    unstrict = dict(strict=False,
                    simulation_type="simType",
                    simulation_release="simRel",
                    particle_content="pions",
                    energy="energy",
                    eta_range="eta_range",
                    zv="zv",
                    EVNT_location="somewhere",)
    param_types = {"empty": empty, "strict": strict,
                   "unstrict": unstrict}
    from hutils.test.utils_for_testing import TempTestDir
    with TempTestDir() as temp_dir_name:
        file_name_base = os.path.join(temp_dir_name, "temp")
        for method in ["string", "text file", "root file"]:
            for param_name in param_types:
                message = "Working {} params, writing to {}"\
                        .format(param_name, method)
                params = param_types[param_name]
                strict = params.get("strict", True)
                sample = metadata.SampleMetadata(**params)
                if method == "string":
                    generated_str = str(sample)
                    assert isinstance(generated_str, str)
                    readback = metadata.SampleMetadata\
                        .read_string(generated_str, strict=strict)
                elif method == "text file":
                    file_name = file_name_base + ".txt"
                    sample.write_text_file(file_name)
                    readback = metadata.SampleMetadata.\
                        read_text_file(file_name, strict=strict)
                elif method == "root file":
                    file_name = file_name_base + ".root"
                    sample.write_root_file(file_name)
                    readback = metadata.SampleMetadata.\
                        read_root_file(file_name, strict=strict)
                else:
                    raise NotImplementedError
                assert sample == readback


def test_SampleMetadata_DSID():
    # shouldn't be able to get a DSID out of an empty metadata
    empty = metadata.SampleMetadata()
    try:
        dsid = empty.DSID
    except AttributeError:
        pass # expected
    else:
        assert False, "Shouldn't be able to get dsid, got " + dsid
    # shouldn't be able to get a dsid out of a sample with the wrong information
    inadiquate = metadata.SampleMetadata(sample_name="MySample",
                                         simulation_type="simType",
                                         simulation_release="simRel",
                                         particle_content="pions",
                                         eta_range=(20, 25),
                                         zv=0,
                                         EVNT_location="somewhere.root",
                                         xAOD_location="somewhere/else.root",
                                         flatTTree_location="/my/dir.root",
                                         plots_folder="/a/folder",
                                         base_dir="/sepreate/dir")
    try:
        dsid = inadiquate.DSID
    except AttributeError:
        pass # expected
    else:
        assert False, "Shouldn't be able to get dsid, got " + dsid
    # should have just enough
    simple = metadata.SampleMetadata(particle_content="pions",
                                     energy=1,
                                     eta_range=(20, 25),
                                     zv=0)
    dsid = simple.DSID
    assert dsid == "pions_E1_eta_m25_m20_20_25_z0"


def test_SampleMetadata_harvest():
    from hutils.test.utils_for_testing import TempTestDir
    with TempTestDir() as temp_dir_name:
        # with nothing in the metadata, all information should
        # come from the path
        empty = metadata.SampleMetadata()
        path = os.path.join(temp_dir_name, 
                            "base/dir/photons_E20_eta_m35_m30_30_35_z0/sample.root")
        # the directorys got to exist
        from hutils.general import ensure_folder_exists
        ensure_folder_exists(os.path.dirname(path))

        empty.write_root_file(path)
        harvested = metadata.SampleMetadata.harvest_root_path(path)
        expected = {"particle_content": "photons", "energy": 20,
                    "eta_range": (-35, -30, 30, 35), "zv": 0,
                    "sample_name": "unnamed"}
        # plus 1 for the date
        assert len(harvested) == len(expected) + 1
        for key, value in expected.items():
            assert harvested[key] == value, \
                "expected {}, found {}".format(value, harvested[key])
        # if the sample has data in it, that should overrrid information
        # in the path
        info_in_sample = dict(sample_name="MySample",
                              simulation_type="simType",
                              simulation_release="simRel",
                              particle_content="pions",
                              energy=1,
                              eta_range=(20, 25),
                              zv=1,
                              base_dir="/sepreate/dir")
        for key in info_in_sample:
            expected[key] = info_in_sample[key]
        sample = metadata.SampleMetadata(**info_in_sample)
        sample.write_root_file(path, overwrite=True)
        harvested = metadata.SampleMetadata.harvest_root_path(path)
        # plus 1 for the date
        assert len(harvested) == len(expected) + 1
        for key, value in expected.items():
            assert harvested[key] == value, \
                "expected {}, found {}".format(value, harvested[key])


def test_MultiSampleMetadata_init_set():
    # one on it's own with no parameters
    empty = metadata.MultiSampleMetadata()
    assert empty.num_samples == "unknown"
    assert len(empty.samples) == 1
    # check with just parameters for one specified
    params = dict(sample_name="MySample",
                  simulation_type="simType",
                  particle_content="pions",
                  energy=1,
                  eta_range=(-25, -20, 20, 25),
                  xAOD_location="/somewhere/else.root",
                  base_dir="/sepreate/dir")
    one = metadata.MultiSampleMetadata(**params)
    assert one.num_samples == "unknown"
    assert len(one.samples) == 1
    for key, value in params.items():
        assert one[key] == value, "expected {}, found {}".format(value, one[key])
        assert one.samples[0][key] == value
    # now make the params for two
    params["eta_range"] = "20_25...30_35"
    two = metadata.MultiSampleMetadata(**params)
    assert two.num_samples == 2, "Expected 2, found {}".format(two.num_samples)
    assert len(two.samples) == 2
    for key, value in params.items():
        if key == "eta_range": continue
        assert two[key] == value
        assert two.samples[0][key] == value
        assert two.samples[1][key] == value
    assert two["eta_range"] == params["eta_range"],\
        "expected {}, found {}".format(params["eta_range"], two["eta_range"])
    sample0_eta_range = two.samples[0]["eta_range"]
    sample1_eta_range = two.samples[1]["eta_range"]
    assert sample0_eta_range == (20, 25)
    assert sample1_eta_range == (30, 35)
    # try a set_for_all_samples
    for multi_sample in [one, two]:
        multi_sample.set_for_all_samples("energy", 3)
        assert multi_sample["energy"] == 3
        for sample in multi_sample.samples:
            assert sample["energy"] == 3
    # just setting the value of the whole sample should work as well
    for multi_sample in [one, two]:
        multi_sample["energy"] = 4
        assert multi_sample["energy"] == 4
        for sample in multi_sample.samples:
            assert sample["energy"] == 4
    # try a set_for_each_sample to split one
    import copy
    split_one = copy.deepcopy(one)
    split_one.set_for_each_sample("sample_name", ["nameA", "nameB"])
    assert split_one["sample_name"] == "nameA...nameB"
    assert split_one.num_samples == 2
    sample0_name = split_one.samples[0]["sample_name"]
    sample1_name = split_one.samples[1]["sample_name"]
    assert sample0_name == "nameA"
    assert sample1_name == "nameB"
    # equally, should just be able to set the attribute
    split_one = copy.deepcopy(one)
    split_one["energy"] = "5...6...1"
    assert split_one.num_samples == 3
    sample0_energy = split_one.samples[0]["energy"]
    sample1_energy = split_one.samples[1]["energy"]
    sample2_energy = split_one.samples[2]["energy"]
    assert sample0_energy == 5
    assert sample1_energy == 6
    assert sample2_energy == 1
    # finally, if we try to set an attribute with the wrong number of splits
    # we should get an exception 
    try:
        split_one["energy"] = "5...6"
    except ValueError:
        pass  # expected
    else:
        assert False, "Shouldn't be able to set two variables for a 3 sample"
    try:
        two.set_for_each_sample("energy", [1, 2, 3])
    except ValueError:
        pass  # expected
    else:
        assert False, "Shouldn't be able to set three variables for a 2 sample"
    

def test_MultiSampleMetadata_add_sample():
    empty = metadata.MultiSampleMetadata()
    # adding a sample to a multi where no keys have been set should result in one
    sample = metadata.SampleMetadata(energy=10)
    empty.add_sample(sample)
    assert empty.num_samples == 1, "expeccted 1, found {}".format(empty.num_samples)
    assert empty["energy"] == 10
    # if a sample is already at least partially specified
    # adding a sample should result in two samples
    params = dict(sample_name="MySample",
                  simulation_type="simType",
                  particle_content="pions",
                  energy=1,
                  eta_range=(-25, -20, 20, 25),
                  xAOD_location="/somewhere/else.root",
                  base_dir="/sepreate/dir")
    one = metadata.MultiSampleMetadata(**params)
    one.add_sample(sample)
    assert one.num_samples == 2
    assert one["energy"] == "1...10"
    assert one.samples[0]["energy"] == 1
    assert one.samples[1]["energy"] == 10
    # adding a sample to two samples should result in three samples
    # and variables that were previously constant should become variabel as needed
    params["eta_range"] = "20_25...30_35"
    two = metadata.MultiSampleMetadata(**params)
    two.add_sample(sample)
    assert two.num_samples == 3
    assert two["energy"] == "1...1...10"
    assert two.samples[0]["energy"] == 1
    assert two.samples[1]["energy"] == 1
    assert two.samples[2]["energy"] == 10


def test_MultiSampleMetadata_read_root_text():
    # two basic versions, 
    # 1, read a single file to create a multisample
    # 2, read multiple files and create a multisample
    simple1 = metadata.MultiSampleMetadata(sample_name="MySampleA...MySampleB",
                                           energy=1,
                                           eta_range=(20, 25),
                                           base_dir="/sepreate/dir")
    simple2 = metadata.SampleMetadata(sample_name="MySampleC",
                                      energy=2,
                                      eta_range=(20, 25),
                                      base_dir="/sepreate/dir")
    from hutils.test.utils_for_testing import TempTestDir
    with TempTestDir() as temp_dir_name:
        file_name_base = os.path.join(temp_dir_name, "temp")
        # check for consistancy for text readback
        path = file_name_base + '.txt'
        simple1.write_text_file(path)
        readback = metadata.MultiSampleMetadata.read_text_file(path)
        assert len(simple1.samples) == len(readback.samples)
        for sample in simple1.samples:
            assert sample in readback.samples
        # check for consistancy for root readback
        file_name_base = os.path.join(temp_dir_name, "temp")
        path = file_name_base + '.root'
        simple1.write_root_file(path)
        readback = metadata.MultiSampleMetadata.read_root_file(path)
        assert len(simple1.samples) == len(readback.samples)
        for sample in simple1.samples:
            assert sample in readback.samples
        # should be abel to read a single as a multi
        single_path = file_name_base + "_single.root"
        simple2.write_root_file(single_path)
        readback = metadata.MultiSampleMetadata.read_root_file(single_path)
        assert readback.num_samples == 1
        assert len(readback.samples) == 1
        assert simple2 in readback.samples
        # should also be able to read all three into one multi
        all_paths = path + "..." + single_path
        readback = metadata.MultiSampleMetadata.read_root_file(all_paths)
        assert readback.num_samples == 3
        assert len(readback.samples) == 3
        assert simple2 in readback.samples
        for sample in simple1.samples:
            assert sample in readback.samples
        assert readback["base_dir"] == "/sepreate/dir"
        assert readback["energy"] == "1...1...2"


def test_MultiSampleMetadata_harvest():
    # harvest is basically like a read, only with path info
    simple = metadata.MultiSampleMetadata(sample_name="MySampleA...MySampleB",
                                          energy=1,
                                          eta_range=(20, 25),
                                          base_dir="/sepreate/dir")
    from hutils.test.utils_for_testing import TempTestDir
    with TempTestDir() as temp_dir_name:
        # try without additional info in the dir_name
        file_name_base = os.path.join(temp_dir_name, "temp")
        # check for consistancy for root readback
        path = file_name_base + '.root'
        simple.write_root_file(path)
        readback = metadata.MultiSampleMetadata.harvest_root_path(path)
        # weexpect them to be identical
        assert len(simple.samples) == len(readback.samples)
        for sample in simple.samples:
            assert sample in readback.samples
        # now add some info to the path
        file_name_base = os.path.join(temp_dir_name,
            "EVNT.photons_E34_eta_m30_m20_20_30_z0")
        # check for consistancy for root readback
        path = file_name_base + '.root'
        simple.write_root_file(path)
        readback = metadata.MultiSampleMetadata.harvest_root_path(path)
        # the data in the samples should override the data in the path
        assert len(simple.samples) == len(readback.samples)
        # added info means they are not quite present anymore
        for sample in simple.samples:
            assert sample not in readback.samples
        # but add the info in;
        simple.set_for_all_samples("particle_content", "photons")
        # and there are there again
        for sample in simple.samples:
            assert sample in readback.samples


def test_MetaDataArgParser():
    # basically there are three switches,
    # require_complete_metadata, strict and multisample
    # we can reasonablly assume they interact fine, but each setting of
    # each of them must be tested
    # will try the simplest case;
    # 1. all False
    # 2. - 4. then will try eahc one true
    # 5. then will try all true
    params1 = dict(sample_name="incomplete_unstrict_single",
                   energy="energy")
    params2 = dict(sample_name="incomplete_unstrict_multi",
                   energy="energy...2")
    params3 = dict(sample_name="incomplete_strict_multi",
                   energy="1...2")
    params4 = dict(sample_name="incomplete_strict_single",
                   xAOD_location="/some/path/to/xAOD.root",
                   energy="1")
    params5 = dict(sample_name="complete_unstrict_single",
                   energy="energy",
                   eta_range="eta_range",
                   zv="zv",
                   base_dir="/some/dir",
                   particle_content="particles",
                   simulation_type="simType",
                   simulation_release="simRel")
    params6 = dict(sample_name="complete_unstrict_multi",
                   energy="energy...2",
                   eta_range="eta_range",
                   zv="zv",
                   base_dir="/some/dir",
                   particle_content="particles",
                   simulation_type="simType",
                   simulation_release="simRel")
    params7 = dict(sample_name="complete_strict_multi",
                   energy="1...2",
                   eta_range="20_25",
                   zv="0",
                   base_dir="/some/dir",
                   particle_content="electrons",
                   simulation_type="simType",
                   simulation_release="simRel")
    params8 = dict(sample_name="complete_strict_single",
                   energy="1",
                   eta_range="20_25",
                   zv="0",
                   base_dir="/some/dir",
                   particle_content="electrons",
                   simulation_type="simType",
                   simulation_release="simRel")
    params9 = dict(sample_name="complete_strict_single",
                   dsid="electrons_E1_eta_20_25_z0",
                   base_dir="/some/dir",
                   simulation_type="simType",
                   simulation_release="simRel")
    params = [params1, params2, params3, params4,
              params5, params6, params7, params8, params9]
    for param in params:
        flags = []
        for key, value in param.items():
            flags.append("--"+key)
            flags.append(str(value))
        # condition 1
        all_false = metadata.MetaDataArgParser(require_complete_metadata=False,
                                               strict=False,
                                               multisample=False)
        # as not strict, this should always work
        found = all_false.parse_metadata(flags)
        if 'dsid' in param:
            from hutils import dsid
            particle, energy, eta_range, zv = dsid.decode_DSID(param['dsid'])
            param = {'particle_content': particle, 'energy': energy,
                     'eta_range': eta_range, 'zv': zv, **param}
            del param['dsid']
        expected = metadata.SampleMetadata(strict=False, **param)
        assert found == expected
        # condition 2
        complete = metadata.MetaDataArgParser(require_complete_metadata=True,
                                              strict=False,
                                              multisample=False)
        if param["sample_name"].startswith("complete"):
            found = complete.parse_metadata(flags)
            assert found == expected
        else:
            try:
                found = complete.parse_metadata(flags)
            except AssertionError:
                pass  # expected
            else:
                assert False, ("Incomplete params {} "
                               "should have created exception, "
                               "but found {}").format(param, found)
        # condition 3
        strict = metadata.MetaDataArgParser(require_complete_metadata=False,
                                            strict=True,
                                            multisample=False)
        if param["sample_name"].endswith("_strict_single"):
            found = strict.parse_metadata(flags)
            assert found == expected
        else:
            pass
            # could only do this with a subprocess,
            # argparse quicts when it hist an error....
            #try:
            #    found = strict.parse_metadata(flags)
            #except ValueError:
            #    pass  # expected
            #else:
            #    assert False, ("Unstrict params {} "
            #                   "should have created exception, "
            #                   "but found {}").format(param, found)
        # condition 4
        multi = metadata.MetaDataArgParser(require_complete_metadata=False,
                                           strict=False,
                                           multisample=True)
        multi_expected = metadata.MultiSampleMetadata(strict=False, **param)
        found = multi.parse_metadata(flags)
        assert found.num_samples == multi_expected.num_samples
        for sample in found.samples:
            assert sample in multi_expected.samples
        # condition 5
        all_true = metadata.MetaDataArgParser(require_complete_metadata=True,
                                              strict=True,
                                              multisample=True)
        if param["sample_name"].startswith("complete_strict_"):
            found = all_true.parse_metadata(flags)
            assert found.num_samples == multi_expected.num_samples
            for sample in found.samples:
                assert sample in multi_expected.samples
        # condition 6
        need_xAOD = metadata.MetaDataArgParser(require_complete_metadata=False,
                                               strict=False,
                                               multisample=False,
                                               require_locations=['xAOD'])
        if param["sample_name"].startswith("complete") or 'xAOD_location' in param:
            found = need_xAOD.parse_metadata(flags)
            if 'xAOD_location' not in expected and 'xAOD_location' in found:
                # it was added by the requirement
                expected['xAOD_location'] = found['xAOD_location']
            assert found == expected, f"{found}\n!=\n{expected}"
        else:
            try:
                found = complete.parse_metadata(flags)
            except AssertionError:
                pass  # expected
            else:
                assert False, ("Not enough for xAOD {} "
                               "should have created exception, "
                               "but found {}").format(param, found)
            



if __name__ == "__main__":
    from hutils.test.utils_for_testing import run_tests
    run_tests(globals())


import sys, os
package_dir = os.path.join(os.path.dirname(__file__), "../..")
sys.path.insert(0, package_dir)
from hutils import particles

def test_known_pids():
    assert all([isinstance(pid, int) for pid in particles.known_pids])


def test_plural_names():
    assert all([isinstance(pid, str) for pid in particles.plural_names])


def test_pid_to_name():
    assert particles.pid_to_name(11) == "electrons"
    assert particles.pid_to_name(22) == "photons"


def test_name_to_pid():
    assert 11 == particles.name_to_pid("electrons")
    assert 22 == particles.name_to_pid("photons")


if __name__ == "__main__":
    from hutils.test.utils_for_testing import run_tests
    run_tests(globals())


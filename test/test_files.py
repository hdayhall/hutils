import sys, os
package_dir = os.path.join(os.path.dirname(__file__), "../..")
sys.path.insert(0, package_dir)
from hutils import files

def test_construct_inner_dir():
    # non standard components
    expected = "particles_E_energy_eta_eta_range_zzv/simType/simRel"
    found = files.construct_inner_dir("simType", "simRel", "particles", "energy", "eta_range", "zv")
    assert found == expected
    # values that can be processed to a standard format
    expected = "photons_E1_eta_m25_m20_20_25_z0/simType/simRel"
    found = files.construct_inner_dir("simType", "simRel", 22, 1, (20, 25), 0)
    assert found == expected


def test_locate():
    expected = "base_dir/validation/EVNT/"
    expected += "photons_E1_eta_m25_m20_20_25_z0/simType/simRel/"
    expected += "EVNT.photons_E1_eta_m25_m20_20_25_z0.root"
    found = files.locate("EVNT", "simType", "simRel", "photons",
                         1, (20, 25), base_dir="base_dir")
    assert expected == found
    expected = "/eos/atlas/atlascerngroupdisk/proj-simul/flatTTree/"
    expected += "photons_E1_eta_m25_m20_20_25_z0/simType/simRel/"
    expected += "flatTTree.photons_E1_eta_m25_m20_20_25_z0.root"
    found = files.locate("flatTTree", "simType", "simRel", "photons",
                         1, (20, 25), base_dir="eos")
    assert expected == found



def test_describe_location():
    output = files.describe_location()
    assert isinstance(output, str)


def test_extract_DSID():
    folder_structures = ["/base_dir/structure/{0}/simType/simRel/thing.{0}.root",
                         "/base_dir/photons/things_EVNTS/{}/something_else/sample.root",
                         "/base_dir/pid11/energy2/{}/something_else/sample.root"]

    strict_dsids = ["electrons_E6_eta_m20_m10_20_30_z0", "photons_E1_eta_20_25_z0",
                    "photons_E100_eta_m35_m30_20_30_z1"]
    for dsid in strict_dsids:
        for structure in folder_structures:
            path = structure.format(dsid)
            found = files.extract_DSID(path, True)
            assert found == dsid, "Expected {}, but found {} in {}".format(dsid, found, path)

    unstrict_dsids = ["pid11_E22_2220_eta_20_25_z0", "electrons_E6p1_eta_20_z0",
                      "photons_E1p2_eta_20_25_z0", "electronsplit_E6_eta_20_30_z0",
                      "photons_E100p4_eta_m35_m30_20_30_z1",
                      "pids11_E_energy_2220_eta_20-25_z0p0"]
    for dsid in unstrict_dsids:
        for structure in folder_structures:
            path = structure.format(dsid)
            try:
                found = files.extract_DSID(path, True)
            except (AssertionError, ValueError):
                pass # this should fail
            else:
                assert False, \
                    "Shouldn't find a dsid in {}, but found {}".format(path, found)
            # should be find without strict constraints
            found = files.extract_DSID(path, False)
            assert found == dsid, "Expected {}, but found {} in {}".format(dsid, found, path)
    
    invalid_dsids = ["photons_E1_eta_20_25", "photons_E100_eta_z1", "pid22_eta_20_50_z0"]
    for dsid in invalid_dsids:
        for structure in folder_structures:
            path = structure.format(dsid)
            try:
                found = files.extract_DSID(path, False)
            except (AssertionError, ValueError):
                pass # this should fail
            else:
                assert False, \
                    "Shouldn't find a dsid in {}, but found {}".format(path, found)


def test_extract_base_dir():
    path = "/base/dir/flatTTrees/stuff.root"
    found = files.extract_base_dir(path)
    found = found.rstrip('/')
    assert found == "/base/dir"


if __name__ == "__main__":
    from hutils.test.utils_for_testing import run_tests
    run_tests(globals())


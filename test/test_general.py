import sys, os
package_dir = os.path.join(os.path.dirname(__file__), "../..")
sys.path.insert(0, package_dir)
from hutils import general

def test_multi_index():
    # should throw errors if the substring isn't found
    for search, keys in [("", []),
                         ("abc", ["d", "e"]),
                         ("abc", ["ac"])]:
        try:
            general.multi_index(search, keys)
        except ValueError:
            pass
        else:
            assert False, \
                "Expected ValueError as none {} are in {}".format(keys, search)
    for search, keys, i, key in [("aaa", ['a'], 0, 'a'),
                                 ("abcde", ["d", "e"], 3, 'd'),
                                 ("abcac", ["ac"], 3, 'ac')]:
        found_i, found_key = general.multi_index(search, keys)
        assert found_key == key, ("Expected to find {} out of {}, but found {}"
                                  " first when searching {}."
                                  ).format(key, keys, found_key, search)
        assert found_i == i, ("Expected to find {} at index {} when searching "
                              "{}, but found it at {}"
                              ).format(key, i, search, found_i)

def test_ensure_folder_exists():
    from hutils.test.utils_for_testing import TempTestDir
    with TempTestDir() as temp_dir_name:
        sub_dir = os.path.join(temp_dir_name, "subdir")
        # should be able to create a directory
        general.ensure_folder_exists(sub_dir)
        assert "subdir" in os.listdir(temp_dir_name)
        # the directory is new so should be empty
        assert len(os.listdir(sub_dir)) == 0
        # calling it again should have no effect
        general.ensure_folder_exists(sub_dir)
        assert "subdir" in os.listdir(temp_dir_name)
        assert len(os.listdir(temp_dir_name)) == 1
        assert len(os.listdir(sub_dir)) == 0


def test_get_date():
    date = general.get_date()
    assert date.isdigit()


if __name__ == "__main__":
    from hutils.test.utils_for_testing import run_tests
    run_tests(globals())


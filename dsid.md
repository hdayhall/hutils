
<!-- create_documentation.py START Module dsid -->

# Module dsid

*Documentation for the code at `dsid.py`*

Create and manipulate DSIDs.
This module provides tools for reading, unpacking and constructing DSIDs.
I assume that DSID stands for Data Sample IDentity, but since this codebase doesn't
suffer from an overabundance of documentation your guess is as good as mine.

The form of a DSID takes the basic template;
`<particles>_E<energy>_eta_<eta range>_z<zv>`,
and if `strict` settings are used;

1. `<particles>` must be found in `particles.valid_particle_content`
2. `<energy>` will be interpreted as a single number, it must be composed
of only numbers, `.` or `p` \(where `p` stands for `.`\),
excluding the first charicter, which can be `m` and `-` to make
a negative energy.
3. `<eta range>` must be a sequence of 2 or 4 integers, seperated by `_`
as with energy, the first charicter can be `-` or `m` to create a negative
number, however decmial points are not possible.
4. `<zv>` can be anything that can be converted to an int or float,
any occurances or `p` can be interpreted as `.` to form a float.

Without the `strict` requirement, all underscores can be replaced with `.`.
The form of the components is almost unrestricted;

1. `<particles>` can be any string that doesn't include the substring `_E`
or `.E`.
2. `<energy>` can be any string that doesn't include the substring `_eta`
or `.eta`.
3. `<eta range>` can be any string that doesn't include the substring `_z`
or `.z`.
4. `<zv>` is unrestricted.

In general, attempts will still be made to coerce the entries
into a regular form, but if these attempts fail no error will be raised,
and the string itself will be stored.

Two functions are likely to be most useful here;

1. `make_DSID`
2. `decode_DSID`


## Example


To construct a DSID the `make_DISD` function can be used;


```python
particle_content = "photons"
energy = 65536
eta_range = (-25, 20, 20, 25)
zv = 0
dsid = make_DSID(particle_content, energy, eta_range, zv)
assert dsid == "photons_E65536_eta_m25_m20_20_25_z0"
```


This function is also capable of taking a dictionary that
includes there values, any any other parameters
\(which are ignored\);


```python
my_sample = {'particle_content': "photons",
             'energy': 65536,
             'eta_range': (-25, 20, 20, 25),
             'zv': 0,
             'extra_info': "irrelevent and will be ignored"}
dsid = make_DSID(**my_sample)
assert dsid == "photons_E65536_eta_m25_m20_20_25_z0"
```


This is designed to allow `metadata.SampleMetadata` to be
used as input to `make_DSID`, since `metadata.SampleMetadata`
has enough dict like behavior to expand to an argument list
this way.


## Example


To read the parameters out from a DISD `decode_DSID` can be used;


```python
dsid = "photons_E65536_eta_m25_m20_20_25_z0"
particle_content, energy, eta_range, zv = decode_DSID(dsid, strict=True)
assert particle_content == "photons"
assert energy == 65536
assert eta_range == (-25, -20, 20, 25)
assert zv == 0
```


even without `strict=True` the same result is obtained,
the `strict=True` requirment just means that if a conversion fails
then a `ValueError` is raised. Without it, any atypical component
would just be returned as a string.


<!-- create_documentation.py END Module dsid -->


<!-- create_documentation.py START Breakpoint disclaimer -->



 -----
 -----

 This should be everything needed for basic use of the package.
 Unless you are looking to develop the package, or debug bad behaviour,
 you probably don't need anything below this point.


<!-- create_documentation.py END Breakpoint disclaimer -->


<!-- create_documentation.py START Docstrings header -->



 ## Doctrings of functions and classes


<!-- create_documentation.py END Docstrings header -->


<!-- create_documentation.py START Function decode\_DSID -->

## Function decode\_DSID

**decode\_DSID\(dsid, strict=False\)**

Given a DSID, extract the sample parameters.
Has a strict and a non\-strict form.

The form of a DSID takes the basic template;
`<particle_content>_E<energy>_eta_<eta range>_z<zv>`,
Without the `strict` requirement, all underscores can be replaced with `.`.
The form of the components is almost unrestricted.
In general, attempts will still be made to coerce the entries
into a regular form, but if these attempts fail no error will be raised,
and the string itself will be returned.


### Parameters


**dsid : str**

A string representation of the DSID

**strict : bool**

Should errors be raised if the string dosn't fit
into the form of a "strict" DSID?
\(Default value = False\)


### Returns


**particle\_content : str**

If strict, must be found in `particles.valid_particle_content`
If not strict, can be any string that doesn't include the
substring `_E` or `.E`.

**energy : int or float**

If strict, will be interpreted as a single number, it must be composed
of only numbers, `.` or `p` \(where `p` stands for `.`\),
excluding the first character, which can be `m` and `-` to make
a negative energy.
If not strict, can be any string that doesn't include
the substring `_eta` or `.eta`.

**eta\_range : tuple**

Must be a sequence of 2 or 4 integers, separated by `_`
as with energy, the first character can be `-` or `m` to create a negative
number, however decimal points are not possible.
Will be converted to a tuple by `eta_range_to_tuple`.
If not strict, can be any string that doesn't include
the substring `_z` or `.z`.
Will try to convert to a tuple by `eta_range_to_tuple`.

**zv : int or float**

If strict, can be anything that can be converted to an int or float,
any occurrences or `p` can be interpreted as `.` to form a float.
If not strict, is unrestricted.


### Raises

ValueError
If strict is required, and the given dsid doesn't fit the
requirements for a strict dsid then ValueError is raised.
<!-- create_documentation.py END Function decode\_DSID -->


<!-- create_documentation.py START Function eta\_range\_to\_string -->

## Function eta\_range\_to\_string

**eta\_range\_to\_string\(iterable, force\_long=True\)**

Given an eta range as an iterable, create a string format.

The string will have the form `#_#_#_#` or `#_#`.
Where `#` is the string representation of an integer
with `-` signs converted to `m`.
If the iterable is length 4 the form will always be
`#_#_#_#`. If the iterable is length 2, the and
`force_long` is not `True` then the short from will be
used, otherwise the first two values will be generated by
negating the values found in iterable.


### Parameters


**iterable : iterable of ints**

End values of the eta range. Must be length 2 or 4.

**force\_long : bool**

Should the longer form be created, even if the iterable
is length 2?
\(Default value = True\)


### Returns


**string : str**

The constructed eta string.


### Raises

ValueError
if the itterable is not length 2 or 4
<!-- create_documentation.py END Function eta\_range\_to\_string -->


<!-- create_documentation.py START Function eta\_range\_to\_tuple -->

## Function eta\_range\_to\_tuple

**eta\_range\_to\_tuple\(string\)**

Given an eta range in string from, produce a tuple.

Eta ranges have can the form `#_#` or `#_#_#_#` where
`#` stands for a positive or negative integer.
Negative values can be indicated with `-` or `m`.
Alternatively, if the string is the string representation
of a tuple or list of at least length 2, it will be
converted to a python tuple.


### Parameters


**string : str**

The string to interpret.


### Returns


**: tuple**

The eta range exspressed as a tuple.


### Raises

ValueError
If the string is given has an unrecognised format
then value error will be raised.
<!-- create_documentation.py END Function eta\_range\_to\_tuple -->


<!-- create_documentation.py START Function make\_DSID -->

## Function make\_DSID

**make\_DSID\(particle\_content, energy, eta\_range, zv, \*\*excess\_args\)**

Make a DSID from the sample's parameters.


### Parameters


**particle\_content : int or str**

String representing the particle, or pid for the particle.
pids will be converted to names using `particles.pid_to_name`.

**energy : int, float or str**

Energy of the sample. If this is a string that does not
start with `-`, `m` or a digit an underscore will be appended.
The string cannot be the empty string.

**eta\_range : iterable or str**

Eta range of the sample. If an iterable is given,
`eta_range_to_string` with `force_long=True` will be used
to form a string.

**zv : str or int**

The z vertex of the sample.

**\*\*excess\_args : dict of anything**

Any additional keyword arguments will be ignored.
This allows dicts that contain more than these keywords to be
expanded as arguments for this function.


### Returns


**string : str**

String representing the DSID of the particle,
this has the form;
`<particle_content>_E<energy>_eta_<eta_range>_z<zv>`.
Any `.` will be replaced with `p`.
<!-- create_documentation.py END Function make\_DSID -->


<!-- create_documentation.py START Object absolute\_import -->

## Object absolute\_import

**_Feature**

\_Feature\(\(2, 5, 0, 'alpha', 1\), \(3, 0, 0, 'alpha', 0\), 262144\)
<!-- create_documentation.py END Object absolute\_import -->


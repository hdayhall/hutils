""" Manipulations for file paths.

Within the context of locating and identifying samples, 
this module contains often needed file path manipulations.
It is designed to provide consistent placement of different types of data files,
according to the data type and the sample that the data represents.

When an analysis has multiple steps, one of the major challenges if ensuring that
each step properly locates the data created by the previous step.
Before this code, most steps had their own logic for choosing where to write and read from.
Delegating that to this centralised code has a number of advantages.
Firstly, as each step is using the same logic code to identify the path for a data file,
we have guaranteed that subsequent steps will be able to find a file if it has been written.
Secondly, if we keep the logic for locating files in this common tool,
we remove associated code repetition, which means that any bugs only need to be fixed once.
Thirdly, if in the future we decide to change where data is stored, that
change must only be made here, and not repeated for each processing step.

The expected data types are;

1. `EVNT` files, large files, storing event data before detector simulation.
2. `param` files, files that parameterise the detector simulation.
3. `xAOD` files, moderately large files, that store compile objects representing the event after detector simulation.
4. `flatTTree` files, small TTree files, with one value per event per branch.
5. `images`, histograms of various kinds created from the flatTTrees.

Example
-------

Use case, you want to find where a data file of a given sample
type should reside, either to write or read it.

First, let us create a set of variable that would describe
the sample it self; 

>>> simulation_type = 'ATLFAST3'
>>> simulation_release = '22.0.62'
>>> particle_content = 'photons'
>>> energy = 65536
>>> eta_range = (-25, -20, 20, 25)

No fixed for for these variables is enforced, 
but if they do take a regular form, sensible conversions
are care needed. 
the simulation type should be one of the entries on
[this](https://gitlab.cern.ch/atlas/athena/-/blob/master/simulation/isf/isf_config/python/isf_configconfigdb.py)
web page.
The simulation release is normally the athena version, though sometimes
more information is given here.
The particle content should be a plural particle name
or something else from `particles.valid_particle_content`,
the energy is normally an int, the eta range is a
tuple of 2 or 4 ints, or a string representing eta range,
see documentation for `dsid.eta_range_to_string`.

Then we need to know what kind of data file is required, 
it could be a `EVNT`, `param`, `xAOD`, `flatTTree` or `images`;

>>> data_type = 'EVNT'

Finally, the default base directory (`base_dir`) is `eos`,
which puts files under `/eos/atlas/atlascerngroupdisk/proj-simul/`,
but there are other options.
You could specify the full file path manually;

>>> base_dir = '/path/to/the/files/'

Or you can ask for the files to be put in `~/public/data/` using;

>>> base_dir = 'local'

Now the correct location of the data is returned with;

>>> data_path = locate(data_type, simulation_type, reconstruction_release,
...                    simulation_release, particle_content, energy,
...                    eta_range, base_dir=base_dir)


Example
-------

Given a `metadata.SampleMetadata` object, you would like to find the correct
location of a sample of that data type.

>>> import metadata
>>> isinstance(my_sample, metadata.SampleMetadata)

A fully specified `metadata.SampleMetadata` holds all needed information
about the sample, and applies equally to all data types.
Because it applies equally to all data types
you must define the data type first.
it could be a `EVNT`, `param`, `xAOD`, `flatTTree` or `images`;

>>> data_type = 'EVNT'

besides that information, the metadata object can supply the rest.
`metadata.SampleMetadata` objects have some `dict` like behavior,
specifically, they implement keyword indexing; `my_sample["particle_content"]`,
length checks; `len(my_sample)`, iteration over keys; `iter(my_sample)`
and checking for keys; `"particle_content" in my_sample`.

This is more than enough to expand the dict into an argument list;

>>> data_path = locate(data_type, **metadata)

If it turns out there is not enough information in the metadata object,
this will raise a `TypeError`, but the message is less than helpful
in older versions of python.

Example
-------

Use case, given a file that you believe to contain some data,
you can extract the base directory used for storing data
and also the dsid of the data;

>>> my_data_at = "/some/base/dir/OutputSamples/photons_E65536_eta_m25_m20_20_25_z0/ATLFAST3/22.0.26/"
>>> base_dir = extract_base_dir(my_data_at)
>>> assert base_dir == "/some/base/dir"
>>> my_dsid = extract_DSID(my_data_at, strict=True)
>>> assert my_dsid == "photons_E65536_eta_m25_m20_20_25_z0"

If the dsid may contain irregular values, you will need
`strict=False`. Then finally, you may extract the values themselves using
`decode_DSID` from the `dsid` module.

"""
# underscore avoids polluting the namespace
import os as _os
from .dsid import make_DSID as _make_DSID
from . import dsid as _dsid
from .general import get_home_dir as _get_home_dir
from . import particles as _particles


base_directories = {'local' : _os.path.join(_get_home_dir(), 'public/data'),
                    'env_DATADIR' : _os.environ.get("DATADIR", None),
                    'eos22' : '/eos/atlas/atlascerngroupdisk/proj-simul/AF3_Run3/',
                    'eos' : '/eos/atlas/atlascerngroupdisk/proj-simul/'}

sub_dir = {'EVNT' : 'validation/EVNT',
           'params' : 'InputsToBigParamFiles',
           'xAOD' : 'OutputSamples',
           'flatTTree' : 'flatTTree',
           'images' : 'ComparisonFigures'}


def construct_inner_dir(simulation_type, simulation_release, 
                        particle_content, energy, eta_range, zv,
                        **excess_args):
    """
    For given sample specification, construct a string representing
    the third and innermost section of the path.
    The outermost section is the `base_dir`, then there
    is the data type specific `sub_dir`, then finally the inner
    directory specific to the sample type.

    Parameters
    ----------
    simulation_type : str
        Name of the simulation used for the sample.
        The simulation type should be one of the entries on
        [this](https://gitlab.cern.ch/atlas/athena/-/blob/master/simulation/isf/isf_config/python/isf_configconfigdb.py)
        web page.
    simulation_release : str
        The release of the simulation that was used for the sample.
        The simulation release is normally the athena version,
        though sometimes more information is given here.
    particle_content : str or int
        The particle in the sample.
        The particle content should be a plural particle name
        or something else from `particles.valid_particle_content`,
        or a pid.
    energy : int or float or str
        The energy of the sample.
    eta_range : tuple or str
        The eta range is a tuple of 2 or 4 ints, or a string representing eta range,
        see documentation for `dsid.eta_range_to_string`.
    zv : int or str
        The z vertex offset of the sample
    **excess_args : anything
        Swallows excessive arguments silently.
        This allows dicts that contain at least the required arguments
        to be used to construct an argument list.

    Returns
    -------
    inner_dir : str
        inner most layers of the directory path for the sample.
        This is independent of the data type.
        
    """
    DSID = _make_DSID(particle_content=particle_content, 
                      energy=energy, eta_range=eta_range, zv=zv)
    inner_dir = _os.path.join(DSID, simulation_type, simulation_release)
    return inner_dir


def locate(data_type, simulation_type,
           simulation_release, reconstruction_release, particle_content, energy,
           eta_range, zv=0, base_dir='eos', 
           **excess_args):
    """
    For given sample specification and data type, construct
    a string representing the full path that would be the correct
    place for the data file.
    Does not check if a data file is actually there.

    Parameters
    ----------
    data_type : str
        What kind of data file is being located.
        It could be a `EVNT`, `param`, `xAOD`, `flatTTree` or `images`.
    simulation_type : str
        Name of the simulation used for the sample.
        The simulation type should be one of the entries on
        this (https://gitlab.cern.ch/atlas/athena/-/blob/master/simulation/isf/isf_config/python/isf_configconfigdb.py)
        web page.
    simulation_release : str
        The release of the simulation that was used for the sample.
        The simulation release is normally the athena version,
        though sometimes more information is given here.
    particle_content : str or int
        The particle in the sample.
        The particle content should be a plural particle name
        or something else from `particles.valid_particle_content`,
        or a pid.
    energy : int or float or str
        The energy of the sample.
    eta_range : tuple or str
        the eta range is a tuple of 2 or 4 ints, or a string representing eta range,
        see documentation for `dsid.eta_range_to_string`.
    zv : int or str (optional)
        The z vertex offset of the sample
    **excess_args : anything
        Swallows excessive arguments silently.
        This allows dicts that contain at least the required arguments
        to be used to construct an argument list.

    Returns
    -------
    file_path : str
        Full path that the data file should reside at.
        
    """
    path_components = []
    path_components.append(base_directories.get(base_dir, base_dir))
    path_components.append(sub_dir[data_type])
    inner_dir = construct_inner_dir(
        simulation_type=simulation_type,
        simulation_release=simulation_release if (reconstruction_release is None or simulation_release == reconstruction_release) 
                            else '_'.join([simulation_release, reconstruction_release]),
        particle_content=particle_content,
        energy=energy,
        eta_range=eta_range,
        zv=zv)
    path_components.append(inner_dir)
    DSID = _make_DSID(particle_content=particle_content, 
                      energy=energy, eta_range=eta_range, zv=zv)
    ending = "" if data_type == 'images' else ".root"
    file_name = "{}.{}{}".format(data_type, DSID, ending)
    path_components.append(file_name)
    file_path = _os.path.join(*path_components)
    return file_path


class Catalogue:
    """
    Create a list of existing data, within some specifications.
    Optionally, also check data opens and holds metadata corrisponding to claims.
    """
    WILDCARD = object()
    DEFAULT_data_types = sub_dir.keys()
    DEFAULT_simulation_types = ["FullG4", "ATLFAST3", "ATLFAST3MT"]
    DEFAULT_simulation_releases = WILDCARD
    DEFAULT_particle_contents = _particles.valid_particle_content
    DEFAULT_energies = WILDCARD
    DEFAULT_eta_ranges = WILDCARD
    DEFAULT_zvs = [0]
    DEFAULT_base_dirs = base_directories.keys()
    def __init__(self, data_type=None, simulation_type=None,
                 simulation_release=None, particle_content=None, energy=None,
                 eta_range=None, zv=None, base_dir=None):
        """
        Parameters may be a single item of an iterable of items.
        Each parameter restricts the search to items on paths
        matching the specifications.
        If a parameter is set to `Cataloge.WILDCARD`, it will match
        any variable found on disk, `data_type` and `base_dir` cannot
        be wildcards.
        When parameters are left as None they take their default
        values as defined by class attributes.

        Parameters
        ----------
        data_type : str (optional)
            What kind of data file is being located.
            It could be a `EVNT`, `param`, `xAOD`, `flatTTree` or `images`.
            Cannot be a wildcard.
        simulation_type : str (optional)
            Name of the simulation used for the sample.
            The simulation type should be one of the entries on
            this (https://gitlab.cern.ch/atlas/athena/-/blob/master/simulation/isf/isf_config/python/isf_configconfigdb.py)
            web page.
        simulation_release : str (optional)
            The release of the simulation that was used for the sample.
            The simulation release is normally the athena version,
            though sometimes more information is given here.
        particle_content : str or int (optional)
            The particle in the sample.
            The particle content should be a plural particle name
            or something else from `particles.valid_particle_content`,
            or a pid.
        energy : int or float or str (optional)
            The energy of the sample.
        eta_range : tuple or str (optional)
            the eta range is a tuple of 2 or 4 ints, or a string representing eta range,
            see documentation for `dsid.eta_range_to_string`.
        zv : int or str (optional)
            The z vertex offset of the sample
        base_dir : str (optional)
            Start of the path of the sample.
            Cannot be a wildcard.
        """
        for arg, attr_name, can_wild, types in [
                (data_type, "data_types", False, str),
                (simulation_type, "simulation_types", True, str),
                (simulation_release, "simulation_releases", True, str),
                (particle_content, "particle_contents", True, str),
                (energy, "energies", True, (float, int, str)),
                (zv, "zvs", True, (float, int, str)),
                (base_dir, "base_dirs", False, str)]:
            if arg is None:
                value = getattr(self, "DEFAULT_" + attr_name)
            elif arg is self.WILDCARD and not can_wild:
                raise ValueError(attr_name + " cannot be a wildcard")
            elif isinstance(arg, types):
                value = [arg]
            else:
                value = arg
                if not hasattr(arg, '__iter__'):
                    raise ValueError(
                        "{} given as {} is not in types {} or iterable"
                        .format(arg, attr_name, types))
            setattr(self, attr_name, value)

        # eta_range is more complex since it itself is an iterable if floats/ints
        if eta_range is None:
            self.eta_ranges = self.DEFAULT_eta_ranges
        elif (hasattr(eta_range, '__iter__') and 
              all(hasattr(r, '__iter__') for r in eta_range)):
            # it's a list of permissable etas
            self.eta_ranges = eta_range
        elif eta_range is self.WILDCARD:
            self.eta_ranges = eta_range
        else:
            self.eta_ranges = [eta_range]

        self._dsid_list = self._get_dsid_list()
        self.roots = []
        self.images = []
        self.linked_roots = []
        self.linked_images = []
        self._append_paths()

    def _get_dsid_list(self):
        if self.WILDCARD in [self.particle_contents, self.energies,
                             self.eta_ranges, self.zvs]:
            return []
        dsids = [_dsid.make_DSID(p, e, er, z)
                 for p in self.particle_contents
                 for e in self.energies
                 for er in self.eta_ranges
                 for z in self.zvs]
        return dsids

    def _is_compatable_dsid(self, candidate):
        if candidate in self._dsid_list:
            return True

        try:
            particle, energy, \
                eta_range, zv = _dsid.decode_DSID(candidate, strict=True)
        except Exception as e:  # not a strict dsid
            return False

        if not self.WILDCARD is self.particle_contents:
            if not any(_particles.are_equal(particle, p)
                       for p in self.particle_contents):
                return False
        if not self.WILDCARD is self.energies:
            if not any(abs(energy - float(e)) < 0.1
                       for e in self.energies):
                return False
        if not self.WILDCARD is self.eta_ranges:
            if not any(_dsid.eta_ranges_equal(eta_range, e)
                       for e in self.eta_ranges):
                return False
        if not self.WILDCARD is self.zvs:
            if not any(abs(zv - float(z)) < 0.0001
                       for z in self.zvs):
                return False
        return True

    def _append_paths(self):
        for base in self.base_dirs:
            base = base_directories.get(base, base)
            if not isinstance(base, str):
                continue
            for data_t in self.data_types:
                inner = _os.path.join(base, sub_dir[data_t])
                print("\nSearching " + inner )
                if _os.path.exists(inner):
                    self._append_paths_from_inner(inner)

    def _append_paths_from_inner(self, inner):
        for dsid in _os.listdir(inner):
            path = _os.path.join(inner, dsid)
            if not _os.path.isdir(path):
                continue
            if not self._is_compatable_dsid(dsid):
                continue
            for simulation in _os.listdir(path):
                sim_path = _os.path.join(path, simulation)
                if not _os.path.isdir(sim_path):
                    continue
                if not (self.simulation_types is self.WILDCARD or
                        simulation in self.simulation_types):
                    continue
                for release in _os.listdir(sim_path):
                    rel_path = _os.path.join(sim_path, release)
                    if not _os.path.isdir(rel_path):
                        continue
                    if not (self.simulation_releases is self.WILDCARD or
                            release in self.simulation_releases):
                        continue
                    self._append_paths_from_dir(rel_path, dsid)

    def _append_paths_from_dir(self, dir_name, dsid):
        for item in _os.listdir(dir_name):
            if dsid not in item:
                continue
            path = _os.path.join(dir_name, item)
            if item.startswith("images"):
                if _os.path.isdir(path):
                    self.images.append(path)
                elif _os.path.islink(path):
                    self.linked_images.append(path)
            elif item.endswith(".root"):
                if _os.path.isfile(path):
                    self.roots.append(path)
                elif _os.path.islink(path):
                    self.linked_roots.append(path)


def describe_location(data_type=None,
                      simulation_type=None, simulation_release=None, reconstruction_release=None,
                      particle_content=None, energy=None,
                      eta_range=None, zv=0, base_dir=None,
                      **excess_args):
    """
    Documentaion generation function, for a given set of restrictions
    print and return a string that describes the way the locaton
    for samples with those charicteristics would be found.
    Any parameters not given are treated as variables.

    Parameters
    ----------
    data_type : str (optional)
        Specfiy the kind of data file.
        It could be a `EVNT`, `param`, `xAOD`, `flatTTree` or `images`.
    simulation_type : str (optional)
        Specfiy the simulation used.
        The simulation type should be one of the entries on
        [this](https://gitlab.cern.ch/atlas/athena/-/blob/master/simulation/isf/isf_config/python/isf_configconfigdb.py)
        web page.
    simulation_release : str (optional)
        The release of the simulation that is of intrest.
        The simulation release is normally the athena version,
        though sometimes more information is given here.
    particle_content : str or int (optional)
        The particle in the sample.
        The particle content should be a plural particle name
        or something else from `particles.valid_particle_content`,
        or a pid.
    energy : int or float or str (optional)
        The energy of the sample.
    eta_range : tuple or str (optional)
        the eta range is a tuple of 2 or 4 ints, or a string representing eta range,
        see documentation for `dsid.eta_range_to_string`.
    zv : int or str (optional)
        The z vertex offset of the sample
        (Default value = 0)
    **excess_args : anything
        Swallows excessive arguments silently.
        This allows dicts that contain at least the required arguments
        to be used to construct an argument list.

    Returns
    -------
    message : str
        A description of all of the possibilities that are consistent with
        this restriction.

    """
    options_for = []
    options = []
    if data_type is None:
        data_type = "<data_type>"
        options_for.append(data_type)
        options.append("one of " + ', '.join(sub_dir.keys()))
        sub_dir[data_type] = "<structure_for_data_type>"
    if simulation_type is None:
        simulation_type = "<simulation_type>"
        options_for.append(simulation_type)
        options.append(
            "any string describing the simulation type and param files")
        compare_with_full_sim = True
    else:
        compare_with_full_sim = False
    if simulation_release is None:
        simulation_release = "<simulation_release>"
        options_for.append(simulation_release)
        options.append(
            "any string describing the release version" +
            " of the simulation. Not required or used when " +
            " <simulation_type> contains 'Full'")
    if particle_content is None:
        particle_content = "<particle_content>"
        options_for.append(particle_content)
        particles = _particles.valid_particle_content
        particles += [str(i) for i in _particles.known_pids]
        options.append("an int representing a pid or one of " +
                       ', '.join(particles))
    if energy is None:
        energy = "<energy>"
        options_for.append(energy)
        options.append("an int or float specifying simulation energy")
    if eta_range is None:
        eta_range = "<eta_range>"
        options_for.append(eta_range)
        options.append("a tuple of length 2 or 4 describing the " + 
                       " range of eta values, or " +
                       "a string describing the eta range")
    if base_dir is None:
        base_dir = "<base_dir>"
        options_for.append(base_dir)
        options.append("one of " + ', '.join(base_directories.keys())
                       + " or a directory path")
    path = locate(data_type=data_type, simulation_type=simulation_type,
                  simulation_release=simulation_release,
                  reconstruction_release=reconstruction_release,
                  particle_content=particle_content, energy=energy,
                  eta_range=eta_range, zv=zv, base_dir=base_dir)

    message = "The path has the form; \n{}\n\n".format(path)
    if compare_with_full_sim:
        path = locate(data_type=data_type, simulation_type="Full<simtype>",
                      simulation_release=simulation_release,
                      reconstruction_release=reconstruction_release,
                      particle_content=particle_content, energy=energy,
                      eta_range=eta_range, zv=zv, base_dir=base_dir)

        message += "Special case; the simulation is a full simulation, in which case there is no simulation_release; \n{}\n\n".format(path)
    if options_for:
        longest_name = max([len(name) for name in options_for])
        for name, possible in zip(options_for, options):
            name = name.replace("<", '"').replace(">", '"')
            name = name.rjust(longest_name)
            message += "{} argument can be; {}\n".format(name, possible)
    print(message)
    return message


def extract_DSID(filename, strict=False):
    """
    Given the full path to a file, which has one of the forms
    that could be produced by `locate`, extract a DSID.

    Parameters
    ----------
    filename : str
        Full path to the file
    strict : bool
        Should the filename be interpreted strictly to
        minimise the possibility of mistakes.
        If `False` the filename is interpreted 
        in a relaxed way that may allow for mistakes
        by is less likely to raise an error

    Returns
    -------
    dsid : str
        The sample DSID, as a string.
    """
    from .general import multi_index
    poss_particle_starts = _particles.valid_particle_content[:]
    if not strict:
        poss_particle_starts.append("pid")
    clipped_name = filename[:]
    while clipped_name:
        try:
            particle_start, first_particle = multi_index(
                clipped_name, poss_particle_starts)
            clipped_name = clipped_name[particle_start:]
        except ValueError as e:
            raise ValueError("Couldn't find a valid dsid in " + filename)
        try:
            clipped_to_part = clipped_name.split('/')[0]
            return _extract_DSID(clipped_to_part, first_particle, strict)
        except (AssertionError, ValueError, IndexError) as e:
            clipped_name = clipped_name[len(first_particle):]
    

def _extract_DSID(clipped_name, start_particle, strict):
    from .general import multi_index

    poss_energy_starts = ["_E"]
    if not strict:
        poss_energy_starts.append(".E")
    energy_start, _ = multi_index(
        clipped_name, poss_energy_starts)

    # at the very least, the string that identified the particle start
    # must end before the energy_start
    assert len(start_particle) <= energy_start, \
        "energy_start {} seems too short for first particle {}"\
        .format(energy_start, start_particle)
    if strict:
        assert len(start_particle) == energy_start, "There was excess content in the particle string"

    poss_eta_starts = ["_eta"]
    if not strict:
        poss_eta_starts.append(".eta")
    eta_offset, _ = multi_index(
        clipped_name[energy_start:], poss_eta_starts)
    eta_start = energy_start + eta_offset


    energy_str = clipped_name[energy_start+2:eta_start]
    assert energy_str, "No content found in energy string"
    if strict:
        assert all(c.isdigit() for c in energy_str), "the energy string was not a positive int"

    poss_z_starts = ["_z"]
    if not strict:
        poss_z_starts.append(".z")
    z_offset, _ = multi_index(
        clipped_name[eta_start:], poss_z_starts)
    z_start = eta_start + z_offset

    eta_str = clipped_name[eta_start+4:z_start]
    assert eta_str.replace("_", ""), "No content found in eta string"

    end_index = z_start
    # get to the number
    while not clipped_name[end_index].isdigit():
        end_index += 1
    # get passed the number
    try:
        if strict:
            while clipped_name[end_index].isdigit():
                end_index += 1
        else:
            while clipped_name[end_index].isdigit() or \
                    (clipped_name[end_index] in 'p.' and
                     clipped_name[end_index+1].isdigit()):
                end_index += 1
    except IndexError as e:
        if len(clipped_name) == end_index:
            dsid = clipped_name[:end_index]
        elif clipped_name[end_index] in "p.":
            dsid = clipped_name[:end_index-1]
        else:
            raise RuntimeError("Unexpected IndexError {}".format(e))
    else:
        dsid = clipped_name[:end_index]
    assert z_start + 2 < end_index, "no content in zv"
    return dsid


def extract_base_dir(filepath):
    """
    Given the full path to a file, which has one of the forms
    that could be produced by `locate`, extract it's base directory.

    Parameters
    ----------
    filename : str
        Full path to the file
    strict : bool
        Should the filename be interpreted strictly to
        minimise the possibility of mistakes.
        If `False` the filename is interpreted 
        in a relaxed way that may allow for mistakes
        by is less likely to raise an error

    Returns
    -------
    base_dir : str
        The base directory of this filepath,
        before the sample specific sub_dir component.
    """
    for path_section in sub_dir.values():
        if path_section is not None and path_section in filepath:
            base_dir = filepath.split(path_section, 1)[0]
            break  # skips the else
    else:
        raise NotImplementedError(
                "Don't know how to get a base dir for {}"\
                .format(filepath))

    return base_dir


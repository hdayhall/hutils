
<!-- create_documentation.py START Module copy metadata -->

# Module copy metadata

*Documentation for the code at `copy_metadata.py`*

Copy metadata.
Intended to form a script that can be used from bash, to copy metadata
from one location onto another. The source and target can be text files
or root files.


## Example


Say you have a text file containign metadata at
`/path/to/prepared/metadta.txt` and a root
file that should be labeled with this metadata at
`/path/to/unlabeled/data.root`.
The metadata can be added to the root file by
running;


```bash
python copy_metadata.py /path/to/prepared/metadata.txt /path/to/unlabeled/data.root
```


If the target path does not exist it will be created.
If the target path has existing metadata it will be overwritten.


<!-- create_documentation.py END Module copy metadata -->


<!-- create_documentation.py START Breakpoint disclaimer -->



 -----
 -----

 This should be everything needed for basic use of the package.
 Unless you are looking to develop the package, or debug bad behaviour,
 you probably don't need anything below this point.


<!-- create_documentation.py END Breakpoint disclaimer -->


<!-- create_documentation.py START Docstrings header -->



 ## Doctrings of functions and classes


<!-- create_documentation.py END Docstrings header -->


<!-- create_documentation.py START Function copy -->

## Function copy

**copy\(input\_file, output\_file\)**

Copy metadata from one place to another.


### Parameters


**input\_file : str**

Path to file to read from. If this path ends with `.root`
it is assumed the input file is a root file, otherwise
it is assumed the file is a text file.

**output\_file : str**

Path to file to write to. If this path ends with `.root`
it is assumed the output file is a root file, otherwise
it is assumed the file is a text file.
If the file doesn't exist it will be created.
<!-- create_documentation.py END Function copy -->


<!-- create_documentation.py START Object absolute\_import -->

## Object absolute\_import

**_Feature**

\_Feature\(\(2, 5, 0, 'alpha', 1\), \(3, 0, 0, 'alpha', 0\), 262144\)
<!-- create_documentation.py END Object absolute\_import -->


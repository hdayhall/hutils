""" Objects representing sample metadata.

Data samples used to characterise simulations have some core characteristics;
1. `simulation_type` Name of the simulation used for the sample.
 The simulation type should be one of the entries on
 [this](https://gitlab.cern.ch/atlas/athena/-/blob/master/simulation/isf/isf_config/python/isf_configconfigdb.py)
 web page.
2. `simulation_release` The release of the simulation that was used for the sample.
 The simulation release is normally the athena version,
 though sometimes more information is given here.
3. `reconstruction_release` The release of the simulation that was used for the sample.
 The simulation release is normally the athena version,
 though sometimes more information is given here.
4. `particle_content` The particle in the sample.
 The particle content should be a plural particle name
 or something else from `particles.valid_particle_content`, or a pid.
5. `energy` The energy of the sample.
 . `eta_range` The eta range is a tuple of 2 or 4 ints,
6 or a string representing eta range, see documentation for `dsid.eta_range_to_string`.
7. `zv` The z vertex offset of the sample

Then there are some key locations on the disk associated with this sample;
1. `base_dir` The directory inside which data should be stored in a predetermined structure.
2. `params_location` the path to a parameter file matching this sample
3. `EVNT_location` the path to an EVNT file matching this sample
4. `xAOD_location` the path to an xAOD file matching this sample
5. `flatTTree_location` the path to a flatTTree matching this sample
6. `plots_folder` the path to a folder containing plots for this sample


Example
-------
Use case; you think that a text file or root file contains metadata
in the format created in this module, you want to read it so that you
can use the values in python.

>>> smetadata_path = "/path/to/file/with/metadata.root"
>>> smetadata = SampleMetadata.read_root_file(smetadata_path)
>>> print(smetadata["particle_content"])

or for a text file

>>> smetadata_path = "/path/to/file/with/metadata.txt"
>>> smetadata = SampleMetadata.read_text_file(smetadata_path)
>>> print(smetadata["particle_content"])

Example
-------
Use case; you want to create a record of the sample's metadata
and write it to a text file, or on a canvas in a root file (which might contain
some data from the file itself).

A sample's metadata can we created and written to a root file,
writing to a text file overwrites any existing text.

>>> smetadata = SampleMetadata(simulation_type='ATLFAST3',
...                            simulation_release='22.0.62',
...                            reconstruction_release='22.0.62',
...                            particle_content='photons',
...                            energy=65536,
...                            eta_range=(-25, -20, 20, 25))
>>> smetadata.write_text_file("/path/to/write/metadata.txt")

Alternatively, the sample's metadata can be written to a root file.
This can create a root file if one is not already present,
otherwise the data will be added to the existing root file.
If there is an existing root file and it already has a
canvas at top level named `MetaDataTextCanvas`, it will be
overwritten with the new metadata.

>>> smetadata.write_root_file("/path/to/write/metadata.root")

When metadata has been written to a root file, it is
possible to view it (for example in a `TBrowser`) on the canvas
named `MetaDataTextCanvas`.

Example
-------
Use case; you have a script to start a program and the user must
start the script with enough information to locate a sample.

`MetaDataArgParser` is a subclass of `argparse.ArgumentParser`
which comes preloaded with all the arguments that define a sample.
It can be used just like an `ArgumentParser` object, so any
additional arguments needed can be added.

Depending on the input arguments the amount of information
that is required (not requested, all sample flags are always
valid flags) can be adjusted.
Here is an example of an argument parser that does not require
all information but requires enough information to locate the 
xAOD file;

```python
from hutils.metadata import MetaDataArgParser
# make an argument parser that obtains details of a single sample
# takes all arguments that ArgumentParser takes, plus 4 specilised arguments.
parser = MetaDataArgParser(description="Description to appear in help text.",
                           require_complete_metadata=False,
                           require_locations=['xAOD'],
                           multisample=False,
                           strict=False)
parser.add_argument('-v', '--verbose', action='store_true',
                    help="This is an additional item of information "
                    "needed from the user")
namespace = parser.parse_args()
if namespace.verbose:
    print("This is verbose")
smetadata = parser.parse_metadata()
print(f"My sample is {smetadata}")
```

This will require that the user supplies enough flags
to determine the location of an xAOD file for this sample.
The simples option is the `--xAODlocation` flag, but it is also
possible to specify `--simulation_type`, `--energy` etc. instead,
or even a full `--dsid`.
Of all sample parameters, only `zv` has an inbuilt default, which is 0.
If the above lines were saved as `read_sample.py` 
an example of calling this would be;

```bash
python3 read_sample.py --simulation_type ATLFAST3 --simulation_release 22.0.62 \
        --energy 65536 --eta_range 20_25 --particle_content photons --verbose
```

Which is equivalent to

```bash
python3 read_sample.py --simulation_type ATLFAST3 --simulation_release 22.0.62 \
        --dsid photons_E65536_eta_20_25_z0 --verbose
```

All of these options are detailed in the help message, which
appears if the user provides the flag `--help`.
Note that the help message of any `ArgumentParser` or subclass is to appear
when the user passes a `--help` flag, then pyroot needs to be prevented
from swallowing the `--help` flag. 
By default, when a program imports pyroot, pyroot replaces
any `--help` flag behaviour with it's own behavior.
For an example of how to avoid this issue, look at the
`__root_importer` class in the code of the `hutils/root.py` module.

Optionally, you can allow the user to specify more than one sample.
This can be used for constructing comparisons.

```python
from hutils.metadata import MetaDataArgParser
# make an argument parser that obtains details of one or more samples
parser = MetaDataArgParser(description="Description to appear in help text.",
                           require_complete_metadata=False,
                           require_locations=[],
                           multisample=True,
                           strict=False)
multi_smetadata = parser.parse_metadata()
print(f"Found {len(multi_smetadata.samples)}")
for i, smetadata in enumerate(multi_smetadata.samples):
    print(f"Sample {i} is {smetadata}")
```

The specification of each sample are eliminated by three dots, `...`,
like the notation used to compare branches in git.
If the above lines were saved as `read_multi_samples.py` 
an example of calling this would be;

```bash
python3 read_multi_samples.py --simulation_type ATLFAST3...FullG4 --simulation_release 22.0.62 \
        --energy 65536 --eta_range 20_25 --particle_content photons...pions
```

Which would return metadata for two samples,
the first one specifying photons from the simulation ATLFAST3,
the second one specifying pions from the simulation FullG4.
Both of them have energy of 65536, an eta range of (-25, -20, 20, 25)
(if only two numbers are given in the eta range then symmetry is assumed),
and be from a simulation release labeled `22.0.62`.

In short when multiple options, delimited by `...`, are given
for a flag, it is assumed that each option refers to exactly one sample metadata,
when only one option is given for a flag it is assumed that this option applies
to all the sample metadata.

Aside from the special case of only giving one option, all flags must
be provided with the same number of options.

"""
# underscore avoid polluting the namespace
from __future__ import absolute_import
import os as _os
from . import particles as _particles
from . import dsid as _dsid
from . import files as _files


def copy_script_location():
    """
    Returns the location of the copy_metadata script on the disk.
    Useful if you need to copy metadata from one place to another in
    a bash script. See the copy_metadata documentation.

    Returns
    -------
    copy_script_location : str
        Full absolute path to the copy_metadata script.
    """
    this_location = _os.path.dirname(__file__)
    copy_script_location = _os.path.join(this_location, "copy_metadata.py")
    return _os.path.abspath(copy_script_location)


# gotta derive from object in for setter/getters
# to work in python2
class SampleMetadata(object):
    """
    Represents the metadata needed to specify the input used to produce a
    single sample of events. Has almost all of the behavior of a standard python
    dictionary with respect to the `SampleMetadata.valid_kw_args`.
    In particular, if another function, say `my_metadata_process` needs the values of 
    the keyword arguments as input, and you have a SampleMetadata object
    called `smetadata` with the correct values set it is valid to
    do `my_metadata_process(**smetadata)`.

    Attributes
    ----------
    valid_kw_args : list of str
        List of attributes that are valid keyword arguments for
        constructing this object.
        Items on this list that end in "location" or "folder"
        are file locations themselves, all other items
        are used to procedurally determine file locations.
    strict : bool
        True if attributes are required to conform to strict
        validity criteria. For more information about strict validity
        for attributes, see the documentation of `hutils/dsid.py`
        If this attribute is set to `True` after the SampleMetadata object
        has been created, then all of it's existing attributes will be
        checked, and `ValueError` is raised if they are non conforming.
    DSID : str
        The data sample id for this sample. 
        The form of a DSID takes the basic template;
        `<particles>_E<energy>_eta_<eta range>_z<zv>`,
        for more information, see the documentation of `hutils/dsid.py`.
    notes : list of str
        Any other information about the sample.
        Not used to determine the location of data files for
        the sample.
        Only for records.
    """
    _canvas_name = "MetaDataTextCanvas"
    _attribute_order = ['sample_name', 'simulation_type',
                        'simulation_release', 'reconstruction_release', 'particle_content',
                        'energy', 'eta_range', 'zv',
                        'params_location',
                        'EVNT_location', 'xAOD_location',
                        'flatTTree_location', 'plots_folder',
                        'date', 'base_dir']
    valid_kw_args = ['sample_name', 'simulation_type',
                     'simulation_release', 'reconstruction_release', 'particle_content',
                     'energy', 'eta_range', 'zv',
                     'params_location',
                     'EVNT_location', 'xAOD_location',
                     'flatTTree_location', 'plots_folder',
                     'base_dir']
    __default = object()
    def __init__(self, notes=None, strict=True, **kw_args):
        """
        Initialisation method for SampleMetadata.

        Parameters
        ----------
        notes : str or list of str (optional)
            Any other information about the sample.
            Not used to determine the location of data files for
            the sample.
            Only for records.
        strict : bool (optional)
            True if attributes are required to conform to strict
            validity criteria. For more information about strict validity
            for attributes, see the documentation of `hutils/dsid.py`
            If this attribute is set to `True` all arguments are
            checked, and `ValueError` is raised if they are non conforming.
            (Default value = True)
        simulation_type : str (optional)
            Name of the simulation used for the sample.
            The simulation type should be one of the entries on
            this (https://gitlab.cern.ch/atlas/athena/-/blob/master/simulation/isf/isf_config/python/isf_configconfigdb.py)
            web page.
        simulation_release : str (optional)
            The release of the simulation that was used for the sample.
            The simulation release is normally the athena version,
            though sometimes more information is given here.
        reconstruction_release : str (optional)
            The release of the reconstruction that was used for the sample.
            The simulation release is normally the athena version,
            though sometimes more information is given here.
        particle_content : str or int (optional)
            If int, will be interpreted as a pid.
            If it is a string that starts with "pid"
            the first 3 characters will be clipped off,
            and the remainder of the string treated as an int.
            If strict, must be found in `particles.valid_particle_content`
            or in `particles.known_pids`.
            If not strict, can be any string that doesn't include the
            substring `_E` or `.E` or any int.
        energy : int or float (optional)
            If strict, will be interpreted as a single number, it must be composed
            of only numbers, `.` or `p` (where `p` stands for `.`),
            excluding the first character, which can be `m` and `-` to make
            a negative energy.
            If not strict, can be any string that doesn't include
            the substring `_eta` or `.eta`.
        eta_range : tuple or str (optional)
            If str, must be a sequence of 2 or 4 integers, separated by `_`
            as with energy, the first character can be `-` or `m` to create a negative
            number, however decimal points are not possible.
            Will be converted to a tuple by `eta_range_to_tuple`.
            If not strict, can be any string that doesn't include
            the substring `_z` or `.z`.
            Will try to convert to a tuple by `eta_range_to_tuple`.
            If tuple, must be 2 or 4 ints.
        zv : int or float (optional)
            If strict, can be anything that can be converted to an int or float,
            any occurrences or `p` can be interpreted as `.` to form a float.
            If not strict, is unrestricted.
            
        
        """
        self.__dict = {}
        self.__set_date()
        self.strict = strict
        # set defaults
        if 'zv' not in kw_args:
            kw_args['zv'] = 0
        if 'sample_name' not in kw_args:
            kw_args['sample_name'] = 'unnamed'
        # read values
        for key in kw_args:
            self[key] = kw_args[key]
        # parse notes
        self.notes = []
        if isinstance(notes, str):
            self.notes.append(notes)
        elif hasattr(notes, '__iter__'):
            self.notes += [n for n in notes]
        elif notes is not None:
            self.notes.append(notes)

    def __getitem__(self, key):
        return self.__dict[key]

    def __len__(self):
        return len(self.__dict)

    def get(self, key, default=__default):
        """
        Return the value of any item from `SampleMetadata.valid_kw_args`,
        else default if given.

        Parameters
        ----------
        key : str
            Name of key word argument to fetch
        default : any (optional)
            Value to return if key word argument is not set for this
            SampleMetadata.

        Raises
        ------
        KeyError
            If no default is given, and key is not set for this sample
            this error will be raised.

        """
        if default is not SampleMetadata.__default:
            return self.__dict.get(key, default)
        else:
            return self.__dict.get(key)

    @staticmethod
    def process_particle(particle):
        """
        Try to convert a particle to standard form; plural string.
        Note if the conversion is successful.

        Parameters
        ----------
        particle : str or int
            If int, will be interpreted as a pid.
            Can be any string that doesn't include the
            substring `_E` or `.E`, or any int.
            If it is a string that starts with "pid"
            the first 3 characters will be clipped off,
            and the remainder of the string treated as an int.
            Will be declared valid if be found in
            `particles.valid_particle_content` or in `particles.known_pids`.

        Returns
        -------
        particle : str or int
            particle, converted to one of the entries in
            `particles.valid_particle_content` if possible
        valid_particle : bool
            True if the conversion was successful
        error_message : str
            Error message, which describes the problem if
            the conversion did not succeed.

        """
        if particle in _particles.valid_particle_content:
            valid_particle = True
        elif isinstance(particle, str):
            if particle.startswith("pid"):
                particle = particle[3:]
            try:  # can we make it a pid?
                particle = int(particle)
            except ValueError:
                valid_particle = False

        if isinstance(particle, int):
            if particle in _particles.known_pids:
                particle = _particles.pid_to_name(particle)
                valid_particle = True  # MCpid
            else:
                valid_particle = False # maybe pid, but not knonw

        error_message = "particle_content -> {}".format(particle)
        if not valid_particle:
            error_message += "Did not recognise as a pid or " + \
                             "composite particle; {}"\
                             .format(_particles.composite_particle_content)
        return particle, valid_particle, error_message

    @classmethod
    def process_item(cls, key, item):
        """
        Try to convert an item from `SampleMetadata.valid_kw_args`
        to standard form, as defined by strict settings.
        All "location" keys are converted to full paths.
        Note if the conversion is successful.

        Parameters
        ----------
        key : str
            Attribute name from `SampleMetadata.valid_kw_args`
        item : various
            Item to be processed, see the documentation for 
            `SampleMetadata.__init__` to learn about target 
            formats and values.

        Returns
        -------
        item : various
            item that has been converted to strict form where possible.
        valid : bool
            True if the conversion was successful
        error_message : str
            Error message, which describes the problem if
            the conversion did not succeed.

        """
        error_message = "{} -> {}".format(key, item)
        if key in ['sample_name', 'simulation_type', 'simulation_release', 'reconstruction_release']:
            valid = isinstance(item, str)
            error_message += "not a string".format(key, item)
        elif key == 'particle_content':
            # try it as a single particle
            item, valid, error_message = cls.process_particle(item)
            if not valid and hasattr(item, '__iter__'):
                particles = []
                for part in item:
                    p, valid_p, err_p = cls.process_particle(part)
                    if not valid_p: break
                else:  # didn't hit the break, so all valid
                    valid = True
                    item = '_'.join(particles)
        elif key in ['energy', 'zv']:
            valid = False
            if isinstance(item, (int, float)):
                valid = True
            elif isinstance(item, str):
                try:
                    item = int(item)
                    valid = True
                except ValueError:
                    valid = False
            if valid and key == 'energy' and item < 0:
                valid = False
                error_message += " energy must be positive"
            else:
                error_message += " not an int or float"
        elif key == 'eta_range':
            valid = True
            if isinstance(item, str):
                try:
                    item = _dsid.eta_range_to_tuple(item)
                except ValueError:
                    valid = False
                    error_message += " Cannot convert this string to eta range."
            if not len(item) in [2, 4]:
                valid = False
                error_message += " There should be 2 or 4 values in eta range."
            for part in item:
                if not isinstance(part, int):
                    valid = False
                    error_message += " Eta range should contain only ints."
                    break
        elif key.endswith('location') or key.endswith('folder'):
            valid = isinstance(item, str)
            error_message += " should be a filepath string"
            item = _os.path.abspath(item)
        elif key == "base_dir":
            valid = isinstance(item, str)
            error_message += " should be a string"
        else:
            if key in cls.valid_kw_args:
                raise NotImplementedError("Failed to process " + key)
            raise KeyError("{} not a valid key".format(key))
        if valid:
            error_message += " no error, conversion succeeded"
        return item, valid, error_message

    def __setitem__(self, key, item):
        item, valid, error_message = self.process_item(key, item)
        if valid or not self.strict:
            self.__dict[key] = item
        else:
            raise ValueError(error_message)

    def set_without_check(self, **kwargs):
        """
        Set values into the dictionary of keyword arguments
        without any checks. Ignores whether the items are in
        `SampleMetadata.valid_kw_args` does not perform conversions,
        and ignores the strict criteria if set.
        Using this to add items not specified in `SampleMetadata.valid_kw_args`
        is very likely to break functions of this class.
        To be used with caution.

        Parameters
        ----------
        to_add : various (optional)
            Any key and value to be added to the dictionary of
            keyword arguments. May specify multiple at once.
            Must be given as a `key=value` argument.

        """
        for key, item in kwargs.items():
            self.__dict[key] = item

    @property
    def strict(self):
        return self._strict

    @strict.setter
    def strict(self, value):
        value = bool(value)
        # need to change the attribute
        self._strict = value
        if not value:
            return  # no action needed
        # check each value is compliant
        for key in self.valid_kw_args:
            if key not in self:
                continue
            value, valid, error_message = self.process_item(key, self[key])
            if not valid:
                self._strict = False
                raise ValueError(error_message)
            self[key] = value

    def __set_date(self):
        from .general import get_date
        str_date = get_date()
        self.__dict['date'] = str_date

    def keys(self):
        """
        List keys in the keyword arguments whose values have been specified.

        Returns
        -------
        keys : list of str
            List keys whose values have been specified,
            in a fixed order.
            
        """
        keys = [key for key in self._attribute_order
                if key in self.__dict]
        return keys

    def __contains__(self, key):
        return key in self.__dict

    def __iter__(self):
        return iter(self.__dict)

    def __eq__(self, other):
        if len(self) != len(other):
            return False
        for key in self:
            if key not in other:
                return False
            try:
                same_value = self[key] == other[key]
                assert isinstance(same_value, bool)
            except Exception:  # in the case of problems, compare strings
                same_value = str(self[key]) == str(other[key])
            if not same_value:
                return False
        if len(self.notes) != len(other.notes):
            return false
        for my_note, other_note in zip(self.notes, other.notes):
            if my_note != other_note:
                return false
        return True

    def __ne__(self, other):
        return not self.__eq__(other)

    def __str__(self):
        output = ""
        for key in self.keys():
            output += "{}={}\n".format(key, self[key])
        output += "Notes ~~~~~~\n"
        for note in self.notes:
            output += "{}\n".format(note)
        return output

    @classmethod
    def read_string(cls, string, strict=True):
        """
        Alternative constructor.
        Constructs an object of type SampleMetadata from the string
        representation of an object of type SampleMetadata.

        Parameters
        ----------
        string : str
            String created by converting object of type
            SampleMetadata to string.
        strict : bool (optional)
            True if values in the string should be parsed with
            strict settings. See the main `SampleMetadata.__init__`
            method for more information.

        Returns
        -------
        new_metadata : SampleMetadata
            A clone of the SampleMetadata object that created the
            input string, wither the exception of any changes forced by the
            strict criteria and possibly the date.

        """
        date = False
        attributes = {}
        lines = string.split('\n')
        while lines:
            next_line = lines.pop(0)
            if next_line.startswith("Notes ~~~~~~"):
                break
            key, value = next_line.split('=', 1)
            if key == "date":
                date = value
            else:
                # the item setter takes care of type conversions
                attributes[key] = value
        # enter the notes
        notes = []
        while lines:
            next_line = lines.pop(0).strip()
            if not next_line:
                break  # line has no content
            notes.append(next_line)
        new_metadata = cls(notes=notes, strict=strict, **attributes)
        new_metadata.set_without_check(date=date)
        return new_metadata

    @classmethod
    def read_text_file(cls, filepath, strict=True):
        """
        Alternative constructor.
        Constructs an object of type SampleMetadata from a text file,
        using the `SampleMetadata.read_string` method.

        Parameters
        ----------
        filepath : str
            Path to a text file containing a string created by
            converting object of type SampleMetadata to string.
        strict : bool (optional)
            True if values in the file should be parsed with
            strict settings. See the main `SampleMetadata.__init__`
            method for more information.

        Returns
        -------
        new_metadata : SampleMetadata
            A clone of the SampleMetadata object that created the
            input file, with the exception of any changes forced by the
            strict criteria and possibly the date.

        """
        with open(filepath, 'r') as inputfile:
            content = inputfile.read()
        return cls.read_string(content, strict=strict)

    def write_text_file(self, filepath, overwrite=True):
        """
        Create a human readable text file from this SampleMetadata object.
        Contains all information except for the value of the strict criteria.

        Parameters
        ----------
        filepath : str
            Path at which to write the text file
        overwrite : bool (optional)
            If `True`, any existing file at the given file path will
            be overwritten.
            (Default value = `True`)

        Raises
        ------
        AssertionError
            If overwrite is set to `False` and a file already exists
            at the given filepath.
        """
        if not overwrite:
            assert not os.path.exists(filepath),\
                "Error writing, {} already exists".format(filepath)
        with open(filepath, 'w') as outputfile:
            outputfile.write(str(self))

    def set_location(self, for_format, path):
        """
        Given a path and which format it corrisponds to, 
        set the metadatas record for that format.

        Parameters
        ----------
        for_format : str
            String indicating the format of the data that 
            this path points to. Must be one of 
            "EVNT", "xAOD", "flatTTree" or "images"
        path : str
            Path to file on disk. Not required to actually exist.

        Raises
        ------
        ValueError
            If the for_format value is not a known format.

        """
        data_formats = ["EVNT", "xAOD", "flatTTree", "images"]
        if for_format not in data_formats:
            raise ValueError("Invalid value for for_format. " + 
                             "Expected one of {}, found {}"
                             .format(data_formats, for_format))
        if for_format == "images":
            location = "plots_folder" 
        else:
            location = for_format + "_location"
        self[location] = path

    @classmethod
    def read_root_file(cls, filepath, strict=True, path_is_location=False):
        """
        Look in the specified root file for a canvas named "MetaDataTextCanvas"
        at top level and read the text on it to create a SampleMetadata
        object.

        Parameters
        ----------
        filepath : str
            Path to the root file to read from.
        strict : bool (optional)
            True if values in the file should be parsed with
            strict settings. See the main `SampleMetadata.__init__`
            method for more information.
        path_is_location : False or str
            If this is a string it indicates that the given file path
            corrisponds to the location of one format of this dataset.
            Valid formats are "EVNT", "xAOD", "flatTTree" or "images".

        Returns
        -------
        new_metadata : SampleMetadata
            A clone of the SampleMetadata object that wrote the text on the
            canvas, with the exception of any changes forced by the
            strict criteria and possibly the date.

        """
        from .root import read_canvas_text
        content = read_canvas_text(filepath, cls._canvas_name)
        if path_is_location:
            content.set_location(path_is_location, filepath)
        return cls.read_string(content, strict=strict)

    def write_root_file(self, filepath, overwrite=True):
        """
        Add the SampleMetadata to the specified root file in
        human (and TBrowser) readable format, on a canvas called
        "MetaDataTextCanvas". If the root file does not exist,
        create it.

        Parameters
        ----------
        filepath : str
            Path for the root file to write to.
        overwrite : bool (optional)
            If there is a root file already at the path,
            and the root file already has a "MetaDataTextCanvas",
            should it be overwritten?
            (Default value = `False`)

        Raises
        ------
        AssertionError
            If overwrite is set to `False` and a root file already
            exists at the given filepath and that root file already has
            a "MetaDataTextCanvas" at the top level.
        
        """
        from .root import write_canvas_text, has_canvas
        if not overwrite:
            assert not has_canvas(filepath, self._canvas_name), \
                "Error writing, {} already has metadata".format(filepath)
        write_canvas_text(filepath, self._canvas_name, str(self))

    def write_known_roots(self, overwrite=False):
        """
        For all root files known to contain data from this sample,
        because they are referred to by a "location" keyword,
        write the SampleMetadata on the root file

        Parameters
        ----------
        overwrite : bool (optional)
            If there is a root file already at any path,
            and the root file already has a "MetaDataTextCanvas",
            should it be overwritten?
            (Default value = `False`)

        Raises
        ------
        AssertionError
            If overwrite is set to `False` and a root file already
            exists at the given filepath and that root file already has
            a "MetaDataTextCanvas" at the top level.
        
        """
        known_roots = [self[key] for key in self.keys()
                       if key.endswith('location') and 
                       self[key].endswith('.root')]
        for root in known_roots:
            try:
                self.write_root_file(root, overwrite)
            except AssertionError as e:
                if not overwrite:
                    print(str(e).replace("Error", "Problem"))
                else:
                    raise e

    @property
    def DSID(self):
        try:
            return _dsid.make_DSID(**self)
        except TypeError:
            raise AttributeError("This SampleMetadata dosn't "
                                 "have enough information to make a DSID")

    @classmethod
    def harvest_root_path(cls, path, strict=True, path_is_location=False):
        """
        Alternative constructor.
        Get the most info possible from a path for a root file.
        Constructs an object of type SampleMetadata, preferentially using
        information written to the root file itself (if the root file exists)
        using `SampleMetadata.read_root_file`,
        but also deducing information from the structure of the path of the
        root file.
        Fault tolerant, will almost always return some SampleMetadata rather than
        raise an error, but the SampleMetadata may have many or all keyword values
        unset if their values cannot be determine automatically.

        Parameters
        ----------
        path : str
            Path that may point to a root file to read from,
            and may contain information about the sample characteristics
            in and of itself.
        strict : bool (optional)
            True if values in the file and path should be parsed with
            strict settings. See the main `SampleMetadata.__init__`
            method for the impact of strict settings on reading data from the 
            root file, and the documentation of `hutils/files.py` for information
            about strict settings for interpreting a path.
        path_is_location : False or str
            If this is a string it indicates that the given file path
            corrisponds to the location of one format of this dataset.
            Valid formats are "EVNT", "xAOD", "flatTTree" or "images".

        Returns
        -------
        found : SampleMetadata
            A sample metadata object containing information gathered from
            reading the root file, if it exists, and interpreting the file path.
            Information read from the root file takes precedence.
        
        """
        found = cls()
        if _os.path.exists(path):
            try:
                found = cls.read_root_file(path, strict=strict)
            except Exception:
                print("Couldn't read metadata in " + path)
        else:
            print("File {} unreadable or non-existant, ".format(path)
                  + "infering all metadata from path.")
        try:
            dsid = _files.extract_DSID(path, strict=strict)
            particle_str, energy, eta_range, zv = \
                _dsid.decode_DSID(dsid, strict=strict)
        except Exception:
            pass
        else:
            for key, value in [('particle_content', particle_str),
                               ('energy', energy),
                               ('eta_range', eta_range),
                               ('zv', zv)]:
                if key not in found:
                    found[key] = value
        if path_is_location:
            found.set_location(path_is_location, path)
        if 'base_dir' not in found:
            try:
                base_dir = _files.extract_base_dir(path)
            except NotImplementedError:
                pass
            else:
                found['base_dir'] = base_dir
        return found


def add_dsid_to_metadata(dsid_list, metadata):
    seperate_decodes = [_dsid.decode_DSID(dsid) for dsid in dsid_list]
    keys = ["particle_content", "energy", "eta_range", "zv"]
    if len(seperate_decodes) > 1:
        dsid_data = ['...'.join([str(d[i]) for d in seperate_decodes]) for i in range(len(keys))]
    else:
        dsid_data = seperate_decodes[0]
    def close(a, b):
        return abs(a-b) < 1e-3
    test_functions = {"particle_content": _particles.are_equal,
                      "energy": close,
                      "eta_range": _dsid.eta_ranges_equal,
                      "zv": close,}
    for key, value in zip(keys, dsid_data):
        if key not in metadata:
            metadata[key] = value
        elif key == "zv" and metadata[key] == 0:
            metadata[key] = value
        elif not test_functions[key](value, metadata[key]):
            raise ValueError("Given dsid {} which contains {} with "
                             "value {} but the metadata already "
                             "contains {} with value {}."
                             " Conflict. Full metadata;\n{}").format(
                                dsid, key, value, key, metadata[key], metadata)


class MultiSampleMetadata(SampleMetadata):
    """
    Represents the metadata needed to specify the input used to produce a
    one or more samples of events. Has almost all of the behavior of a standard python
    dictionary with respect to the `SampleMetadata.valid_kw_args`.
    Inherits from `SampleMetadata`, and also contains one or more
    `SampleMetadata` object representing each sample in the set.

    Works on the principle that options that vary are written in a string
    representation, and each item is delimited by `...`.
    For example, if the multisample called `multi_smetadata` contained samples with energies
    10, 65536 and 10000, the value of `multi_smetadata['energy']` would be
    `10...65536...10000`.
    If all the samples in `multi_smetadata` where for photons,
    then the value of `multi_smetadata['particle_content']` would simply
    be `photons` because single value are considered general to all the samples.

    This notation is inspired by the notation git uses to compare branches in
    the log. As it introduces no spaces, or other special characters,
    the values can still be used as part of a valid directory or file path.

    Attributes
    ----------
    num_samples : int
        Number of distinct SampleMetadata objects that this MultiSampleMetadata
        object comprises of.
    samples : tuple of SampleMetadata
        Each of the samples referred to in this MultiSampleMetadata,
        constructed as a single SampleMetadata object.
    uniform_attributes : set
        Names of keyword attributes, that are common to
        all sample metadata in the set.
    varying_attributes : set
        Names of keyword attributes, that vary between sample metadata
        in the set.
    valid_kw_args : list of str
        List of attributes that are valid keyword arguments for
        constructing this object.
        Items on this list that end in "location" or "folder"
        are file locations themselves, all other items
        are used to procedurally determine file locations.
    strict : bool
        True if attributes are required to conform to strict
        validity criteria. For more information about strict validity
        for attributes, see the documentation of `hutils/dsid.py`
        If this attribute is set to `True` after the SampleMetadata object
        has been created, then all of it's existing attributes will be
        checked, and `ValueError` is raised if they are non conforming.
    DSID : str
        Varient of data sample id for this sample. 
        The form of a DSID takes the basic template;
        `<particles>_E<energy>_eta_<eta range>_z<zv>`,
        for more information, see the documentation of `hutils/dsid.py`.
        This DSID may contain at least one `...` deliminator,
        indicating the parts of the sample that vary.
    """
    sample_sep = "..."  # inspired by git
    def __init__(self, *args, **kwargs):
        non_kws = {key: value for key, value in kwargs.items()
                   if key not in SampleMetadata.valid_kw_args}
        self.__samples = [SampleMetadata(*args, **non_kws)]
        self.__found_split = False
        self.num_samples = 'unknown'
        self.uniform_attributes = set()
        self.varying_attributes = set()
        # python 2/3 cross compatable
        # shoud take case of setting the sample kw_args too
        # via the overwritten __setitem__ method
        super(type(self), self).__init__(*args, **kwargs)

    @property
    def samples(self):
        #  really don't want to add a sample by appending it
        # so jey make the samepl list an immutable
        return tuple(self.__samples)

    @property
    def strict(self):
        return self._strict

    @strict.setter
    def strict(self, value):
        value = bool(value)
        self._strict = value
        #  apply to all contained samples
        for sample in self.samples:
            sample.strict = value

    def set_for_all_samples(self, key, item):
        """
        For all the sample metadata variants in this MultiSampleMetadata,
        set the value of the keyword variable to the specified value.

        Parameters
        ----------
        key : str
            name of the keyword attribute from `SampleMetadata.valid_kw_args`
            to be set to a uniform value.
        item : various
            common value to set
        """
        self.uniform_attributes.add(key)
        super(type(self), self).__setitem__(key, item)
        for sample in self.samples:
            sample[key] = item

    def set_for_each_sample(self, key, parts):
        """
        For each of the sample metadata variants in the MultiSampleMetadata
        set the value of the keyword variable to a different value.

        Parameters
        ----------
        key : str
            name of the keyword attribute from `SampleMetadata.valid_kw_args`
            to be set to varying values.
        parts : iterable
            One value for each sample metadata in the set.

        Raises
        ------
        ValueError
            Raised if the number of samples in this set
            does not match the length of the parts iterable given.

        """
        self.varying_attributes.add(key)
        n_parts = len(parts)
        if self.__found_split:
            # consitancy check
            if not n_parts == self.num_samples:
                message = ("Existing inputs are for {} samples "
                           "but key {} with parts {} contains "
                           "{} parts. All inputs must be a string "
                           "with either a single value to apply to all "
                           "samples (set using the method "
                           "set_for_all_samples, not set_for_each_sample)"
                           " or exactly as many values as there are "
                           "samples, seperated by '{}'"
                           ).format(self.num_samples, key, parts,
                                    n_parts, self.sample_sep)
                raise ValueError(message)
        else:  #  first time we found a split event
            self.__found_split = True
            self.num_samples = n_parts
            # make the first template sampel into the correct number
            # of samples
            import copy
            self.__samples += [copy.deepcopy(self.samples[0])
                               for _ in range(n_parts-1)]
        # at this point we know we have the name number of parts and samples
        if key == "eta_range":
            # sometimes eta_range is only paritally specified
            # in that case treat any eta_ranges that could be identical as the same
            if all(_dsid.eta_ranges_equal(parts[0], part) for part in parts[1:]):
                self.set_for_all_samples(key, parts[0])
                return
        processed_parts = []
        for part, sample in zip(parts, self.samples):
            sample[key] = part
            #  if there was a problem in any part setting it in the sample
            # would raise an error
            # the sample will also properly process the part
            processed = sample[key]
            if key == "eta_range":
                processed = _dsid.eta_range_to_string(processed, force_long=False)
            else:
                processed = str(processed)
            processed_parts.append(processed)
        joined_parts = self.sample_sep.join(processed_parts)
        #  don't do any checks or processing on the joined string though
        self.set_without_check(**{key: joined_parts})

    def __setitem__(self, key, item):
        has_split = False
        if isinstance(item, list) and len(item)>1:
            parts = item
            has_split = True
        else:
            item = item[0] if isinstance(item, list) else item
            try:
                if self.sample_sep in item:
                    parts = item.split(self.sample_sep)
                    has_split = True
            except TypeError:
                pass  # can't do sample_sep in item
        if has_split:
            self.set_for_each_sample(key, parts)
        else:
            self.set_for_all_samples(key, item)

    def add_sample(self, metadata):
        """
        Add a whole sample or set of samples to this set.
        If the sample is not fully specified, any unspecified
        keyword arguments that are uniform for this MultiSampleMetadata
        are added to the sample.

        Parameters
        ----------
        metadata : SampleMetadata or MultiSampleMetadata
            One or more sample metadata to be added to this set

        """
        if isinstance(metadata, MultiSampleMetadata):
            for sample in metadata.samples:
                self.add_sample(sample)
            return
        n_default_attributes = 2
        if self.__found_split:
            # there is already more than one sample in this set
            self.num_samples += 1
            self.__samples.append(metadata)
        elif len(self.uniform_attributes) > n_default_attributes:
            # there is at least one uniform attribute for this set
            # so consider this the first split
            self.num_samples = 2
            self.__found_split = True
            self.__samples.append(metadata)
        else:
            # this is truly a new multisample
            self.num_samples = 1
            self.__found_split = True
            self.__samples = [metadata]
        # now to fix the keys of the multisample object
        for key in metadata:
            value = metadata[key]
            if key in self.varying_attributes or value != self.get(key, value):
                # if it was a uniform_attributes
                # it isn't any longer
                self.uniform_attributes.discard(key)
                parts = [sample[key] for sample in self.samples]
                self.set_for_each_sample(key, parts)
            elif key in self.valid_kw_args:  # dont set things that shouldn't be set
                # if it's already the value for this key
                # this funciton is a noop
                self.set_for_all_samples(key, value)

    @classmethod
    def __multi_read(cls, path, func_name, **kwargs):
        read_func = getattr(super(MultiSampleMetadata, cls), func_name)
        new_multimetadata = cls(strict=kwargs['strict'])
        if isinstance(path, list) and len(path) > 1:
            parts = path
        else:
            if isinstance(path, list):
                path = path[0]
            parts = path.split(cls.sample_sep)
        for part in parts:
            # first, read it as a single multisample
            multidata = read_func(part, **kwargs)
            # then add the samples into a new one
            for sample in multidata.samples:
                new_multimetadata.add_sample(sample)
        return new_multimetadata

    @classmethod
    def read_root_file(cls, path, strict=True, path_is_location=False):
        """
        Look in the specified root file for a canvas named "MetaDataTextCanvas"
        at top level and read the text on it to create a MultiSampleMetadata
        object that contains a single sample.

        Parameters
        ----------
        filepath : str
            Path to the root file to read from.
        strict : bool (optional)
            True if values in the file should be parsed with
            strict settings. See the main `SampleMetadata.__init__`
            method for more information.
        path_is_location : False or str
            If this is a string it indicates that the given file path
            corrisponds to the location of one format of this dataset.
            Valid formats are "EVNT", "xAOD", "flatTTree" or "images".

        Returns
        -------
        new_metadata : MultiSampleMetadata
            A MultiSampleMetadata containing just one sample which is
            the clone of the SampleMetadata object that wrote the text on the
            canvas, with the exception of any changes forced by the
            strict criteria and possibly the date.

        """
        return cls.__multi_read(path, "read_root_file", strict=strict,
                                path_is_location=path_is_location)

    @classmethod
    def read_text_file(cls, path, strict=True):
        """
        Alternative constructor.
        Constructs an object of type MultiSampleMetadata from a text file.

        Parameters
        ----------
        filepath : str
            Path to a text file containing a string created by
            converting object of type MultiSampleMetadata or
            SampleMetadata to string.
        strict : bool (optional)
            True if values in the file should be parsed with
            strict settings. See the main `SampleMetadata.__init__`
            method for more information.

        Returns
        -------
        new_metadata : MultiSampleMetadata
            A clone of the MultiSampleMetadata object that created the
            input file, with the exception of any changes forced by the
            strict criteria and possibly the date.

        """
        return cls.__multi_read(path, "read_text_file", strict=strict)

    @classmethod
    def harvest_root_path(cls, path, strict=True, path_is_location=False):
        """
        Alternative constructor.
        Get the most info possible from a path for a root file.
        Constructs an object of type MultiSampleMetadata, preferentially using
        information written to the root file itself (if the root file exists)
        using `MultiSampleMetadata.read_root_file`,
        but also deducing information from the structure of the path of the
        root file.
        Fault tolerant, will almost always return some MultiSampleMetadata rather than
        raise an error, but the MultiSampleMetadata may have many or all keyword values
        unset if their values cannot be determined automatically.

        Parameters
        ----------
        path : str
            Path that may point to a root file to read from,
            and may contain information about the sample characteristics
            in and of itself.
        strict : bool (optional)
            True if values in the file and path should be parsed with
            strict settings. See the main `SampleMetadata.__init__`
            method for the impact of strict settings on reading data from the 
            root file, and the documentation of `hutils/files.py` for information
            about strict settings for interpreting a path.
        path_is_location : False or str
            If this is a strign it indicates that the given file path
            corrisponds to the location of one format of this dataset.
            Valid formats are "EVNT", "xAOD", "flatTTree" or "images".

        Returns
        -------
        found : MultiSampleMetadata
            A multiple sample metadata object containing information gathered from
            reading the root file, if it exists, and interpreting the file path.
            Information read from the root file takes precedence.
        
        """
        return cls.__multi_read(path, "harvest_root_path", strict=strict,
                                path_is_location=path_is_location)


import argparse as _argparse
class MetaDataArgParser(_argparse.ArgumentParser):
    """
    Argument parser specialised for getting one or more objects
    of type SampleMetadata.
    As a subclass of `argparse.ArgumentParser` it behaves in every way like a regular
    `ArgumentParser`, with some added features.
    It comes with all flags which could help to specify SampleMetadata
    preset, with relevant help messages.
    It can optionally raise a human readable error,
    if not enough information to construct a
    full SampleMetadata or find a required data file is given.
    It can construct the SampleMetadata (or MultiSampleMetadata)
    with `MetaDataArgParser.parse_metadata`, which behaves
    in an equivalent manner to `MetaDataArgParser.parse_args`, or
    equally `ArgumentParser.parse_args`.

    Attributes
    ----------
    prefilled : dict of str
        Primary name of each flag, and their help messages.
    universal_conditions : dict
        Additional kwargs that should be passed when all the
        flags are added with `add_argument`.
    special_conditions : dict of dict
        Additional kwargs that should be passed when a specified flag
        is added with `add_argument`.
    metadata : SampleMetadata or MultiSampleMetadata
        The specified metadata for the samples.
        Not filled out until `MultiSampleMetadata.parse_metadata` 
        has been called.

    """
    prefilled = {'sample_name' : "Name for the sample.",
                 'simulation_type' : "Name of the type of simulation for the sample.",
                 'simulation_release' : "Release of the simulation for the sample.",
                 'reconstruction_release' : "Release of the reconstruction for the sample.",
                 'particle_content' : "Particle content of the sample, lowercase, plural.",
                 'energy' : "Energy of the sample.",
                 'eta_range' : "Eta range of the sample.",
                 'zv' : "zv of the sample.",
                 'params_location' : "Full path of the params file for the sample.",
                 'EVNT_location' : "Full path of the EVNT file for the sample.",
                 'xAOD_location' : "Full path of the xAOD file for the sample.",
                 'flatTTree_location' : "Full path of the flatTTree for the sample.",
                 'plots_folder' : "Folder path of the plots for the sample.",
                 'base_dir' : "Base directory for paths for the sample.",
                 'dsid' : "Data sample id for the sample of the form; "
                          "`<particles>_E<energy>_eta_<eta range>_z<zv>`.",
                 }
    universal_conditions = {}
    __default = object()
    # note that using *args and **kwargs ensures that inheritance
    # and multiple inheritance work as intended
    def __init__(self, *args, **kwargs):
        """
        Initialisation method.

        Parameters
        ----------
        description : str (optional)
            Text to display before the argument help
            (Default value = `""`)
        strict : bool (optional)
            True if attributes are required to conform to strict
            validity criteria. For more information about strict validity
            for attributes, see the documentation of `hutils/dsid.py`
            `ValueError` is raised if any values given are non conforming.
            (Default value = `False`)
        multisample : bool (optional)
            If true, a MultiSampleMetadata is created and any `...` in
            the flags are interpreted as the delimitors for values that
            vary between the samples.
            If `False` a single SampleMetadata object is created, and 
            `...` is not considered to be a delimitor (just a strange part
            of the sample value).
            Alternatively, a flag can just be given many values,
            these will be interpreted the same way as a `...` seperated string.
            (Default value = `False`)
        require_complete_metadata : bool (optional)
            If `True` an error should be raised if the user does not
            supply all sufficient information to completely fill the
            physical parameters of the SampleMetadata.
            This can be by filling all flags that are not "location"s
            or by providing at least one "location" that contains
            a record of the complete metadata for the sample.
            (Default value = `False`)
        require_locations : list of str (optional)
            This list may contain the string representing any of the
            5 data types; `params`, `EVNT`, `xAOD`, `flatTTree` and/or
            `plots`. If the user does not supply a value for the corrisplonding
            location `parse_metadata` will attempt to construct it
            using information in other flags/found in other specified files.
            If it is not possible to get a location for this, an error will
            be raised.
            (Default value = `[]`)
            
        """
        # some attributes specific to the subclass
        subclass_attrs = {'require_complete_metadata': True,
                          'strict': False,
                          'multisample': False,
                          'require_locations': []}
        for name in subclass_attrs:
            if name in kwargs:
                setattr(self, name, kwargs[name])
                del kwargs[name]
            else:
                setattr(self, name, subclass_attrs[name])
        self.special_conditions = {}
        if self.strict and not self.multisample:
            self.special_conditions['particle_content'] = \
                dict(choices=_particles.valid_particle_content)
            self.special_conditions['energy'] = dict(type=int)
            self.special_conditions['zv'] = dict(type=int)
        elif self.multisample:
            self.universal_conditions['nargs'] = '*'
        # superclass initialisation
        super(type(self), self).__init__(*args, **kwargs)
        # add the metadata arguments
        self._add_metadata_args()
        # will be filled out when the args are parsed
        self.metadata = self.__init_metadata()

    def _add_metadata_args(self):
        for name, help_msg in self.prefilled.items():
            flags = ['--' + name]
            if '_' in name:
                flags.append('--' + name.replace('_', ''))
            additional_args = self.special_conditions.get(name, {})
            additional_args.update(self.universal_conditions)
            self.add_argument(*flags,
                              dest=name,
                              default=self.__default,
                              help=help_msg,
                              **additional_args)

    def __init_metadata(self):
        if self.multisample:
            metadata = MultiSampleMetadata(strict=self.strict)
        else:
            metadata = SampleMetadata(strict=self.strict)
        return metadata

    def get_full_keys(self):
        """
        Get a list of keyword arguments required for the SampleMetadata
        to be considered complete. This list is also necessary and sufficient
        for procedurally deciding on the locations for files.

        Returns
        -------
        full_keys : set of str
            List of keys required to consider a SampleMetadata complete.
            
        """
        full_keys = []
        for name in self.metadata.valid_kw_args:
            if name.endswith('location') or name.endswith('folder'):
                # locations can be constructed from other data
                continue
            if name == "sample_name":
                # not required
                continue
            full_keys.append(name)
        return set(full_keys)

    def parse_metadata(self, args=None, namespace=None):
        """
        Generate the metadata described by the users flag choices.
        Mimics the `ArgumentParser.parse_args` function.
        Fills in the attribute of this object `MetaDataArgParser.metadata`
        and returns it.

        Parameters
        ----------
        args : list of str (optional)
            The arguments as a list of strings, as they would be presented on
            the command line.
            If neither this nor the namespace object are supplied,
            the command line arguments will be read to obtain the needed
            information.
        namespace : argparse.Namespace
            An object that has been returned after calling `parse_args`
            on this object, it has attributes corresponding to
            the values of the flags set by the user.
            If neither this nor the args object are supplied,
            the command line arguments will be read to obtain the needed
            information.

        Returns
        -------
        metadata : SampleMetadata or MultiSampleMetadata
            Class depends on the value given to `multisample`
            at initialisation. It is the metadata described by the
            flags the user has set, combined with information
            harvested from any files pointed to by "location" flags.

        """
        if namespace is None:
            namespace = self.parse_args(args=args)
        # check what's possible to construct
        #  list of things that are really needed
        unfound = self.get_full_keys()
        unfound.discard('zv')  #  gets assumed to be 0 if not given
        # check the namespace for things that are metadata inputs
        namedict = vars(namespace)
        valid_inputs = self.metadata.valid_kw_args
        for name, value in namedict.items():
            if value == self.__default:
                continue
            if name in valid_inputs:
                #  add valid items to out metadata
                self.metadata[name] = value
                unfound.discard(name)
        if not unfound:  # done
            return self.metadata
        #  check the locations for metadata,
        #  from most derived to lowest level
        location_precidence = ["flatTTree_location", "xAOD_location",
                               "EVNT_location"]
        for loc in location_precidence:
            value = namedict[loc]
            if value == self.__default:
                continue
            found = self.metadata.harvest_root_path(
                namedict[loc], path_is_location=loc.split('_', 1)[0])
            found.strict = self.strict
            for key in found:
                if key not in valid_inputs:
                    continue
                if key in self.metadata:
                    continue  # don't overwrite
                #  if we are multisamplign, the found may have varying_attributes
                if self.multisample and key in found.varying_attributes:
                    value = [sample[key] for sample in found.samples]
                    self.metadata.set_for_each_sample(key, value)
                else:
                    self.metadata[key] = found[key]
                unfound.discard(key)
        #  finally, if there is a dsid, add it
        if namedict['dsid'] != self.__default:
            add_dsid_to_metadata(namedict['dsid'], self.metadata)
            unfound = unfound - {'energy', 'particle_content', 'eta_range'}
        if self.require_complete_metadata:
            assert len(unfound) == 0, \
                "Didn't find a value for " + ', '.join(unfound)
        for loc in self.require_locations:
            if loc == "plots":
                full_name = loc + "_folder"
            else:
                full_name = loc + "_location"
            if full_name in self.metadata:
                continue
            if unfound:  # otherwise, need all metadata
                message = "Not enough information given to find " + full_name
                message += "\n need to supply {} directly or all of {}"\
                           .format(full_name, unfound)
                raise ValueError(message)
            # add it!
            location = _files.locate(data_type=loc, **self.metadata)
            self.metadata[full_name] = location
        return self.metadata


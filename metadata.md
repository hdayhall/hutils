
<!-- create_documentation.py START Module metadata -->

# Module metadata

*Documentation for the code at `metadata.py`*

Objects representing sample metadata.

Data samples used to characterise simulations have some core characteristics;
1. `simulation_type` Name of the simulation used for the sample.
The simulation type should be one of the entries on
\[this\]\(https://gitlab.cern.ch/atlas/athena/\-/blob/master/simulation/isf/isf\_config/python/isf\_configconfigdb.py\)
web page.
2. `simulation_release` The release of the simulation that was used for the sample.
The simulation release is normally the athena version,
though sometimes more information is given here.
3. `particle_content` The particle in the sample.
The particle content should be a plural particle name
or something else from `particles.valid_particle_content`, or a pid.
4. `energy` The energy of the sample.
5. `eta_range` The eta range is a tuple of 2 or 4 ints,
or a string representing eta range, see documentation for `dsid.eta_range_to_string`.
6. `zv` The z vertex offset of the sample

Then there are some key locations on the disk associated with this sample;
1. `base_dir` The directory inside which data should be stored in a predetermined structure.
2. `params_location` the path to a parameter file matching this sample
3. `EVNT_location` the path to an EVNT file matching this sample
4. `xAOD_location` the path to an xAOD file matching this sample
5. `flatTTree_location` the path to a flatTTree matching this sample
6. `plots_folder` the path to a folder containing plots for this sample



## Example

Use case; you think that a text file or root file contains metadata
in the format created in this module, you want to read it so that you
can use the values in python.


```python
smetadata_path = "/path/to/file/with/metadata.root"
smetadata = SampleMetadata.read_root_file(smetadata_path)
print(smetadata["particle_content"])
```


or for a text file


```python
smetadata_path = "/path/to/file/with/metadata.txt"
smetadata = SampleMetadata.read_text_file(smetadata_path)
print(smetadata["particle_content"])
```



## Example

Use case; you want to create a record of the sample's metadata
and write it to a text file, or on a canvas in a root file \(which might contain
some data from the file itself\).

A sample's metadata can we created and written to a root file,
writing to a text file overwrites any existing text.


```python
smetadata = SampleMetadata(simulation_type='ATLFAST3',
                           simulation_release='22.0.62',
                           particle_content='photons',
                           energy=65536,
                           eta_range=(-25, -20, 20, 25))
smetadata.write_text_file("/path/to/write/metadata.txt")
```


Alternatively, the sample's metadata can be written to a root file.
This can create a root file if one is not already present,
otherwise the data will be added to the existing root file.
If there is an existing root file and it already has a
canvas at top level named `MetaDataTextCanvas`, it will be
overwritten with the new metadata.


```python
smetadata.write_root_file("/path/to/write/metadata.root")
```


When metadata has been written to a root file, it is
possible to view it \(for example in a `TBrowser`\) on the canvas
named `MetaDataTextCanvas`.


## Example

Use case; you have a script to start a program and the user must
start the script with enough information to locate a sample.

`MetaDataArgParser` is a subclass of `argparse.ArgumentParser`
which comes preloaded with all the arguments that define a sample.
It can be used just like an `ArgumentParser` object, so any
additional arguments needed can be added.

Depending on the input arguments the amount of information
that is required \(not requested, all sample flags are always
valid flags\) can be adjusted.
Here is an example of an argument parser that does not require
all information but requires enough information to locate the
xAOD file;


```python
from hutils.metadata import MetaDataArgParser
# make an argument parser that obtains details of a single sample
# takes all arguments that ArgumentParser takes, plus 4 specilised arguments.
parser = MetaDataArgParser(description="Description to appear in help text.",
                           require_complete_metadata=False,
                           require_locations=['xAOD'],
                           multisample=False,
                           strict=False)
parser.add_argument('-v', '--verbose', action='store_true',
                    help="This is an additional item of information "
                    "needed from the user")
namespace = parser.parse_args()
if namespace.verbose:
    print("This is verbose")
smetadata = parser.parse_metadata()
print(f"My sample is {smetadata}")
```


This will require that the user supplies enough flags
to determine the location of an xAOD file for this sample.
The simples option is the `--xAODlocation` flag, but it is also
possible to specify `--simulation_type`, `--energy` etc. instead,
or even a full `--dsid`.
Of all sample parameters, only `zv` has an inbuilt default, which is 0.
If the above lines were saved as `read_sample.py`
an example of calling this would be;


```bash
python3 read_sample.py --simulation_type ATLFAST3 --simulation_release 22.0.62 \
        --energy 65536 --eta_range 20_25 --particle_content photons --verbose
```


Which is equivalent to


```bash
python3 read_sample.py --simulation_type ATLFAST3 --simulation_release 22.0.62 \
        --dsid photons_E65536_eta_20_25_z0 --verbose
```


All of these options are detailed in the help message, which
appears if the user provides the flag `--help`.
Note that the help message of any `ArgumentParser` or subclass is to appear
when the user passes a `--help` flag, then pyroot needs to be prevented
from swallowing the `--help` flag.
By default, when a program imports pyroot, pyroot replaces
any `--help` flag behaviour with it's own behavior.
For an example of how to avoid this issue, look at the
`__root_importer` class in the code of the `hutils/root.py` module.

Optionally, you can allow the user to specify more than one sample.
This can be used for constructing comparisons.


```python
from hutils.metadata import MetaDataArgParser
# make an argument parser that obtains details of one or more samples
parser = MetaDataArgParser(description="Description to appear in help text.",
                           require_complete_metadata=False,
                           require_locations=[],
                           multisample=True,
                           strict=False)
multi_smetadata = parser.parse_metadata()
print(f"Found {len(multi_smetadata.samples)}")
for i, smetadata in enumerate(multi_smetadata.samples):
    print(f"Sample {i} is {smetadata}")
```


The specification of each sample are eliminated by three dots, `...`,
like the notation used to compare branches in git.
If the above lines were saved as `read_multi_samples.py`
an example of calling this would be;


```bash
python3 read_multi_samples.py --simulation_type ATLFAST3...FullG4 --simulation_release 22.0.62 \
        --energy 65536 --eta_range 20_25 --particle_content photons...pions
```


Which would return metadata for two samples,
the first one specifying photons from the simulation ATLFAST3,
the second one specifying pions from the simulation FullG4.
Both of them have energy of 65536, an eta range of \(\-25, \-20, 20, 25\)
\(if only two numbers are given in the eta range then symmetry is assumed\),
and be from a simulation release labeled `22.0.62`.

In short when multiple options, delimited by `...`, are given
for a flag, it is assumed that each option refers to exactly one sample metadata,
when only one option is given for a flag it is assumed that this option applies
to all the sample metadata.

Aside from the special case of only giving one option, all flags must
be provided with the same number of options.


<!-- create_documentation.py END Module metadata -->


<!-- create_documentation.py START Breakpoint disclaimer -->



 -----
 -----

 This should be everything needed for basic use of the package.
 Unless you are looking to develop the package, or debug bad behaviour,
 you probably don't need anything below this point.


<!-- create_documentation.py END Breakpoint disclaimer -->


<!-- create_documentation.py START Docstrings header -->



 ## Doctrings of functions and classes


<!-- create_documentation.py END Docstrings header -->


<!-- create_documentation.py START Class MetaDataArgParser -->

## Class MetaDataArgParser

Argument parser specialised for getting one or more objects
of type SampleMetadata.
As a subclass of `argparse.ArgumentParser` it behaves in every way like a regular
`ArgumentParser`, with some added features.
It comes with all flags which could help to specify SampleMetadata
preset, with relevant help messages.
It can optionally raise a human readable error,
if not enough information to construct a
full SampleMetadata or find a required data file is given.
It can construct the SampleMetadata \(or MultiSampleMetadata\)
with `MetaDataArgParser.parse_metadata`, which behaves
in an equivalent manner to `MetaDataArgParser.parse_args`, or
equally `ArgumentParser.parse_args`.


### Attributes


**prefilled : dict of str**

Primary name of each flag, and their help messages.

**universal\_conditions : dict**

Additional kwargs that should be passed when all the
flags are added with `add_argument`.

**special\_conditions : dict of dict**

Additional kwargs that should be passed when a specified flag
is added with `add_argument`.

**metadata : SampleMetadata or MultiSampleMetadata**

The specified metadata for the samples.
Not filled out until `MultiSampleMetadata.parse_metadata`
has been called.

## Class function MetaDataArgParser.\_\_init\_\_

**\_\_init\_\_\(self, \*args, \*\*kwargs\)**

Initialisation method.


### Parameters


**description : str \(optional\)**

Text to display before the argument help
\(Default value = `""`\)

**strict : bool \(optional\)**

True if attributes are required to conform to strict
validity criteria. For more information about strict validity
for attributes, see the documentation of `hutils/dsid.py`
`ValueError` is raised if any values given are non conforming.
\(Default value = `False`\)

**multisample : bool \(optional\)**

If true, a MultiSampleMetadata is created and any `...` in
the flags are interpreted as the delimitors for values that
vary between the samples.
If `False` a single SampleMetadata object is created, and
`...` is not considered to be a delimitor \(just a strange part
of the sample value\).
\(Default value = `False`\)

**require\_complete\_metadata : bool \(optional\)**

If `True` an error should be raised if the user does not
supply all sufficient information to completely fill the
physical parameters of the SampleMetadata.
This can be by filling all flags that are not "location"s
or by providing at least one "location" that contains
a record of the complete metadata for the sample.
\(Default value = `False`\)

**require\_locations : list of str \(optional\)**

This list may contain the string representing any of the
5 data types; `params`, `EVNT`, `xAOD`, `flatTTree` and/or
`plots`. If the user does not supply a value for the corrisplonding
location `parse_metadata` will attempt to construct it
using information in other flags/found in other specified files.
If it is not possible to get a location for this, an error will
be raised.
\(Default value = `[]`\)
    

## Class function MetaDataArgParser.add\_argument

**add\_argument\(self, \*args, \*\*kwargs\)**

add\_argument\(dest, ..., name=value, ...\)
add\_argument\(option\_string, option\_string, ..., name=value, ...\)

## Class function MetaDataArgParser.add\_argument\_group

**add\_argument\_group\(self, \*args, \*\*kwargs\)**



## Class function MetaDataArgParser.add\_mutually\_exclusive\_group

**add\_mutually\_exclusive\_group\(self, \*\*kwargs\)**



## Class function MetaDataArgParser.add\_subparsers

**add\_subparsers\(self, \*\*kwargs\)**



## Class function MetaDataArgParser.convert\_arg\_line\_to\_args

**convert\_arg\_line\_to\_args\(self, arg\_line\)**



## Class function MetaDataArgParser.error

**error\(self, message\)**

error\(message: string\)

Prints a usage message incorporating the message to stderr and
exits.

If you override this in a subclass, it should not return \-\- it
should either exit or raise an exception.

## Class function MetaDataArgParser.exit

**exit\(self, status=0, message=None\)**



## Class function MetaDataArgParser.format\_help

**format\_help\(self\)**



## Class function MetaDataArgParser.format\_usage

**format\_usage\(self\)**



## Class function MetaDataArgParser.get\_default

**get\_default\(self, dest\)**



## Class function MetaDataArgParser.get\_full\_keys

**get\_full\_keys\(self\)**

Get a list of keyword arguments required for the SampleMetadata
to be considered complete. This list is also necessary and sufficient
for procedurally deciding on the locations for files.


### Returns


**full\_keys : set of str**

List of keys required to consider a SampleMetadata complete.
    

## Class function MetaDataArgParser.parse\_args

**parse\_args\(self, args=None, namespace=None\)**



## Class function MetaDataArgParser.parse\_intermixed\_args

**parse\_intermixed\_args\(self, args=None, namespace=None\)**



## Class function MetaDataArgParser.parse\_known\_args

**parse\_known\_args\(self, args=None, namespace=None\)**



## Class function MetaDataArgParser.parse\_known\_intermixed\_args

**parse\_known\_intermixed\_args\(self, args=None, namespace=None\)**



## Class function MetaDataArgParser.parse\_metadata

**parse\_metadata\(self, args=None, namespace=None\)**

Generate the metadata described by the users flag choices.
Mimics the `ArgumentParser.parse_args` function.
Fills in the attribute of this object `MetaDataArgParser.metadata`
and returns it.


### Parameters


**args : list of str \(optional\)**

The arguments as a list of strings, as they would be presented on
the command line.
If neither this nor the namespace object are supplied,
the command line arguments will be read to obtain the needed
information.

**namespace : argparse.Namespace**

An object that has been returned after calling `parse_args`
on this object, it has attributes corresponding to
the values of the flags set by the user.
If neither this nor the args object are supplied,
the command line arguments will be read to obtain the needed
information.


### Returns


**metadata : SampleMetadata or MultiSampleMetadata**

Class depends on the value given to `multisample`
at initialisation. It is the metadata described by the
flags the user has set, combined with information
harvested from any files pointed to by "location" flags.

## Class function MetaDataArgParser.print\_help

**print\_help\(self, file=None\)**



## Class function MetaDataArgParser.print\_usage

**print\_usage\(self, file=None\)**



## Class function MetaDataArgParser.register

**register\(self, registry\_name, value, object\)**



## Class function MetaDataArgParser.set\_defaults

**set\_defaults\(self, \*\*kwargs\)**


<!-- create_documentation.py END Class MetaDataArgParser -->


<!-- create_documentation.py START Class MultiSampleMetadata -->

## Class MultiSampleMetadata

Represents the metadata needed to specify the input used to produce a
one or more samples of events. Has almost all of the behavior of a standard python
dictionary with respect to the `SampleMetadata.valid_kw_args`.
Inherits from `SampleMetadata`, and also contains one or more
`SampleMetadata` object representing each sample in the set.

Works on the principle that options that vary are written in a string
representation, and each item is delimited by `...`.
For example, if the multisample called `multi_smetadata` contained samples with energies
10, 65536 and 10000, the value of `multi_smetadata['energy']` would be
`10...65536...10000`.
If all the samples in `multi_smetadata` where for photons,
then the value of `multi_smetadata['particle_content']` would simply
be `photons` because single value are considered general to all the samples.

This notation is inspired by the notation git uses to compare branches in
the log. As it introduces no spaces, or other special characters,
the values can still be used as part of a valid directory or file path.


### Attributes


**num\_samples : int**

Number of distinct SampleMetadata objects that this MultiSampleMetadata
object comprises of.

**samples : tuple of SampleMetadata**

Each of the samples referred to in this MultiSampleMetadata,
constructed as a single SampleMetadata object.

**uniform\_attributes : set**

Names of keyword attributes, that are common to
all sample metadata in the set.

**varying\_attributes : set**

Names of keyword attributes, that vary between sample metadata
in the set.

**valid\_kw\_args : list of str**

List of attributes that are valid keyword arguments for
constructing this object.
Items on this list that end in "location" or "folder"
are file locations themselves, all other items
are used to procedurally determine file locations.

**strict : bool**

True if attributes are required to conform to strict
validity criteria. For more information about strict validity
for attributes, see the documentation of `hutils/dsid.py`
If this attribute is set to `True` after the SampleMetadata object
has been created, then all of it's existing attributes will be
checked, and `ValueError` is raised if they are non conforming.

**DSID : str**

Varient of data sample id for this sample.
The form of a DSID takes the basic template;
`<particles>_E<energy>_eta_<eta range>_z<zv>`,
for more information, see the documentation of `hutils/dsid.py`.
This DSID may contain at least one `...` deliminator,
indicating the parts of the sample that vary.

## Class function MultiSampleMetadata.\_\_init\_\_

**\_\_init\_\_\(self, \*args, \*\*kwargs\)**

Initialisation method for SampleMetadata.


### Parameters


**notes : str or list of str \(optional\)**

Any other information about the sample.
Not used to determine the location of data files for
the sample.
Only for records.

**strict : bool \(optional\)**

True if attributes are required to conform to strict
validity criteria. For more information about strict validity
for attributes, see the documentation of `hutils/dsid.py`
If this attribute is set to `True` all arguments are
checked, and `ValueError` is raised if they are non conforming.
\(Default value = True\)

**simulation\_type : str \(optional\)**

Name of the simulation used for the sample.
The simulation type should be one of the entries on
this \(https://gitlab.cern.ch/atlas/athena/\-/blob/master/simulation/isf/isf\_config/python/isf\_configconfigdb.py\)
web page.

**simulation\_release : str \(optional\)**

The release of the simulation that was used for the sample.
The simulation release is normally the athena version,
though sometimes more information is given here.

**particle\_content : str or int \(optional\)**

If int, will be interpreted as a pid.
If it is a string that starts with "pid"
the first 3 characters will be clipped off,
and the remainder of the string treated as an int.
If strict, must be found in `particles.valid_particle_content`
or in `particles.known_pids`.
If not strict, can be any string that doesn't include the
substring `_E` or `.E` or any int.

**energy : int or float \(optional\)**

If strict, will be interpreted as a single number, it must be composed
of only numbers, `.` or `p` \(where `p` stands for `.`\),
excluding the first character, which can be `m` and `-` to make
a negative energy.
If not strict, can be any string that doesn't include
the substring `_eta` or `.eta`.

**eta\_range : tuple or str \(optional\)**

If str, must be a sequence of 2 or 4 integers, separated by `_`
as with energy, the first character can be `-` or `m` to create a negative
number, however decimal points are not possible.
Will be converted to a tuple by `eta_range_to_tuple`.
If not strict, can be any string that doesn't include
the substring `_z` or `.z`.
Will try to convert to a tuple by `eta_range_to_tuple`.
If tuple, must be 2 or 4 ints.

**zv : int or float \(optional\)**

If strict, can be anything that can be converted to an int or float,
any occurrences or `p` can be interpreted as `.` to form a float.
If not strict, is unrestricted.
    

## Class function MultiSampleMetadata.add\_sample

**add\_sample\(self, metadata\)**

Add a whole sample or set of samples to this set.
If the sample is not fully specified, any unspecified
keyword arguments that are uniform for this MultiSampleMetadata
are added to the sample.


### Parameters


**metadata : SampleMetadata or MultiSampleMetadata**

One or more sample metadata to be added to this set

## Class function MultiSampleMetadata.get

**get\(self, key, default=<object object at 0x7fcaa6767690>\)**

Return the value of any item from `SampleMetadata.valid_kw_args`,
else default if given.


### Parameters


**key : str**

Name of key word argument to fetch

**default : any \(optional\)**

Value to return if key word argument is not set for this
SampleMetadata.


### Raises

KeyError
If no default is given, and key is not set for this sample
this error will be raised.

## Class function MultiSampleMetadata.harvest\_root\_path

**harvest\_root\_path\(cls, path, strict=True, path\_is\_location=False\)**

Alternative constructor.
Get the most info possible from a path for a root file.
Constructs an object of type MultiSampleMetadata, preferentially using
information written to the root file itself \(if the root file exists\)
using `MultiSampleMetadata.read_root_file`,
but also deducing information from the structure of the path of the
root file.
Fault tolerant, will almost always return some MultiSampleMetadata rather than
raise an error, but the MultiSampleMetadata may have many or all keyword values
unset if their values cannot be determined automatically.


### Parameters


**path : str**

Path that may point to a root file to read from,
and may contain information about the sample characteristics
in and of itself.

**strict : bool \(optional\)**

True if values in the file and path should be parsed with
strict settings. See the main `SampleMetadata.__init__`
method for the impact of strict settings on reading data from the
root file, and the documentation of `hutils/files.py` for information
about strict settings for interpreting a path.

**path\_is\_location : False or str**

If this is a strign it indicates that the given file path
corrisponds to the location of one format of this dataset.
Valid formats are "EVNT", "xAOD", "flatTTree" or "images".


### Returns


**found : MultiSampleMetadata**

A multiple sample metadata object containing information gathered from
reading the root file, if it exists, and interpreting the file path.
Information read from the root file takes precedence.

## Class function MultiSampleMetadata.keys

**keys\(self\)**

List keys in the keyword arguments whose values have been specified.


### Returns


**keys : list of str**

List keys whose values have been specified,
in a fixed order.
    

## Class function MultiSampleMetadata.process\_item

**process\_item\(cls, key, item\)**

Try to convert an item from `SampleMetadata.valid_kw_args`
to standard form, as defined by strict settings.
All "location" keys are converted to full paths.
Note if the conversion is successful.


### Parameters


**key : str**

Attribute name from `SampleMetadata.valid_kw_args`

**item : various**

Item to be processed, see the documentation for
`SampleMetadata.__init__` to learn about target
formats and values.


### Returns


**item : various**

item that has been converted to strict form where possible.

**valid : bool**

True if the conversion was successful

**error\_message : str**

Error message, which describes the problem if
the conversion did not succeed.

## Class function MultiSampleMetadata.process\_particle

**process\_particle\(particle\)**

Try to convert a particle to standard form; plural string.
Note if the conversion is successful.


### Parameters


**particle : str or int**

If int, will be interpreted as a pid.
Can be any string that doesn't include the
substring `_E` or `.E`, or any int.
If it is a string that starts with "pid"
the first 3 characters will be clipped off,
and the remainder of the string treated as an int.
Will be declared valid if be found in
`particles.valid_particle_content` or in `particles.known_pids`.


### Returns


**particle : str or int**

particle, converted to one of the entries in
`particles.valid_particle_content` if possible

**valid\_particle : bool**

True if the conversion was successful

**error\_message : str**

Error message, which describes the problem if
the conversion did not succeed.

## Class function MultiSampleMetadata.read\_root\_file

**read\_root\_file\(cls, path, strict=True, path\_is\_location=False\)**

Look in the specified root file for a canvas named "MetaDataTextCanvas"
at top level and read the text on it to create a MultiSampleMetadata
object that contains a single sample.


### Parameters


**filepath : str**

Path to the root file to read from.

**strict : bool \(optional\)**

True if values in the file should be parsed with
strict settings. See the main `SampleMetadata.__init__`
method for more information.

**path\_is\_location : False or str**

If this is a string it indicates that the given file path
corrisponds to the location of one format of this dataset.
Valid formats are "EVNT", "xAOD", "flatTTree" or "images".


### Returns


**new\_metadata : MultiSampleMetadata**

A MultiSampleMetadata containing just one sample which is
the clone of the SampleMetadata object that wrote the text on the
canvas, with the exception of any changes forced by the
strict criteria and possibly the date.

## Class function MultiSampleMetadata.read\_string

**read\_string\(cls, string, strict=True\)**

Alternative constructor.
Constructs an object of type SampleMetadata from the string
representation of an object of type SampleMetadata.


### Parameters


**string : str**

String created by converting object of type
SampleMetadata to string.

**strict : bool \(optional\)**

True if values in the string should be parsed with
strict settings. See the main `SampleMetadata.__init__`
method for more information.


### Returns


**new\_metadata : SampleMetadata**

A clone of the SampleMetadata object that created the
input string, wither the exception of any changes forced by the
strict criteria and possibly the date.

## Class function MultiSampleMetadata.read\_text\_file

**read\_text\_file\(cls, path, strict=True\)**

Alternative constructor.
Constructs an object of type MultiSampleMetadata from a text file.


### Parameters


**filepath : str**

Path to a text file containing a string created by
converting object of type MultiSampleMetadata or
SampleMetadata to string.

**strict : bool \(optional\)**

True if values in the file should be parsed with
strict settings. See the main `SampleMetadata.__init__`
method for more information.


### Returns


**new\_metadata : MultiSampleMetadata**

A clone of the MultiSampleMetadata object that created the
input file, with the exception of any changes forced by the
strict criteria and possibly the date.

## Class function MultiSampleMetadata.set\_for\_all\_samples

**set\_for\_all\_samples\(self, key, item\)**

For all the sample metadata variants in this MultiSampleMetadata,
set the value of the keyword variable to the specified value.


### Parameters


**key : str**

name of the keyword attribute from `SampleMetadata.valid_kw_args`
to be set to a uniform value.

**item : various**

common value to set

## Class function MultiSampleMetadata.set\_for\_each\_sample

**set\_for\_each\_sample\(self, key, parts\)**

For each of the sample metadata variants in the MultiSampleMetadata
set the value of the keyword variable to a different value.


### Parameters


**key : str**

name of the keyword attribute from `SampleMetadata.valid_kw_args`
to be set to varying values.

**parts : iterable**

One value for each sample metadata in the set.


### Raises

ValueError
Raised if the number of samples in this set
does not match the length of the parts iterable given.

## Class function MultiSampleMetadata.set\_location

**set\_location\(self, for\_format, path\)**

Given a path and which format it corrisponds to,
set the metadatas record for that format.


### Parameters


**for\_format : str**

String indicating the format of the data that
this path points to. Must be one of
"EVNT", "xAOD", "flatTTree" or "images"

**path : str**

Path to file on disk. Not required to actually exist.


### Raises

ValueError
If the for\_format value is not a known format.

## Class function MultiSampleMetadata.set\_without\_check

**set\_without\_check\(self, \*\*kwargs\)**

Set values into the dictionary of keyword arguments
without any checks. Ignores whether the items are in
`SampleMetadata.valid_kw_args` does not perform conversions,
and ignores the strict criteria if set.
Using this to add items not specified in `SampleMetadata.valid_kw_args`
is very likely to break functions of this class.
To be used with caution.


### Parameters


**to\_add : various \(optional\)**

Any key and value to be added to the dictionary of
keyword arguments. May specify multiple at once.
Must be given as a `key=value` argument.

## Class function MultiSampleMetadata.write\_known\_roots

**write\_known\_roots\(self, overwrite=False\)**

For all root files known to contain data from this sample,
because they are referred to by a "location" keyword,
write the SampleMetadata on the root file


### Parameters


**overwrite : bool \(optional\)**

If there is a root file already at any path,
and the root file already has a "MetaDataTextCanvas",
should it be overwritten?
\(Default value = `False`\)


### Raises

AssertionError
If overwrite is set to `False` and a root file already
exists at the given filepath and that root file already has
a "MetaDataTextCanvas" at the top level.

## Class function MultiSampleMetadata.write\_root\_file

**write\_root\_file\(self, filepath, overwrite=True\)**

Add the SampleMetadata to the specified root file in
human \(and TBrowser\) readable format, on a canvas called
"MetaDataTextCanvas". If the root file does not exist,
create it.


### Parameters


**filepath : str**

Path for the root file to write to.

**overwrite : bool \(optional\)**

If there is a root file already at the path,
and the root file already has a "MetaDataTextCanvas",
should it be overwritten?
\(Default value = `False`\)


### Raises

AssertionError
If overwrite is set to `False` and a root file already
exists at the given filepath and that root file already has
a "MetaDataTextCanvas" at the top level.

## Class function MultiSampleMetadata.write\_text\_file

**write\_text\_file\(self, filepath, overwrite=True\)**

Create a human readable text file from this SampleMetadata object.
Contains all information except for the value of the strict criteria.


### Parameters


**filepath : str**

Path at which to write the text file

**overwrite : bool \(optional\)**

If `True`, any existing file at the given file path will
be overwritten.
\(Default value = `True`\)


### Raises

AssertionError
If overwrite is set to `False` and a file already exists
at the given filepath.
<!-- create_documentation.py END Class MultiSampleMetadata -->


<!-- create_documentation.py START Class SampleMetadata -->

## Class SampleMetadata

Represents the metadata needed to specify the input used to produce a
single sample of events. Has almost all of the behavior of a standard python
dictionary with respect to the `SampleMetadata.valid_kw_args`.
In particular, if another function, say `my_metadata_process` needs the values of
the keyword arguments as input, and you have a SampleMetadata object
called `smetadata` with the correct values set it is valid to
do `my_metadata_process(**smetadata)`.


### Attributes


**valid\_kw\_args : list of str**

List of attributes that are valid keyword arguments for
constructing this object.
Items on this list that end in "location" or "folder"
are file locations themselves, all other items
are used to procedurally determine file locations.

**strict : bool**

True if attributes are required to conform to strict
validity criteria. For more information about strict validity
for attributes, see the documentation of `hutils/dsid.py`
If this attribute is set to `True` after the SampleMetadata object
has been created, then all of it's existing attributes will be
checked, and `ValueError` is raised if they are non conforming.

**DSID : str**

The data sample id for this sample.
The form of a DSID takes the basic template;
`<particles>_E<energy>_eta_<eta range>_z<zv>`,
for more information, see the documentation of `hutils/dsid.py`.

**notes : list of str**

Any other information about the sample.
Not used to determine the location of data files for
the sample.
Only for records.

## Class function SampleMetadata.\_\_init\_\_

**\_\_init\_\_\(self, notes=None, strict=True, \*\*kw\_args\)**

Initialisation method for SampleMetadata.


### Parameters


**notes : str or list of str \(optional\)**

Any other information about the sample.
Not used to determine the location of data files for
the sample.
Only for records.

**strict : bool \(optional\)**

True if attributes are required to conform to strict
validity criteria. For more information about strict validity
for attributes, see the documentation of `hutils/dsid.py`
If this attribute is set to `True` all arguments are
checked, and `ValueError` is raised if they are non conforming.
\(Default value = True\)

**simulation\_type : str \(optional\)**

Name of the simulation used for the sample.
The simulation type should be one of the entries on
this \(https://gitlab.cern.ch/atlas/athena/\-/blob/master/simulation/isf/isf\_config/python/isf\_configconfigdb.py\)
web page.

**simulation\_release : str \(optional\)**

The release of the simulation that was used for the sample.
The simulation release is normally the athena version,
though sometimes more information is given here.

**particle\_content : str or int \(optional\)**

If int, will be interpreted as a pid.
If it is a string that starts with "pid"
the first 3 characters will be clipped off,
and the remainder of the string treated as an int.
If strict, must be found in `particles.valid_particle_content`
or in `particles.known_pids`.
If not strict, can be any string that doesn't include the
substring `_E` or `.E` or any int.

**energy : int or float \(optional\)**

If strict, will be interpreted as a single number, it must be composed
of only numbers, `.` or `p` \(where `p` stands for `.`\),
excluding the first character, which can be `m` and `-` to make
a negative energy.
If not strict, can be any string that doesn't include
the substring `_eta` or `.eta`.

**eta\_range : tuple or str \(optional\)**

If str, must be a sequence of 2 or 4 integers, separated by `_`
as with energy, the first character can be `-` or `m` to create a negative
number, however decimal points are not possible.
Will be converted to a tuple by `eta_range_to_tuple`.
If not strict, can be any string that doesn't include
the substring `_z` or `.z`.
Will try to convert to a tuple by `eta_range_to_tuple`.
If tuple, must be 2 or 4 ints.

**zv : int or float \(optional\)**

If strict, can be anything that can be converted to an int or float,
any occurrences or `p` can be interpreted as `.` to form a float.
If not strict, is unrestricted.
    

## Class function SampleMetadata.get

**get\(self, key, default=<object object at 0x7fcaa6767690>\)**

Return the value of any item from `SampleMetadata.valid_kw_args`,
else default if given.


### Parameters


**key : str**

Name of key word argument to fetch

**default : any \(optional\)**

Value to return if key word argument is not set for this
SampleMetadata.


### Raises

KeyError
If no default is given, and key is not set for this sample
this error will be raised.

## Class function SampleMetadata.harvest\_root\_path

**harvest\_root\_path\(cls, path, strict=True, path\_is\_location=False\)**

Alternative constructor.
Get the most info possible from a path for a root file.
Constructs an object of type SampleMetadata, preferentially using
information written to the root file itself \(if the root file exists\)
using `SampleMetadata.read_root_file`,
but also deducing information from the structure of the path of the
root file.
Fault tolerant, will almost always return some SampleMetadata rather than
raise an error, but the SampleMetadata may have many or all keyword values
unset if their values cannot be determine automatically.


### Parameters


**path : str**

Path that may point to a root file to read from,
and may contain information about the sample characteristics
in and of itself.

**strict : bool \(optional\)**

True if values in the file and path should be parsed with
strict settings. See the main `SampleMetadata.__init__`
method for the impact of strict settings on reading data from the
root file, and the documentation of `hutils/files.py` for information
about strict settings for interpreting a path.

**path\_is\_location : False or str**

If this is a string it indicates that the given file path
corrisponds to the location of one format of this dataset.
Valid formats are "EVNT", "xAOD", "flatTTree" or "images".


### Returns


**found : SampleMetadata**

A sample metadata object containing information gathered from
reading the root file, if it exists, and interpreting the file path.
Information read from the root file takes precedence.

## Class function SampleMetadata.keys

**keys\(self\)**

List keys in the keyword arguments whose values have been specified.


### Returns


**keys : list of str**

List keys whose values have been specified,
in a fixed order.
    

## Class function SampleMetadata.process\_item

**process\_item\(cls, key, item\)**

Try to convert an item from `SampleMetadata.valid_kw_args`
to standard form, as defined by strict settings.
All "location" keys are converted to full paths.
Note if the conversion is successful.


### Parameters


**key : str**

Attribute name from `SampleMetadata.valid_kw_args`

**item : various**

Item to be processed, see the documentation for
`SampleMetadata.__init__` to learn about target
formats and values.


### Returns


**item : various**

item that has been converted to strict form where possible.

**valid : bool**

True if the conversion was successful

**error\_message : str**

Error message, which describes the problem if
the conversion did not succeed.

## Class function SampleMetadata.process\_particle

**process\_particle\(particle\)**

Try to convert a particle to standard form; plural string.
Note if the conversion is successful.


### Parameters


**particle : str or int**

If int, will be interpreted as a pid.
Can be any string that doesn't include the
substring `_E` or `.E`, or any int.
If it is a string that starts with "pid"
the first 3 characters will be clipped off,
and the remainder of the string treated as an int.
Will be declared valid if be found in
`particles.valid_particle_content` or in `particles.known_pids`.


### Returns


**particle : str or int**

particle, converted to one of the entries in
`particles.valid_particle_content` if possible

**valid\_particle : bool**

True if the conversion was successful

**error\_message : str**

Error message, which describes the problem if
the conversion did not succeed.

## Class function SampleMetadata.read\_root\_file

**read\_root\_file\(cls, filepath, strict=True, path\_is\_location=False\)**

Look in the specified root file for a canvas named "MetaDataTextCanvas"
at top level and read the text on it to create a SampleMetadata
object.


### Parameters


**filepath : str**

Path to the root file to read from.

**strict : bool \(optional\)**

True if values in the file should be parsed with
strict settings. See the main `SampleMetadata.__init__`
method for more information.

**path\_is\_location : False or str**

If this is a string it indicates that the given file path
corrisponds to the location of one format of this dataset.
Valid formats are "EVNT", "xAOD", "flatTTree" or "images".


### Returns


**new\_metadata : SampleMetadata**

A clone of the SampleMetadata object that wrote the text on the
canvas, with the exception of any changes forced by the
strict criteria and possibly the date.

## Class function SampleMetadata.read\_string

**read\_string\(cls, string, strict=True\)**

Alternative constructor.
Constructs an object of type SampleMetadata from the string
representation of an object of type SampleMetadata.


### Parameters


**string : str**

String created by converting object of type
SampleMetadata to string.

**strict : bool \(optional\)**

True if values in the string should be parsed with
strict settings. See the main `SampleMetadata.__init__`
method for more information.


### Returns


**new\_metadata : SampleMetadata**

A clone of the SampleMetadata object that created the
input string, wither the exception of any changes forced by the
strict criteria and possibly the date.

## Class function SampleMetadata.read\_text\_file

**read\_text\_file\(cls, filepath, strict=True\)**

Alternative constructor.
Constructs an object of type SampleMetadata from a text file,
using the `SampleMetadata.read_string` method.


### Parameters


**filepath : str**

Path to a text file containing a string created by
converting object of type SampleMetadata to string.

**strict : bool \(optional\)**

True if values in the file should be parsed with
strict settings. See the main `SampleMetadata.__init__`
method for more information.


### Returns


**new\_metadata : SampleMetadata**

A clone of the SampleMetadata object that created the
input file, with the exception of any changes forced by the
strict criteria and possibly the date.

## Class function SampleMetadata.set\_location

**set\_location\(self, for\_format, path\)**

Given a path and which format it corrisponds to,
set the metadatas record for that format.


### Parameters


**for\_format : str**

String indicating the format of the data that
this path points to. Must be one of
"EVNT", "xAOD", "flatTTree" or "images"

**path : str**

Path to file on disk. Not required to actually exist.


### Raises

ValueError
If the for\_format value is not a known format.

## Class function SampleMetadata.set\_without\_check

**set\_without\_check\(self, \*\*kwargs\)**

Set values into the dictionary of keyword arguments
without any checks. Ignores whether the items are in
`SampleMetadata.valid_kw_args` does not perform conversions,
and ignores the strict criteria if set.
Using this to add items not specified in `SampleMetadata.valid_kw_args`
is very likely to break functions of this class.
To be used with caution.


### Parameters


**to\_add : various \(optional\)**

Any key and value to be added to the dictionary of
keyword arguments. May specify multiple at once.
Must be given as a `key=value` argument.

## Class function SampleMetadata.write\_known\_roots

**write\_known\_roots\(self, overwrite=False\)**

For all root files known to contain data from this sample,
because they are referred to by a "location" keyword,
write the SampleMetadata on the root file


### Parameters


**overwrite : bool \(optional\)**

If there is a root file already at any path,
and the root file already has a "MetaDataTextCanvas",
should it be overwritten?
\(Default value = `False`\)


### Raises

AssertionError
If overwrite is set to `False` and a root file already
exists at the given filepath and that root file already has
a "MetaDataTextCanvas" at the top level.

## Class function SampleMetadata.write\_root\_file

**write\_root\_file\(self, filepath, overwrite=True\)**

Add the SampleMetadata to the specified root file in
human \(and TBrowser\) readable format, on a canvas called
"MetaDataTextCanvas". If the root file does not exist,
create it.


### Parameters


**filepath : str**

Path for the root file to write to.

**overwrite : bool \(optional\)**

If there is a root file already at the path,
and the root file already has a "MetaDataTextCanvas",
should it be overwritten?
\(Default value = `False`\)


### Raises

AssertionError
If overwrite is set to `False` and a root file already
exists at the given filepath and that root file already has
a "MetaDataTextCanvas" at the top level.

## Class function SampleMetadata.write\_text\_file

**write\_text\_file\(self, filepath, overwrite=True\)**

Create a human readable text file from this SampleMetadata object.
Contains all information except for the value of the strict criteria.


### Parameters


**filepath : str**

Path at which to write the text file

**overwrite : bool \(optional\)**

If `True`, any existing file at the given file path will
be overwritten.
\(Default value = `True`\)


### Raises

AssertionError
If overwrite is set to `False` and a file already exists
at the given filepath.
<!-- create_documentation.py END Class SampleMetadata -->


<!-- create_documentation.py START Function copy\_script\_location -->

## Function copy\_script\_location

**copy\_script\_location\(\)**

Returns the location of the copy\_metadata script on the disk.
Useful if you need to copy metadata from one place to another in
a bash script. See the copy\_metadata documentation.


### Returns


**copy\_script\_location : str**

Full absolute path to the copy\_metadata script.
<!-- create_documentation.py END Function copy\_script\_location -->


<!-- create_documentation.py START Object absolute\_import -->

## Object absolute\_import

**_Feature**

\_Feature\(\(2, 5, 0, 'alpha', 1\), \(3, 0, 0, 'alpha', 0\), 262144\)
<!-- create_documentation.py END Object absolute\_import -->


<!-- create_documentation.py START Function add\_dsid\_to\_metadata -->

## Function add\_dsid\_to\_metadata

**add\_dsid\_to\_metadata\(dsid, metadata\)**


<!-- create_documentation.py END Function add\_dsid\_to\_metadata -->


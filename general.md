
<!-- create_documentation.py START Module general -->

# Module general

*Documentation for the code at `general.py`*

General utilities.
Tools that perform very generic functions.
Many of these essentially just call one function in one library,
but have been moved to this module to allow for the possibility of
changes in the underlying operating system or libraries.

A few of them provide useful functions not avaliable by default
in any libraries in the default Athena setup.


## Example


Use case; you have a string that contains at least one of a
list of targets, you want to know the index of the first of
any of the targets.


```python
targets = ["dog", "cat"]
my_str = "See dog then cat"
i, found = multi_index(my_str, targets)
assert i == 5
assert found == "dog"
```



## Example


Use case; you need a folder to exist, and you
want to make it recursivly if it doesn't.


```python
folder_needed = "/path/to/folder/"
ensure_folder_exists(folder_needed)
```



## Example


Use case; you need today's date as a regular string.


```python
get_date()
```



## Example


Use case; you need a new directory, that is chosen not to clash
with any current directory, created in a thread safe manner.


```python
make_new_unique_directory(name_base="my_new_dir_")
```



## Example


Use case; you need to move all files in one directory to anouther
directory. The target directory does not have to already exist.


```python
move_files("/path/to/source/dir/", "/path/to/target/")
```


You may optionally specify the start of the name of the files to move


```python
move_files("/path/to/source/dir/filebase_", "/path/to/target/")
```



## Example


Use case; you need to get the name of the home
directory, and you would like it to work even if `$HOME`
doesn't exist \(say in a condor job\).


```python
home_dir = get_home_dir()
```



<!-- create_documentation.py END Module general -->


<!-- create_documentation.py START Breakpoint disclaimer -->



 -----
 -----

 This should be everything needed for basic use of the package.
 Unless you are looking to develop the package, or debug bad behaviour,
 you probably don't need anything below this point.


<!-- create_documentation.py END Breakpoint disclaimer -->


<!-- create_documentation.py START Docstrings header -->



 ## Doctrings of functions and classes


<!-- create_documentation.py END Docstrings header -->


<!-- create_documentation.py START Function ensure\_folder\_exists -->

## Function ensure\_folder\_exists

**ensure\_folder\_exists\(folderName\)**

Make the folder if it doesn't currently exist


### Parameters


**folderName : str**

full path to the needed folder
<!-- create_documentation.py END Function ensure\_folder\_exists -->


<!-- create_documentation.py START Function get\_date -->

## Function get\_date

**get\_date\(\)**

Give todays date


### Returns


**today : str**

today's date as a str in form ddmmyyyy
<!-- create_documentation.py END Function get\_date -->


<!-- create_documentation.py START Function get\_home\_dir -->

## Function get\_home\_dir

**get\_home\_dir\(\)**

Guess the path to the home \(`~`\) directory.
Useful inside condor scripts where $HOME dosn't exist


### Returns


**home : str or None**

the full path to the users home directory
or None if not enough information to find this
exists.
<!-- create_documentation.py END Function get\_home\_dir -->


<!-- create_documentation.py START Function make\_new\_unique\_directory -->

## Function make\_new\_unique\_directory

**make\_new\_unique\_directory\(name\_base='./temp\_'\)**

Make a new directory that is garenteed not to clash
with any existing directory. Thread safe in unix,
even if multiple threads call this function at the same
time, it uses the unitary directory construction operator such
that they will never both return the same directory.


### Parameters


**name\_base : str \(optional\)**

The start of the directories path, can include the first
part of the innermost directory name.
An integer will be appended to this name to ensure
a unique directory may be constructed.
\(Default value = "./temp\_"\)


### Returns


**: str**

The name of the new unique directory.
<!-- create_documentation.py END Function make\_new\_unique\_directory -->


<!-- create_documentation.py START Function move\_files -->

## Function move\_files

**move\_files\(current\_folder\_namestart, destination\)**

Recursively move all files from one location to anouther


### Parameters


**current\_folder\_namestart : str**

Start of all the paths to move,
can be just a directory, or a directory
and the start of the file names.
    

**destination : str**

Path to move all files to.
<!-- create_documentation.py END Function move\_files -->


<!-- create_documentation.py START Function multi\_index -->

## Function multi\_index

**multi\_index\(string, multi\_targets\)**

Given a string that contains at least one of a list of targets,
find the target that appears first, and it's starting index.


### Parameters


**string : str**

The string that contains the targets

**multi\_targets : iterable of str**

The iterable of targets


### Returns


**i : int**

the index of the first target found

**target : str**

the value of the first target found
<!-- create_documentation.py END Function multi\_index -->


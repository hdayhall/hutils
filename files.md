
<!-- create_documentation.py START Module files -->

# Module files

*Documentation for the code at `files.py`*

Manipulations for file paths.

Within the context of locating and identifying samples,
this module contains often needed file path manipulations.
It is designed to provide consistent placement of different types of data files,
according to the data type and the sample that the data represents.

When an analysis has multiple steps, one of the major challenges if ensuring that
each step properly locates the data created by the previous step.
Before this code, most steps had their own logic for choosing where to write and read from.
Delegating that to this centralised code has a number of advantages.
Firstly, as each step is using the same logic code to identify the path for a data file,
we have guaranteed that subsequent steps will be able to find a file if it has been written.
Secondly, if we keep the logic for locating files in this common tool,
we remove associated code repetition, which means that any bugs only need to be fixed once.
Thirdly, if in the future we decide to change where data is stored, that
change must only be made here, and not repeated for each processing step.

The expected data types are;

1. `EVNT` files, large files, storing event data before detector simulation.
2. `param` files, files that parameterise the detector simulation.
3. `xAOD` files, moderately large files, that store compile objects representing the event after detector simulation.
4. `flatTTree` files, small TTree files, with one value per event per branch.
5. `images`, histograms of various kinds created from the flatTTrees.


## Example


Use case, you want to find where a data file of a given sample
type should reside, either to write or read it.

First, let us create a set of variable that would describe
the sample it self;


```python
simulation_type = 'ATLFAST3'
simulation_release = '22.0.62'
particle_content = 'photons'
energy = 65536
eta_range = (-25, -20, 20, 25)
```


No fixed for for these variables is enforced,
but if they do take a regular form, sensible conversions
are care needed.
the simulation type should be one of the entries on
\[this\]\(https://gitlab.cern.ch/atlas/athena/\-/blob/master/simulation/isf/isf\_config/python/isf\_configconfigdb.py\)
web page.
The simulation release is normally the athena version, though sometimes
more information is given here.
The particle content should be a plural particle name
or something else from `particles.valid_particle_content`,
the energy is normally an int, the eta range is a
tuple of 2 or 4 ints, or a string representing eta range,
see documentation for `dsid.eta_range_to_string`.

Then we need to know what kind of data file is required,
it could be a `EVNT`, `param`, `xAOD`, `flatTTree` or `images`;


```python
data_type = 'EVNT'
```


Finally, the default base directory \(`base_dir`\) is `eos`,
which puts files under `/eos/atlas/atlascerngroupdisk/proj-simul/`,
but there are other options.
You could specify the full file path manually;


```python
base_dir = '/path/to/the/files/'
```


Or you can ask for the files to be put in `~/public/data/` using;


```python
base_dir = 'local'
```


Now the correct location of the data is returned with;


```python
data_path = locate(data_type, simulation_type,
                   simulation_release, particle_content, energy,
                   eta_range, base_dir=base_dir)
```




## Example


Given a `metadata.SampleMetadata` object, you would like to find the correct
location of a sample of that data type.


```python
import metadata
isinstance(my_sample, metadata.SampleMetadata)
```


A fully specified `metadata.SampleMetadata` holds all needed information
about the sample, and applies equally to all data types.
Because it applies equally to all data types
you must define the data type first.
it could be a `EVNT`, `param`, `xAOD`, `flatTTree` or `images`;


```python
data_type = 'EVNT'
```


besides that information, the metadata object can supply the rest.
`metadata.SampleMetadata` objects have some `dict` like behavior,
specifically, they implement keyword indexing; `my_sample["particle_content"]`,
length checks; `len(my_sample)`, iteration over keys; `iter(my_sample)`
and checking for keys; `"particle_content" in my_sample`.

This is more than enough to expand the dict into an argument list;


```python
data_path = locate(data_type, **metadata)
```


If it turns out there is not enough information in the metadata object,
this will raise a `TypeError`, but the message is less than helpful
in older versions of python.


## Example


Use case, given a file that you believe to contain some data,
you can extract the base directory used for storing data
and also the dsid of the data;


```python
my_data_at = "/some/base/dir/OutputSamples/photons_E65536_eta_m25_m20_20_25_z0/ATLFAST3/22.0.26/"
base_dir = extract_base_dir(my_data_at)
assert base_dir == "/some/base/dir"
my_dsid = extract_DSID(my_data_at, strict=True)
assert my_dsid == "photons_E65536_eta_m25_m20_20_25_z0"
```


If the dsid may contain irregular values, you will need
`strict=False`. Then finally, you may extract the values themselves using
`decode_DSID` from the `dsid` module.


<!-- create_documentation.py END Module files -->


<!-- create_documentation.py START Breakpoint disclaimer -->



 -----
 -----

 This should be everything needed for basic use of the package.
 Unless you are looking to develop the package, or debug bad behaviour,
 you probably don't need anything below this point.


<!-- create_documentation.py END Breakpoint disclaimer -->


<!-- create_documentation.py START Docstrings header -->



 ## Doctrings of functions and classes


<!-- create_documentation.py END Docstrings header -->


<!-- create_documentation.py START Function construct\_inner\_dir -->

## Function construct\_inner\_dir

**construct\_inner\_dir\(simulation\_type, simulation\_release, particle\_content, energy, eta\_range, zv, \*\*excess\_args\)**

For given sample specification, construct a string representing
the third and innermost section of the path.
The outermost section is the `base_dir`, then there
is the data type specific `sub_dir`, then finally the inner
directory specific to the sample type.


### Parameters


**simulation\_type : str**

Name of the simulation used for the sample.
The simulation type should be one of the entries on

**\[this\]\(https://gitlab.cern.ch/atlas/athena/\-/blob/master/simulation/isf/isf\_config/python/isf\_configconfigdb.py\)**

web page.

**simulation\_release : str**

The release of the simulation that was used for the sample.
The simulation release is normally the athena version,
though sometimes more information is given here.

**particle\_content : str or int**

The particle in the sample.
The particle content should be a plural particle name
or something else from `particles.valid_particle_content`,
or a pid.

**energy : int or float or str**

The energy of the sample.

**eta\_range : tuple or str**

The eta range is a tuple of 2 or 4 ints, or a string representing eta range,
see documentation for `dsid.eta_range_to_string`.

**zv : int or str**

The z vertex offset of the sample

**\*\*excess\_args : anything**

Swallows excessive arguments silently.
This allows dicts that contain at least the required arguments
to be used to construct an argument list.


### Returns


**inner\_dir : str**

inner most layers of the directory path for the sample.
This is independent of the data type.
    
<!-- create_documentation.py END Function construct\_inner\_dir -->


<!-- create_documentation.py START Function describe\_location -->

## Function describe\_location

**describe\_location\(data\_type=None, simulation\_type=None, simulation\_release=None, particle\_content=None, energy=None, eta\_range=None, zv=0, base\_dir=None, \*\*excess\_args\)**

Documentaion generation function, for a given set of restrictions
print and return a string that describes the way the locaton
for samples with those charicteristics would be found.
Any parameters not given are treated as variables.


### Parameters


**data\_type : str \(optional\)**

Specfiy the kind of data file.
It could be a `EVNT`, `param`, `xAOD`, `flatTTree` or `images`.

**simulation\_type : str \(optional\)**

Specfiy the simulation used.
The simulation type should be one of the entries on

**\[this\]\(https://gitlab.cern.ch/atlas/athena/\-/blob/master/simulation/isf/isf\_config/python/isf\_configconfigdb.py\)**

web page.

**simulation\_release : str \(optional\)**

The release of the simulation that is of intrest.
The simulation release is normally the athena version,
though sometimes more information is given here.

**particle\_content : str or int \(optional\)**

The particle in the sample.
The particle content should be a plural particle name
or something else from `particles.valid_particle_content`,
or a pid.

**energy : int or float or str \(optional\)**

The energy of the sample.

**eta\_range : tuple or str \(optional\)**

the eta range is a tuple of 2 or 4 ints, or a string representing eta range,
see documentation for `dsid.eta_range_to_string`.

**zv : int or str \(optional\)**

The z vertex offset of the sample
\(Default value = 0\)

**\*\*excess\_args : anything**

Swallows excessive arguments silently.
This allows dicts that contain at least the required arguments
to be used to construct an argument list.


### Returns


**message : str**

A description of all of the possibilities that are consistent with
this restriction.
<!-- create_documentation.py END Function describe\_location -->


<!-- create_documentation.py START Function extract\_DSID -->

## Function extract\_DSID

**extract\_DSID\(filename, strict=False\)**

Given the full path to a file, which has one of the forms
that could be produced by `locate`, extract a DSID.


### Parameters


**filename : str**

Full path to the file

**strict : bool**

Should the filename be interpreted strictly to
minimise the possibility of mistakes.
If `False` the filename is interpreted
in a relaxed way that may allow for mistakes
by is less likely to raise an error


### Returns


**dsid : str**

The sample DSID, as a string.
<!-- create_documentation.py END Function extract\_DSID -->


<!-- create_documentation.py START Function extract\_base\_dir -->

## Function extract\_base\_dir

**extract\_base\_dir\(filepath\)**

Given the full path to a file, which has one of the forms
that could be produced by `locate`, extract it's base directory.


### Parameters


**filename : str**

Full path to the file

**strict : bool**

Should the filename be interpreted strictly to
minimise the possibility of mistakes.
If `False` the filename is interpreted
in a relaxed way that may allow for mistakes
by is less likely to raise an error


### Returns


**base\_dir : str**

The base directory of this filepath,
before the sample specific sub\_dir component.
<!-- create_documentation.py END Function extract\_base\_dir -->


<!-- create_documentation.py START Function locate -->

## Function locate

**locate\(data\_type, simulation\_type, simulation\_release, particle\_content, energy, eta\_range, zv=0, base\_dir='eos', \*\*excess\_args\)**

For given sample specification and data type, construct
a string representing the full path that would be the correct
place for the data file.
Does not check if a data file is actually there.


### Parameters


**data\_type : str**

What kind of data file is being located.
It could be a `EVNT`, `param`, `xAOD`, `flatTTree` or `images`.

**simulation\_type : str**

Name of the simulation used for the sample.
The simulation type should be one of the entries on
this \(https://gitlab.cern.ch/atlas/athena/\-/blob/master/simulation/isf/isf\_config/python/isf\_configconfigdb.py\)
web page.

**simulation\_release : str**

The release of the simulation that was used for the sample.
The simulation release is normally the athena version,
though sometimes more information is given here.

**particle\_content : str or int**

The particle in the sample.
The particle content should be a plural particle name
or something else from `particles.valid_particle_content`,
or a pid.

**energy : int or float or str**

The energy of the sample.

**eta\_range : tuple or str**

the eta range is a tuple of 2 or 4 ints, or a string representing eta range,
see documentation for `dsid.eta_range_to_string`.

**zv : int or str \(optional\)**

The z vertex offset of the sample

**\*\*excess\_args : anything**

Swallows excessive arguments silently.
This allows dicts that contain at least the required arguments
to be used to construct an argument list.


### Returns


**file\_path : str**

Full path that the data file should reside at.
    
<!-- create_documentation.py END Function locate -->


<!-- create_documentation.py START Object base\_directories -->

## Object base\_directories

**dict**

\{'local': '/afs/cern.ch/user/h/hdayhall/public/data', 'env\_DATADIR': None, 'eos22': '/eos/atlas/atlascerngroupdisk/proj\-simul/AF3\_Run3/', 'eos': '/eos/atlas/atlascerngroupdisk/proj\-simul/'\}
<!-- create_documentation.py END Object base\_directories -->


<!-- create_documentation.py START Object sub\_dir -->

## Object sub\_dir

**dict**

\{'EVNT': 'validation/EVNT', 'params': 'InputsToBigParamFiles', 'xAOD': 'OutputSamples', 'flatTTree': 'flatTTree', 'images': 'ComparisonFigures'\}
<!-- create_documentation.py END Object sub\_dir -->


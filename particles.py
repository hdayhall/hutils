""" Information about known particle content.
Designed to make one central reference point for both single
particles and key hard interactions.
Currently only contains a limited number of particles,
which could be expanded.

Example
-------

Use case; decide if a string corresponds to a known sample particle content;

>>> from hutils import particles
>>> unknown1 = "not_particle"
>>> assert unknown1 not in particles.valid_particle_content
>>> unknown2 = "HiggsGammaGamma"
>>> assert unknown2 in particles.valid_particle_content

Example
-------

Use case; convert to or from a pid;

>>> from hutils import particles
>>> pid1 = 11
>>> name = particles.pid_to_name(pid1)
>>> assert name == "photons"
>>> pid2 = particles.name_to_pid(name)
>>> assert pid1 == pid2

"""
# leading underscore here, because I'd like the freedom 
# to add more information here in the future
_pid_database = {11: "electrons", 15: "taus", 22: "photons", 211: "pions", 2212: "protons"}

# will keep same format
known_pids = sorted(_pid_database.keys())
plural_names = [_pid_database[pid] for pid in known_pids]

def pid_to_name(pid):
    """
    Given a pid of a known particle, return the name
    in lower case and plural.

    Parameters
    ----------
    pid : int
        pid of the particle, as defined by 
        `https://pdg.lbl.gov/2007/reviews/montecarlorpp.pdf`

    Returns
    -------
    name : str
        Name of the particle in plural and all lower case
    
    Raises
    ------
    NotImplementedError
        An unknown pid is given.
    """
    if pid not in _pid_database:
        message = "Only know pids " + ', '.join(map(str, known_pids))
        message += " don't recognise {}".format(pid)
        raise NotImplementedError(message)
    return _pid_database[pid]


def name_to_pid(name):
    """
    Given a name of a known particle, in lower case and plural,
    return the pid.

    Parameters
    ----------
    name : str
        Name of the particle in plural and all lower case

    Returns
    -------
    pid : int
        pid of the particle, as defined by 
        `https://pdg.lbl.gov/2007/reviews/montecarlorpp.pdf`

    Raises
    ------
    NotImplementedError
        An unknown name is given.
    """
    for pid in known_pids:
        if _pid_database[pid] == name:
            return pid
    else:
        message = "Only know names " + ', '.join(_pid_database.values())
        message += " don't recognise {}".format(name)
        raise NotImplementedError(message)


composite_particle_content = ["higgsyy", "dijets", "zee"]
valid_particle_content = plural_names + composite_particle_content

def are_equal(a, b):
    if a.startswith('pid'):
        a = int(a[3:])
    if b.startswith('pid'):
        b = int(b[3:])
    if isinstance(a, int):
        a = pid_to_name(a)
    if isinstance(b, int):
        b = pid_to_name(b)
    return b == a

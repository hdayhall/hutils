""" Handle root files.
Tools specifically for dealing with root files, which both cover
common operations and can sometimes serve all needs without importing
root anywhere else.
This is valuable because when imported, root swallows any `--help`
flags, which interferes with the help text provided by argparse.
The import here is through a proxy pattern, so that root only appears
to be imported from the start, and is actually imported first time 
there is an attempt to retrieve an attribute from it.
This see normally more than enough of a delay to allow argparse 
to be called first, parsing the `--help` flag correctly when needed.

Example
-------

Use case, you need to open a TFile, but don't expect
to do much else requiring pyroot and want to avoid the import
for the reasons mentioned above.

You could just use the proxy pattern import created here;

>>> from hutils.root import ROOT

which fixes issues with the help flag.
But if you really just want a file open;

>>> from hutils import root
>>> tfile = root.open_tfile("/path/to/file.root")

By default this opens the file in `READ` mode,
and throws an error if it doesn't exist.
There is a mode argument to change that behavior.
If the argument passed is actually a TFile, not a string
the `ReOpen` method is called to ensure the mode matches the 
users requirement.

Example
-------

Use case, you have a TFile, made of objects that
implement `GetListOfKeys` and `Get` to some unknown depth.
You want to get a python dictionary that replicates the
TFile structure.

>>> from hutils import root
>>> tfile = root.open_tfile("/path/to/file.root")
>>> key_dict = root.recursive_get_keys(tfile)

This `key_dict` object is a recursive dictionary,
that ends with the string representation of the first object
that cannot be unpacked using `GetListOfKeys` and `Get`.

Example
-------

Use case; you have some text that you with to read, write
or append to an otherwise blank canvas in a root file.

Let us imagine that you want to add the hash of the
git commit currently active to a canvas on a root file.
First, get the has itself;

>>> import subprocess
>>> githash = subprocess.getoutput("git rev-parse HEAD")

Of course, if you are not in a git repo, this will return nonsense,
but let's assume you are.

>>> from hutils import root
>>> text = "Githash at run time=\n" + githash
>>> root.write_canvas_text("/path/to/file.root", "GitTextCanvas", text)

Now if you open the root file at `/path/to/file.root` with a 
TBrowser you should see a canvas with two text objects, one
for each line of text in our `text` string.
They have to be separate text objects, because TText has
very limited facilities for handling line breaks.

To check it worked algorithmically;

>>> from hutils import root
>>> assert root.has_canvas("/path/to/file.root", "GitTextCanvas")
>>> found_text = root.read_canvas_text("/path/to/file.root", "GitTextCanvas")
>>> assert isinstance(found_text, str)
>>> print(found_text)


"""
import sys as _sys    
# if root is imported too early it overrides the help
# messages from argparse.
class __root_importer:
    def __getattr__(self, attr_name):
        global ROOT
        import ROOT
        # actually get the requested attribute
        return getattr(ROOT, attr_name)

# as soon at something tries to get an attribute,
# this will be replaced with the actual root package
ROOT = __root_importer()


def open_tfile(file_name, mode="READ"):
    """
    Given a filepath of a tfile, try to open it in the specified mode,
    calling `ReOpen` if needed.

    Parameters
    ----------
    file_name : str or ROOT.TFile
        File to be opened
    mode : str (optional)
        Mode to open the file in.
        Can be `"READ"`, `"NEW"`, `"CREATE"`, `"RECREATE"`, `"NET"` or `"WEB"`.
        (Default value = `"READ"`)

    Returns
    -------
    tfile : ROOT.TFile
        The opened, or reopened TFile.

    """
    import os
    if mode == "READ" and not os.path.exists(file_name):
        raise OSError("No file named {}, cannot open in READ mode"
                      .format(file_name))
    if isinstance(file_name, ROOT.TFile):
        file_name.ReOpen(mode)
        return file_name
    tfile = ROOT.TFile.Open(file_name, mode)
    return tfile


def recursive_get_keys(root_tree, max_depth=-1, _recusion=0):
    """
    Given a TTree, make a recursive dictionary of all keys found by
    using `GetListOfKeys` and `Get`. The terminal layer of the dictionary
    contains string representations of the objects that did not respond
    to `Get` or `GetListOfKeys`.

    Not often useful, most root objects don't respond to generic `Get`
    which is a shame, because that would be a really nice use of
    the collection pattern.

    Parameters
    ----------
    root_tree : ROOT.TObject
        Open TFile or TTree to be inspected.
    max_depth : int
        Maximum depth to recurse to, if this is hit,
        the final layer will be an empty dict, to
        indicate the possibility of lower layers.

    Returns
    -------
    key_dict : recursive dict of str
        This `key_dict` object is a recursive dictionary,
        that ends with the string representation of the first object
        that cannot be unpacked using `GetListOfKeys` and `Get`.

    """
    key_dict = {}
    if max_depth >= 0:
        if _recusion >= max_depth:
            return key_dict
    for key in root_tree.GetListOfKeys():
        key_name = key.GetName()
        content = root_tree.Get(key_name)
        try:
            content = recursive_get_keys(content, _recusion+1)
        except Exception:
            content = str(content)
        key_dict[key_name] = content
    return key_dict


def has_canvas(root_file, title):
    """
    Check if the given TFile contains a canvas of the specified
    name at top level.

    Parameters
    ----------
    root_file : str or ROOT.TFile
        root file, or path to the root file
    title : str
        The name the canvas is saved under.
        Technically its the canvas name, not it's title.

    Returns
    -------
    : bool
        `True` if the canvas is present
        
    """
    root_file = open_tfile(root_file, "READ")
    keys = recursive_get_keys(root_file, 1)
    if title not in keys:
        return False
    obj = root_file.Get(title)
    return isinstance(obj, ROOT.TCanvas)


def read_canvas_text(root_file, title):
    """
    Extract text that has been written to a canvas on a root file.
    Assumes each separate TText object on the canvas is another line
    of the text, and joins them with line breaks.

    Parameters
    ----------
    root_file : str or ROOT.TFile
        root file, or path to the root file
    title : str
        The name the canvas is saved under.
        Technically its the canvas name, not it's title.

    Returns
    -------
    text : str
        All the lines of text on the canvas joined into a
        multiline string.

    """
    root_file = open_tfile(root_file, "READ")
    canvas = root_file.Get(title)
    canvas_primitives = canvas.GetListOfPrimitives()
    lines = []
    for item in canvas_primitives:
        if isinstance(item, ROOT.TText):
            #line = str(item).split("Title: ", 1)[1]
            # not sure why the text is in the title, but it is
            line = item.GetTitle()
            lines.append(line)
    return '\n'.join(lines[::-1])


def text_on_canvas(canvas_name, text, title=""):
    """
    Create a canvas, and write specified text to it.

    Parameters
    ----------
    canvas_name : str
        The name for the canvas to take in the root file,
        referred to elsewhere in this module as title.
    text : str
        String with one or more lines of text.
    title : str (optional)
        A title for the canvas (in the sense of a root
        canvas object's title attribute).

    Returns
    -------
    canvas : ROOT.TCanvas
        A canvas object containing the lines of text as one
        or more ROOT.TText object.

    """
    canvas = ROOT.TCanvas(canvas_name, title, 10, 10)
    canvas.cd()
    lines = text.split('\n')
    for i, line in enumerate(lines[::-1]):
        y = (i + 0.5)/len(lines)
        tlatex = ROOT.TLatex()
        tlatex.SetNDC()
        tlatex.DrawText(0.1, y, line)
    return canvas


def write_canvas_text(root_file, title, text):
    """
    Given a TFile or the path to a root file, write
    given text on a canvas at top level.

    Parameters
    ----------
    root_file : str or ROOT.TFile
        root file, or path to the root file
    title : str
        The name the canvas is saved under.
        Technically its the canvas name, not it's title.
    text : str
        All the lines of text to put on the canvas joined
        into a multiline string.

    """
    root_file = open_tfile(root_file, "UPDATE")
    root_file.cd()
    canvas = text_on_canvas(title, text)
    root_file.cd()
    canvas.Write()


def append_canvas_text(root_file, title, text):
    """
    Given a TFile or the path to a root file, append
    given text to a canvas that may already be present 
    at top level.

    Parameters
    ----------
    root_file : str or ROOT.TFile
        root file, or path to the root file
    title : str
        The name the canvas is saved under.
        Technically its the canvas name, not it's title.
    text : str
        All the lines of text to put on the canvas joined
        into a multiline string.

    """
    existing_text = read_canvas_text(root_file, title)
    new_text = existing_text + "\n" + text
    write_canvas_text(root_file, title, new_text)


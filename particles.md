
<!-- create_documentation.py START Module particles -->

# Module particles

*Documentation for the code at `particles.py`*

Information about known particle content.
Designed to make one central reference point for both single
particles and key hard interactions.
Currently only contains a limited number of particles,
which could be expanded.


## Example


Use case; decide if a string corresponds to a known sample particle content;


```python
from hutils import particles
unknown1 = "not_particle"
assert unknown1 not in particles.valid_particle_content
unknown2 = "HiggsGammaGamma"
assert unknown2 in particles.valid_particle_content
```



## Example


Use case; convert to or from a pid;


```python
from hutils import particles
pid1 = 11
name = particles.pid_to_name(pid1)
assert name == "photons"
pid2 = particles.name_to_pid(name)
assert pid1 == pid2
```



<!-- create_documentation.py END Module particles -->


<!-- create_documentation.py START Breakpoint disclaimer -->



 -----
 -----

 This should be everything needed for basic use of the package.
 Unless you are looking to develop the package, or debug bad behaviour,
 you probably don't need anything below this point.


<!-- create_documentation.py END Breakpoint disclaimer -->


<!-- create_documentation.py START Docstrings header -->



 ## Doctrings of functions and classes


<!-- create_documentation.py END Docstrings header -->


<!-- create_documentation.py START Function name\_to\_pid -->

## Function name\_to\_pid

**name\_to\_pid\(name\)**

Given a name of a known particle, in lower case and plural,
return the pid.


### Parameters


**name : str**

Name of the particle in plural and all lower case


### Returns


**pid : int**

pid of the particle, as defined by

**`https://pdg.lbl.gov/2007/reviews/montecarlorpp.pdf`**



### Raises

NotImplementedError
An unknown name is given.
<!-- create_documentation.py END Function name\_to\_pid -->


<!-- create_documentation.py START Function pid\_to\_name -->

## Function pid\_to\_name

**pid\_to\_name\(pid\)**

Given a pid of a known particle, return the name
in lower case and plural.


### Parameters


**pid : int**

pid of the particle, as defined by

**`https://pdg.lbl.gov/2007/reviews/montecarlorpp.pdf`**



### Returns


**name : str**

Name of the particle in plural and all lower case


### Raises

NotImplementedError
An unknown pid is given.
<!-- create_documentation.py END Function pid\_to\_name -->


<!-- create_documentation.py START Object composite\_particle\_content -->

## Object composite\_particle\_content

**list**

\['HiggsGammaGamma', 'dijet'\]
<!-- create_documentation.py END Object composite\_particle\_content -->


<!-- create_documentation.py START Object known\_pids -->

## Object known\_pids

**list**

\[11, 22, 211\]
<!-- create_documentation.py END Object known\_pids -->


<!-- create_documentation.py START Object plural\_names -->

## Object plural\_names

**list**

\['electrons', 'photons', 'pions'\]
<!-- create_documentation.py END Object plural\_names -->


<!-- create_documentation.py START Object valid\_particle\_content -->

## Object valid\_particle\_content

**list**

\['electrons', 'photons', 'pions', 'HiggsGammaGamma', 'dijet'\]
<!-- create_documentation.py END Object valid\_particle\_content -->


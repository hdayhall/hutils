""" General utilities.
Tools that perform very generic functions. 
Many of these essentially just call one function in one library,
but have been moved to this module to allow for the possibility of
changes in the underlying operating system or libraries.

A few of them provide useful functions not avaliable by default
in any libraries in the default Athena setup.

Example
-------

Use case; you have a string that contains at least one of a
list of targets, you want to know the index of the first of
any of the targets.

>>> targets = ["dog", "cat"]
>>> my_str = "See dog then cat"
>>> i, found = multi_index(my_str, targets)
>>> assert i == 5
>>> assert found == "dog"

Example
-------

Use case; you need a folder to exist, and you
want to make it recursivly if it doesn't.

>>> folder_needed = "/path/to/folder/"
>>> ensure_folder_exists(folder_needed)

Example
-------

Use case; you need today's date as a regular string.

>>> get_date()

Example
-------

Use case; you need a new directory, that is chosen not to clash
with any current directory, created in a thread safe manner.

>>> make_new_unique_directory(name_base="my_new_dir_")

Example
-------

Use case; you need to move all files in one directory to anouther
directory. The target directory does not have to already exist.

>>> move_files("/path/to/source/dir/", "/path/to/target/")

You may optionally specify the start of the name of the files to move

>>> move_files("/path/to/source/dir/filebase_", "/path/to/target/")

Example
-------

Use case; you need to get the name of the home 
directory, and you would like it to work even if `$HOME`
doesn't exist (say in a condor job).

>>> home_dir = get_home_dir()

"""
import os as _os

def multi_index(string, multi_targets):
    """
    Given a string that contains at least one of a list of targets,
    find the target that appears first, and it's starting index.

    Parameters
    ----------
    string : str
        The string that contains the targets
    multi_targets : iterable of str
        The iterable of targets

    Returns
    -------
    i : int
        the index of the first target found
    target : str
        the value of the first target found

    """
    for i in range(len(string)):
        for target in multi_targets:
            if string[i:].startswith(target):
                return i, target
    else:
        raise ValueError("none of {} found in {}"
                         .format(multi_targets, string))


def ensure_folder_exists(folderName):
    """ 
    Make the folder if it doesn't currently exist

    Parameters
    ----------
    folderName : str
        full path to the needed folder

    """
    _os.system("mkdir -p " + folderName)


def get_date():
    """
    Give todays date

    Returns
    -------
    today : str
        today's date as a str in form ddmmyyyy

    """
    import time
    today = time.strftime("%d%m%Y", time.localtime())
    return today


def make_new_unique_directory(name_base = "./temp_"):
    """
    Make a new directory that is garenteed not to clash
    with any existing directory. Thread safe in unix,
    even if multiple threads call this function at the same
    time, it uses the unitary directory construction operator such
    that they will never both return the same directory.

    Parameters
    ----------
    name_base : str (optional)
        The start of the directories path, can include the first
        part of the innermost directory name.
        An integer will be appended to this name to ensure
        a unique directory may be constructed.
        (Default value = "./temp_")

    Returns
    -------
    : str
        The name of the new unique directory.

    """
    i = 0
    try:
        FileExistsError  # defined in python3
    except NameError:
        # not defined in python2
        FileExistsError = OSError
    while True:
        try:
            _os.makedirs(name_base + str(i))
        except FileExistsError:
            i += 1
        else:
            return name_base + str(i)


def move_files(current_folder_namestart, destination):
    """
    Recursively move all files from one location to anouther

    Parameters
    ----------
    current_folder_namestart : str
        Start of all the paths to move, 
        can be just a directory, or a directory
        and the start of the file names.
        
    destination : str
        Path to move all files to.

    """
    ensure_folder_exists(destination)
    if _os.path.exists(current_folder_namestart):
        current_folder = current_folder_namestart
        current_base = ""
    else:
        current_superdir, current_base = _os.path.split(current_folder_namestart)
        for name in _os.listdir(current_superdir):
            if name.startswith(current_base):
                current_folder = _os.path.join(current_superdir, name)
                break
    import shutil
    for name in _os.listdir(current_folder):
        if name.startswith(current_base):
            to_move = _os.path.join(current_folder, name)
            move_to = _os.path.join(destination, name)
            try:
                shutil.copytree(to_move, move_to, dirs_exist_ok=True)
            except NotADirectoryError:
                shutil.move(to_move, move_to)
            else:
                shutil.rmtree(to_move)


def get_home_dir():
    """ 
    Guess the path to the home (`~`) directory.
    Useful inside condor scripts where $HOME dosn't exist 

    Returns
    -------
    home : str or None
        the full path to the users home directory
        or None if not enough information to find this
        exists.

    """
    # first try enviroment
    if "HOME" in _os.environ:
        return _os.environ["HOME"]
    # if that fails, we recreate the home from the USER
    if "USER" in _os.environ:
        user = _os.environ["USER"]
        home = "/afs/cern.ch/user/{}/{}".format(user[0], user)
        return home
    return None


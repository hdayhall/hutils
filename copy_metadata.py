#!/usr/bin/env python
from __future__ import absolute_import
""" Copy metadata.
Intended to form a script that can be used from bash, to copy metadata
from one location onto another. The source and target can be text files 
or root files.

Example
-------

Say you have a text file containign metadata at 
`/path/to/prepared/metadta.txt` and a root
file that should be labeled with this metadata at
`/path/to/unlabeled/data.root`.
The metadata can be added to the root file by
running;

```bash
python copy_metadata.py /path/to/prepared/metadata.txt /path/to/unlabeled/data.root
```

If the target path does not exist it will be created.
If the target path has existing metadata it will be overwritten.

"""


def copy(input_file, output_file):
    """
    Copy metadata from one place to another.

    Parameters
    ----------
    input_file : str
        Path to file to read from. If this path ends with `.root`
        it is assumed the input file is a root file, otherwise
        it is assumed the file is a text file.
    output_file : str
        Path to file to write to. If this path ends with `.root`
        it is assumed the output file is a root file, otherwise
        it is assumed the file is a text file.
        If the file doesn't exist it will be created.

    """
    if __package__ is None:
        # anticipate being called directly as a script
        # in which case the relative import would fail
        import sys, os
        folder = os.path.dirname(os.path.realpath(__file__))
        folder_above = os.path.abspath(os.path.join(folder, ".."))
        sys.path.insert(1, folder_above)
        from hutils.metadata import SampleMetadata
    else:
        from .metadata import SampleMetadata
    
    if input_file.endswith(".root"):
        sample_meta = SampleMetadata.read_root_file(input_file)
    else:
        sample_meta = SampleMetadata.read_text_file(input_file)

    if output_file.endswith(".root"):
        sample_meta.write_root_file(output_file)
    else:
        sample_meta.write_text_file(output_file)


if __name__ == "__main__":
    import sys
    input_file = sys.argv[1]
    output_file = sys.argv[2]
    copy(input_file, output_file)


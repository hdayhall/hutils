from __future__ import absolute_import
""" Create documentation.
Module for automatically generating documentation for python projects that
have numpy docstrings.

Forms markdown files that can be updated, and can have other
comments included that will not be overwritten by updates.
Markdown files are designed for display in gitlab.
It is assumed that for the most part documentation found in the python
file is not already markdown formatted.

Example
-------

Say you want to create documentation for the python code below,
which you have saved in animal_example.py;

```python
\"\"\" 
My Module docstring
\"\"\"
__documentation_file = "/path/to/documentation/for/animal_example.md"
cat = "Meow"

def dog(noise="woof"):
    \"\"\" Docstring for dog

    Parameters
    ----------
    noise : str
        Noise that a dog makes.
        (Default value = "woof")

    Returns
    -------
    balls : int
        Number of tennis balls
    sticks : float
        Length of stick

    \"\"\"
    balls = 4
    sticks = 3.5
    return balls, sticks

```

In particular note that;

1. All doc strings are surrounded by three double quotes
2. All docstrings have the numpydoc format
3. No markdown is used in the docstrings, as this would be interpreted literally
4. The module has a module docstring at the top before the first class or function.
5. Any single backticks in documentation are contained in a single line, for multiline code formatting, use three backticks.

Documentation for this could be created by running;

>>> import animal_example
>>> from hutils import create_documentation
>>> create_documentation.update_docs(animal_example)

As the `animal_example.py` contains a variable `__documentation_file`
a new file will be created at the path specified by `__documentation_file`.
If that variable hadn't been present then the documentation would have been
created in the same place as the code, with the name `animal_example.md`.

The documentation for this file will contain;

```markdown

<!-- create_documentation.py START Module animal example -->

# Module animal example

*Documentation for the code at `animal_example.py`*

 
My Module docstring

<!-- create_documentation.py END Module animal example -->


<!-- create_documentation.py START Breakpoint disclaimer -->



 -----
 -----

 This should be everything needed for basic use of the package.
 Unless you are looking to develop the package, or debug bad behaviour,
 you probably don't need anything below this point.


<!-- create_documentation.py END Breakpoint disclaimer -->


<!-- create_documentation.py START Docstrings header -->



 ## Doctrings of functions and classes


<!-- create_documentation.py END Docstrings header -->


<!-- create_documentation.py START Function dog -->

## Function dog

**dog\(noise='woof'\)**

Docstring for dog


### Parameters


**noise : str**

Noise that a dog makes.
\(Default value = "woof"\)


### Returns


**balls : int**

Number of tennis balls

**sticks : float**

Length of stick
<!-- create_documentation.py END Function dog -->


<!-- create_documentation.py START Object cat -->

## Object cat

**str**

Meow
<!-- create_documentation.py END Object cat -->

```

Each function, class or object has been given it's own set of
`create_documentation.py START` and `create_documentation.py END` html comments
and the documentation for the function has been written between them.
These form tags for the corresponding section of the documentation file.
These will not be visible when the file is viewed on gitlab.
The blank line below each html comment must be respected.

If the `create_documentation.update_docs` is run again,
it will apply changes found in `animal_example.py` to the 
documentation file automatically, searching for existing `create_documentation.py START`
and `create_documentation.py END` tags and replacing the content between them.
If a pair of tags is not found, the new function, object or class will be
appended to the documentation with new tags.

It is possible to add to this documentation file manually,
and provided the additions are not between a 
`create_documentation.py START` and `create_documentation.py END` 
html comment, they will not be overwritten.

It is also possible to rearrange the order of the tags in the file
and the new order will be respected by subsequent calls to `create_documentation.update_docs`.

"""
import os as _os
doc_delimiator = '"""'

class Docstring:
    """ 
    General object to represent the information relating 
    to a component of documentation. 
    This is intented as a base class, carrying functions general to all Docstrings,
    from which more specific Docstring types can be created.
    Gets converted to a string then written to file.

    Attributes
    ----------
    placement : int
        Gives the intended index of this docstring relative to other docstrings.
        The default value, -1, just places the docstring at the end of the document.
        Value is ignored if document already contains tags for this docstring.
        
    """
    placement = -1
    def __init__(self, title, numpydoc_text, **additional_attributes):
        """ Docstring initialisation.

        Parameters
        ----------
        title : str
            Title attribute, which will be displayed literally
            with all special chars, by processing with 
            Docstring.sanatise_for_markdown.
        numpydoc_text : str
            The body of the docstring. Any common indent will be removed,
            then it will be stored as a list of lines
        **additional_attributes : dict
            anything else that should be stored, will be stored as attributes.
        """
        self.title = self.sanatise_for_markdown(title)
        # clean up the doc string
        numpydoc_text = numpydoc_text.replace(doc_delimiator, '')
        numpydoc_lines = numpydoc_text.split(_os.linesep)
        common_indent = min(next((i for i, c in enumerate(line)
                                  if line.strip()), len(line))
                            for line in numpydoc_lines)
        numpydoc_lines = [line[common_indent:] for line in numpydoc_lines]
        self.numpydoc_lines = numpydoc_lines
        for key, value in additional_attributes.items():
            setattr(self, key, value)

    @classmethod
    def sanatise_for_markdown(cls, line):
        """
        Convert the line into something that will be
        displayed literally in markdown

        Parameters
        ----------
        line : str
            text to be processed

        Returns
        -------
        line : str
            input text represented so that all chars
            are displayed litteraly in markdown
        """
        # backticks indicate places things should be left as is
        parts = line.split('`')
        assert len(parts) %2 != 0, \
            'Line "{}" appears to have an uneven number of backticks'.format(line)
        markdown_special_chars = '\\*_{}[]()#+-!' 
        new_parts = []
        for i, part in enumerate(parts):
            if i%2 == 0:
                for char in markdown_special_chars:
                    part = part.replace(char, '\\' + char)
            new_parts.append(part)
        line = '`'.join(new_parts)
        return line

    @classmethod
    def get_indent(cls, lines):
        """ 
        Find the greatest common number of leading spaces for a list of lines.

        Parameters
        ----------
        lines : list of str
            text to be analysed
        
        Returns
        -------
        : int
            number of leading spaces common to all lines
        """
        indents = [next((i for i, c in enumerate(line)
                         if c != ' '), len(line)) for line in lines]
        return min(indents)

    @classmethod
    def is_attribute(cls, line):
        """ 
        Check if the given line could be an 
        attribute line in a numpy doc. Attribute lines
        optionally name, and always give the class of an 
        object, with a colon seperating the two properties.

        Parameters
        ----------
        line : str
            Line to check.

        Returns
        -------
        : bool
            True if the line looks like an attribute line.

        """
        if line.count(":") != 1:
            return False
        before, after = line.split(":")
        if len(before.split()) > 1:
            return False
        return len(after.split()) > 0

    @classmethod
    def is_exception(cls, line):
        """
        Check if the given line could be an 
        exception line in a numpy doc. Exception lines
        contain only the name of an exception.

        Parameters
        ----------
        line : str
            Line to check.

        Returns
        -------
        : bool
            True if the line looks like an exception line.

        """
        line = line.split()
        if not len(line) == 1:
            return False
        exception = line[0]
        for term in ["Exception", "SystemExit", "KeyboardInterrupt",
                     "GeneratorExit", "StopIteration", "StopAsyncIteration",
                     "Error", "Warning"]:
            if term in exception:
                return True
        return False


    def numpydoc_to_markdown(self, title_level=3):
        """
        Produce a markdown version of the numpydoc_lines stored in this class. 

        Takes a number of formatting decisions designed to render numpydocs nicely
        into markdown. The includes inserting titles, ensuring code is surrounded 
        by formatting tags, and bolding various key lines. With very few exceptions,
        it is assumed that all charicters in the numpydoc shoudl be prepresented literally.
        Things that are not interpreted literally are backticks for code, and indetation.
        Any code that is surrounded by single backticks must be all on one line,
        the alternative is too error prone to interpret.

        Parameters
        ----------
        title_level : int
            Number of hashes to prepend to the largest titles in this section,
            larger values result in smaller titles.

        Returns
        -------
        markdown : str
            The resulting markdown representation of numpydoc_lines.
            
        """
        markdown_lines = []
        title_prefix = "#"*title_level + " "
        line_reached = 0
        in_params_returns = False
        in_exceptions = False
        base_indent = self.get_indent(self.numpydoc_lines)
        while line_reached < len(self.numpydoc_lines):
            line = self.numpydoc_lines[line_reached][base_indent:]
            line = line.rstrip(_os.linesep)
            if not line.strip():
                markdown_lines.append(line)
                line_reached += 1
            elif all(c=='-' for c in line.strip()):
                #  then the previous line was a title
                title_line = title_prefix + \
                             self.sanatise_for_markdown(markdown_lines[-1])
                in_params_returns = ("Parameters" in title_line 
                                     or "Returns" in title_line
                                     or "Attributes" in title_line
                                     or "Properties" in title_line)
                in_exceptions = "Exceptions" in title_line
                markdown_lines[-1] = ""
                markdown_lines.append(title_line)
                markdown_lines.append("")
                line_reached += 1
            elif line.lstrip(" ").startswith(">>> "):
                # line shoudl eb formatted as code
                line = line.lstrip(" ")
                markdown_lines.append('')
                markdown_lines.append("```python")
                # continuation lines can start with ... 
                while line.startswith(">>> ") or line.startswith("... "):
                    line = line[4:]
                    markdown_lines.append(line)
                    line_reached += 1
                    if line_reached >= len(self.numpydoc_lines):
                        break
                    line = self.numpydoc_lines[line_reached]
                    line = line.lstrip(" ").rstrip(_os.linesep)
                markdown_lines.append('```')
                markdown_lines.append('')
            elif line.lstrip(" ").startswith("```"):
                # codeblock already
                start_block = line.split()[0]
                markdown_lines.append('')
                if len(start_block) == 3:
                    markdown_lines.append("```python")
                else:  # it alredy indicates a languge
                    markdown_lines.append(start_block)
                # start by getting all the code lines
                line_reached += 1
                line = self.numpydoc_lines[line_reached]
                code_lines = []
                while not line.strip().startswith("```"):
                    code_lines.append(line.rstrip(_os.linesep))
                    line_reached += 1
                    line = self.numpydoc_lines[line_reached]
                # now remove the common indent
                code_indent = self.get_indent(code_lines)
                for line in code_lines:
                    markdown_lines.append(line[code_indent:])
                line_reached += 1
                markdown_lines.append('```')
                markdown_lines.append('')
            elif ((in_params_returns and self.is_attribute(line))
                    or (in_exceptions and self.is_exception(line))):
                # the line is a section heading,
                line = self.sanatise_for_markdown(line.strip())
                line = "**" + line + "**"
                markdown_lines.append('')
                markdown_lines.append(line)
                markdown_lines.append('')
                line_reached += 1
            else:
                line = self.sanatise_for_markdown(line)
                markdown_lines.append(line.strip())
                line_reached += 1
        markdown = _os.linesep.join(markdown_lines)
        return markdown


class ModuleDocstring(Docstring):
    """
    Contains information relating to the docstring of the module itself.
    Inherits from Docstring.

    Attributes
    ----------
    placement : int
        Gives the intended index of this docstring relative to other docstrings.
        The default value, 0, becuase the Moduledocstring usually want to be the first docstring
        in the documentation.
        Value is ignored if document already contains tags for this docstring.
        
    """
    placement = 0
    def __str__(self):
        title_line = "# " + self.title
        parts = [title_line]
        if hasattr(self, 'relpath_to_code'):
            path_description = "*Documentation for the code at `{}`*"\
                               .format(self.relpath_to_code)
            parts.append(path_description)
        parts.append(self.numpydoc_to_markdown(title_level=2))
        text = (_os.linesep*2).join(parts)
        return text

    @property
    def path_to_documentation(self):
        """
        Path of location for saving documentation.

        Can be relative of absolute, when it is set,
        the process of setting extracts the relative path from
        the documentation to the code
        """
        return self._path_to_documentation

    @path_to_documentation.setter
    def path_to_documentation(self, value):
        self._path_to_documentation = value
        doc_folder = _os.path.dirname(value)
        self.relpath_to_code = _os.path.relpath(self.code_path, doc_folder)

    @classmethod
    def read_from_file(cls, file_path):
        """
        Alternative constructor for this docstring, using a filepath name.

        Getting the module's docstring from the module object itself using inspect
        is unreliable. Inspect assumes that the module docstring is the very first
        thing in the file, but unfortunatly future imports must go first.
        Instead, this method actually reads the file content and identifies the
        module docstring as the first multiline string encountered before
        any class or function is defined.

        Parameters
        ----------
        file_path : str
            Path to the module file

        Returns
        -------
        : ModuleDocstring
            The docstring representaiton

        """
        # based on the first 
        file_name = file_path.split("/")[-1]
        title = "Module " + file_name.replace('.py', '').replace('_', ' ')
        with open(file_path, 'r') as inputfile:
            lines = inputfile.readlines()
        # try to find the start of the docstring
        while lines:
            line = lines.pop(0)
            stripped = line.strip()
            if stripped.startswith("def ") or stripped.startswith("class "):
                #no docstring
                docstring = "\n"
                break
            if doc_delimiator in line:
                text = ''.join([line] + lines)
                docstring = text.split(doc_delimiator, 2)[1]
                break
        return cls(title, docstring, code_path=file_path)


class FunctionDocstring(Docstring):
    """
    Contains information relating to the docstring of a single function.
    Inherits from Docstring.

    Attributes
    ----------
    placement : int
        Gives the intended index of this docstring relative to other docstrings.
        The default value, -1, indicates that this can be placed at the end
        of the docfile, unless it's tags already exist.
        Value is ignored if document already contains tags for this docstring.
        
    """
    def __str__(self):
        title_line = "## " + self.title
        markdown = self.numpydoc_to_markdown(title_level=3)
        text = (_os.linesep*2).join([title_line, self.signature, markdown])
        return text

    @classmethod
    def function_name(cls, to_read):
        """
        Get the name of the function.

        Parameters
        ----------
        to_read : callable
            The function we need a name for

        Returns
        -------
        name : str
            Name for the function
        """
        name = to_read.__name__
        return name
        
    @classmethod
    def function_signature(cls, to_read):
        """
        Given a function, write it's signature.
        Note this will include `cls` or `self` for functions in classes.

        Parameters
        ----------
        to_read : callable
            The function we need a signature for

        Returns
        -------
        : str
            Function signature formatted for markdown.
        """
        name = cls.function_name(to_read)
        # N.B. might need different python2 and python3 versions?
        from inspect import formatargspec, getargspec
        args = formatargspec(*getargspec(to_read))
        sig = cls.sanatise_for_markdown(name + args)
        return "**" + sig + "**"

    @classmethod
    def read_from_function(cls, to_read):
        """
        Alternative constructor for this docstring, using a function object.

        Parameters
        ----------
        to_read : callable
            The python function to be documented

        Returns
        -------
        : FunctionDocstring
            The docstring representaiton

        """
        # it feels ugly to access this private member,
        # but I cannot find anoutehr solution
        title = "Function " + cls.function_name(to_read)
        from inspect import getdoc
        docstring = getdoc(to_read)
        if docstring is None:
            docstring = ""
        signature = cls.function_signature(to_read)
        return cls(title, docstring, signature=signature)


class ClassDocstring(Docstring):
    """
    Contains information relating to the docstring of a single class,
    not including contained functions asside from `__init__`.
    Would typically contain attribute information, becuase attribute information
    should be written in the class docstring according to numpydoc standards.
    Inherits from Docstring.

    Attributes
    ----------
    placement : int
        Gives the intended index of this docstring relative to other docstrings.
        The default value, -1, indicates that this can be placed at the end
        of the docfile, unless it's tags already exist.
        Value is ignored if document already contains tags for this docstring.
        
    """
    def __str__(self):
        title_line = "## " + self.title
        markdown = self.numpydoc_to_markdown(title_level=3)
        parts = [title_line, markdown]
        # add the init docstring
        init_docstring = FunctionDocstring.read_from_function(self.init_function)
        self.modify_func_docstring(init_docstring)
        parts.append(str(init_docstring))
        # add the function docstrings
        for func in self.functions.values():
            docstring = FunctionDocstring.read_from_function(func)
            self.modify_func_docstring(docstring)
            parts.append(str(docstring)) 
        text = (_os.linesep*2).join(parts)
        return text

    def modify_func_docstring(self, function_docstring):
        """
        Classes often contain functions, given the docstring of a
        function that came from the class described by this docstring,
        modify the functions docstring to indicate it's membership.

        Parameters
        ----------
        function_docstring : FunctionDocstring
            Docstring that describes a function which is a member
            of the class corrisponding to this docstring.
            Will be modified in place.
        """
        function_docstring.title = "Class function " +\
                self.title.replace("Class ", "") + '.' + \
                function_docstring.title.replace("Function ", "")

    @classmethod
    def class_name(cls, to_read):
        """
        Get the name for this class.

        Parameters
        ----------
        to_read : class
            The class whose name we need.

        Returns
        -------
        name : str
            The name of the class
        """
        name = to_read.__name__
        return name

    @classmethod
    def class_functions(cls, to_read):
        """
        Find all the functions and methods in this class (not attributes).

        Parameters
        ----------
        to_read : class
            The class whose functions we need.

        Returns
        -------
        functions : list of callables
            The functions found.

        """
        directory = {name : getattr(to_read, name) for
                     name in dir(to_read)}
        functions = {name: value for name, value in directory.items()
                     if not name.startswith('_') and callable(value)}
        return functions

    @classmethod
    def read_from_class(cls, to_read):
        """
        Alternative constructor for this docstring, using a class object.

        Parameters
        ----------
        to_read : class
            The python class to be documented

        Returns
        -------
        : ClassDocstring
            The docstring representaiton

        """
        title = "Class " + cls.class_name(to_read)
        from inspect import getdoc
        docstring = getdoc(to_read)
        functions = cls.class_functions(to_read)
        init = to_read.__init__
        return cls(title, docstring, init_function=init, functions=functions)


class ObjectDocstring(Docstring):
    """
    Contains information relating to the docstring of a single object.
    To be used for things in modules that are neither classes nor functions.
    Inherits from Docstring.

    Attributes
    ----------
    placement : int
        Gives the intended index of this docstring relative to other docstrings.
        The default value, -1, indicates that this can be placed at the end
        of the docfile, unless it's tags already exist.
        Value is ignored if document already contains tags for this docstring.
        
    """
    def __str__(self):
        title_line = "## " + self.title
        markdown = self.numpydoc_to_markdown(title_level=3)
        text = (_os.linesep*2).join([title_line, self.signature, markdown])
        return text

    @classmethod
    def object_signature(cls, to_read):
        """
        Given an object, write it's signature.

        Parameters
        ----------
        to_read : object
            The function we need a signature for

        Returns
        -------
        : str
            Object signature formatted for markdown.
        """
        sig = "**" + type(to_read).__name__ + "**"
        return sig
    
    @classmethod
    def read_from_module(cls, object_name, module):
        """
        Alternative constructor for this docstring,
        using the module and object name.

        Cannot construct form the object itself as objects
        don't know their own name.

        Parameters
        ----------
        object_name : str
            The variable name of the object to be documented

        module : module
            The module in which the object can be found.

        Returns
        -------
        : ObjectDocstring
            The docstring representaiton

        """
        value = getattr(module, object_name)
        docstring = str(value)
        title = "Object " + object_name
        signature = cls.object_signature(value)
        return cls(title, docstring, signature=signature)


def harvest_module(module):
    """
    Find and classify all top level content in a module.
    
    Sorts items into functions, classes and, where neither of
    thos catagories apply, objects.
    Skips anything with a leading underscore, so classes functions
    imports or variables can be kept out the documentation the 
    same way they are kept out the default namespace,
    by naming them with a leading underscore.

    So this import;

    ```python
    import os
    ```

    would result in `os` being included in the docs but

    ```python
    import os as _os
    ```

    would not result in `_os` being in the docs, and has the
    advantage that `os` will not polute the namespace normally
    accessed by tab completion.

    Parameters
    ----------
    module : module
        The python module object to be documented

    Returns
    -------
    classes : list of classes
        Classes found at top level.
    functions : list of callables
        Callabels that are not classes found at top level.
    objects : list of objects
        Anything else at top level that is neither a class nor
        a callable.

    """
    from inspect import isfunction, isclass
    classes = []
    functions = []
    objects = []
    for name in dir(module):
        if name.startswith("_"):
            continue
        value = getattr(module, name)
        if isfunction(value):
            functions.append(value)
        elif isclass(value):
            classes.append(value)
        else:
            objects.append(name)
    return classes, functions, objects


def get_module_filepath(module):
    """
    Given a python module, get it's path on disk.

    Parameters
    ----------
    module : module
        The python module object whoes file should be located.

    Returns
    -------
    path : str
        Absolute path of the module's code
    """
    path = module.__file__
    return _os.path.abspath(path)


def find_docs(module):
    """
    Given a python module, get the path to it's documentation,
    if it has a `__documentation_file` variable, use that, otherwise
    put it at the same path as it's code, with a markdown extention,
    `.md`.

    Parameters
    ----------
    module : module
        The python module object whoes documentation should be located.

    Returns
    -------
    docs : str
        Absolute path of the module's documentaion
    """
    if hasattr(module, "__documentation_file"):
        module_path = _os.path.dirname(module.__file__)
        docs = _os.path.join(module_path, module.__documentation_file)
    else:
        filepath = get_module_filepath(module)
        if filepath.endswith(".py"):
            docs = filepath.replace(".py", ".md")
        elif filepath.endswith(".pyc"):
            docs = filepath.replace(".pyc", ".md")
        else:
            raise NotImplementedError(filepath)
    return docs


markdown_comment_open = "<!-- "
markdown_comment_close = " -->"

tag_beggining = _os.linesep + markdown_comment_open + "create_documentation.py "
tag_ending = markdown_comment_close + _os.linesep*2

def make_tags(docstring):
    """
    Given an object that represetns a docstring, design the tags
    that should identify it's start and end in a documentation file.

    Allows the documentation file to be updated when the docstrings change
    without losing information about docstring order or information written
    outside of docstrings.

    Parameters
    ----------
    docstring : Docstring
        Object derived from Docstring, that should be written or updated in
        a docuemntation file

    Returns
    -------
    start_tag : str
        An html comment that is placed before the start of the text of this
        documentation section.
    end_tag : str
        An html comment that is placed after the end of the text of this
        documentation section.
    """
    start_tag = tag_beggining + "START " + docstring.title + tag_ending
    end_tag = tag_beggining + "END " + docstring.title + tag_ending
    return start_tag, end_tag


class BreakDiscalimer:
    """
    Represents a special part of the docfile that seperates
    general information about the module from docstrings for the minuta.
    Gets converted to a string then written to file.

    Attributes
    ----------
    placement : int
        Gives the intended index of this object relative to other docstrings.
        The default value, 1, indicates that this can be placed right after
        the module docstring, unless it's tags already exist.
        Value is ignored if document already contains tags for this docstring.

    title : str
        Title of this object.
        
    """
    placement = 1
    title = "Breakpoint disclaimer"
    def __str__(self):
        text = \
            """

            -----
            -----

            This should be everything needed for basic use of the package.
            Unless you are looking to develop the package, or debug bad behaviour,
            you probably don't need anything below this point.

            """
        text = ' '.join(filter(None, text.split(' ')))
        return text
        


class DocstringsHeader:
    """
    Represents a special part of the docfile that opens the section
    on the minuta of the module.
    Gets converted to a string then written to file.

    Attributes
    ----------
    placement : int
        Gives the intended index of this object relative to other docstrings.
        The default value, 2, indicates that this can be placed after the BreakDiscalimer
        and before the classes, functions and objects, unless it's tags already exist.
        Value is ignored if document already contains tags for this docstring.

    title : str
        Title of this object.
        
    """
    placement = 2
    title = "Docstrings header"
    def __str__(self):
        text = \
            """

            ## Doctrings of functions and classes

            """
        text = ' '.join(filter(None, text.split(' ')))
        return text


def update_docs(module):
    """
    Given a module, find it's docuemntation file and update it
    with the current docstrings for the module itself, all functions,
    classes and objects inside the module.

    Respects any tags that already exist in the module, and
    inserts objects for which tags cannot be found.

    Parameters
    ----------
    module : module
        Python module object of the module to be updated.

    """
    # get the documentation text
    doc_path = find_docs(module)
    print("Documentaion path is " + doc_path)
    if _os.path.exists(doc_path):
        print("Found existing documentation")
        with open(doc_path, 'r') as docs_file:
            text = docs_file.read()
    else:
        text = ""
    # get the content to update with
    print("Reading code")
    classes, functions, objects = harvest_module(module)
    print("Found {} classes, {} functions and {} objects"
          .format(len(classes), len(functions), len(objects)))
    module_filepath = get_module_filepath(module)
    print("Reading mofule docstring from " + module_filepath)
    module_docs = ModuleDocstring.read_from_file(module_filepath)
    module_docs.path_to_documentation = doc_path
    print("Module docstring constructed, listing all docstring components")
    all_docs = [module_docs, BreakDiscalimer(), DocstringsHeader()]
    all_docs += [ClassDocstring.read_from_class(c) for c in classes]
    all_docs += [FunctionDocstring.read_from_function(f) for f in functions]
    all_docs += [ObjectDocstring.read_from_module(name, module) for name in objects]
    print("Inserting docstring componets")
    for doc in all_docs:
        print("\t" + doc.title)
        start_tag, end_tag = make_tags(doc)
        if start_tag in text:
            print("\t \t Found existing tags")
            assert end_tag in text, "Found {} but not {}".format(start_tag, end_tag)
            to_replace = text.split(start_tag, 1)[1].split(end_tag, 1)[0]
            text = text.replace(to_replace, str(doc))
        else:
            print("\t \t No existing tags")
            to_insert = start_tag + str(doc) + end_tag
            if doc.placement == -1:
                print("\t \t Inserting new tags at end")
                text += to_insert
            else:
                print("\t \t Inserting new tags at position {}".format(doc.placement))
                insert_at = 0
                if text:
                    for _ in range(doc.placement):
                        print("\t \t {}\r".format(insert_at))
                        insert_at = text.index(tag_beggining + "END", insert_at) + 1
                    insert_at = text.index(tag_ending, insert_at)
                    insert_at += len(tag_ending)
                text = text[:insert_at] + to_insert + text[insert_at:]
                
    print("Writing new docs")
    with open(doc_path, 'w') as docs_file:
        docs_file.write(text)
    print("Written to " + doc_path)



